var express = require("express");
const router = express.Router();
const routing = require("../router");
const _ = require("lodash");
const middleware = require("../middlewares/middleware");
var throttle = require("express-throttle");

var optionsTrollte = {
  burst: 20,
  period: "1sec",
  on_throttled: function (req, res, next, bucket) {
    res.set("X-Rate-Limit-Limit", 5);
    res.set("X-Rate-Limit-Remaining", 0);
    res.set("X-Rate-Limit-Reset", bucket.etime);
    return res.status(503).json({
      error_message: "To many request",
    });
  },
};

routing.forEach((item) => {
  const middlewareFunctions = [
    middleware(item),
    async function (req, res) {
      const service = require("../services" + item.service);
      return await service.exec(req, res);
    },
  ];

  if (["GET", "LOOKUP", "VIEW"].includes(item.type)) {
    router.get(item.endPoint, throttle(optionsTrollte), ...middlewareFunctions);
  } else if (["POST", "CANCEL"].includes(item.type)) {
    router.post(
      item.endPoint,
      throttle(optionsTrollte),
      ...middlewareFunctions
    );
  } else if (item.type == "PUT") {
    router.put(item.endPoint, throttle(optionsTrollte), ...middlewareFunctions);
  } else if (item.type == "PATCH") {
    router.patch(
      item.endPoint,
      throttle(optionsTrollte),
      ...middlewareFunctions
    );
  } else if (item.type == "DELETE") {
    router.delete(
      item.endPoint,
      throttle(optionsTrollte),
      ...middlewareFunctions
    );
  } else if (item.type == "UPLOAD") {
    const multer = require("multer");
    const upload = multer({});
    router.post(
      item.endPoint,
      middleware(item),
      upload.single("file"),
      async function (req, res) {
        const service = require("../services" + item.service);
        return await service.exec(req, res);
      }
    );
  } else if (item.type == "EXPORT") {
    router.get(
      item.endPoint,
      throttle(optionsTrollte),
      middleware(item),
      async function (req, res) {
        const service = require("../services" + item.service);
        return await service(req, res);
      }
    );
  }
});

module.exports = router;
