const router = [
  // {
  //   type: "POST",
  //   endPoint: "/login",
  //   service: "/auth/login/login",
  //   auth: false,
  // },
  // { type: "POST", endPoint: "/password/forgot", service: "/auth/recovery-password/forgotPassword", auth: false },
  // { type: "POST", endPoint: "/password/recovery", service: "/auth/recovery-password/recoveryPassword", auth: false },

  // CONFIG
  {
    type: "GET",
    endPoint: "/config",
    service: "/_config/_config",
    auth: false,
  },

  //UPLOAD
  {
    type: "UPLOAD",
    endPoint: "/upload/file",
    service: "/uploads/file",
    auth: true,
    task: "upload",
  },

  // BASE-URL/view/report/sales/asp/:id
  // BASE-URL/list/report/sales/asp
  // BASE-URL/create/sales/sales-order
  // BASE-URL/create/sales/sales-order
  // BASE-URL/create/sales/sales-order  sales/sales-order/salesOrderDetail

  // INTEGRATED ENDPOINT
  {
    type: "GET",
    endPoint: "/integrasi/corporate",
    service: "/integrasi/corporate",
    auth: false,
  },
  {
    type: "GET",
    endPoint: "/integrasi/unit-produksi",
    service: "/integrasi/production_unit",
    auth: false,
  },
  // AUTH
  {
    type: "POST",
    endPoint: "/auth/login",
    service: "/auth/login",
    auth: false,
  },
  {
    type: "POST",
    endPoint: "/auth/change-password",
    service: "/auth/changePassword",
    auth: true,
  },

  // TRANSACTION
  {
    type: "LOOKUP",
    endPoint: "/lookup/transaction/transaction-invoice-summary",
    service: "/transaction/statistic/viewTransactionSummary",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/transaction/transaction-information",
    service: "/transaction/listInvoiceTransaction",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/transaction/transaction-payment-detail/:invoice_id",
    service: "/transaction/details/viewDetailPayment",
    auth: true,
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/transaction/payment-information-plant",
    service: "/transaction/productionUnit/lookup",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/transaction/corporate-realization-payment",
    service: "/transaction/corporate/lookup",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/transaction/add-invoice",
    service: "/transaction/process/addInvoice",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/transaction/cancel-payment",
    service: "/transaction/process/cancelPayment",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/transaction/payment-channel",
    service: "/transaction/paymentChannel/list",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/transaction/payment-channel",
    service: "/transaction/paymentChannel/create",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/transaction/payment-channel/:id",
    service: "/transaction/paymentChannel/update",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/view/transaction/payment-detail-transaction/:payment_id",
    service: "/admin/paymentGateway/viewPaymentTransaction",
    auth: true,
  },

  // MASTER
  {
    type: "GET",
    endPoint: "/list/master/statistic-realization",
    service: "/master/statistic/realizationProductionUnit",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/master/corporate",
    service: "/master/corporate/list",
    auth: true,
    task: "corporate",
  },
  {
    type: "VIEW",
    endPoint: "/view/master/detail-corporate-profile/:corporate_id",
    service: "/master/corporate/view",
    auth: true,
    task: "corporate",
  },
  {
    type: "POST",
    endPoint: "/create/master/corporate",
    service: "/master/corporate/create",
    auth: true,
    task: "corporate",
  },
  {
    type: "PATCH",
    endPoint: "/update/master/corporate",
    service: "/master/corporate/update",
    auth: true,
    task: "corporate",
  },
  {
    type: "DELETE",
    endPoint: "/delete/master/corporate/:corporate_id",
    service: "/master/corporate/delete",
    auth: true,
    task: "corporate",
  },
  {
    type: "GET",
    endPoint: "/list/master/production-unit",
    service: "/master/productionUnit/list",
    auth: true,
    task: "production-unit",
  },
  {
    type: "VIEW",
    endPoint: "/view/master/production-unit-profile/:production_unit_id",
    service: "/master/productionUnit/view",
    auth: true,
    task: "production-unit",
  },
  {
    type: "POST",
    endPoint: "/create/master/production-unit",
    service: "/master/productionUnit/create",
    auth: true,
    task: "production-unit",
  },
  {
    type: "PATCH",
    endPoint: "/update/master/production-unit",
    service: "/master/productionUnit/update",
    auth: true,
    task: "production-unit",
  },
  {
    type: "DELETE",
    endPoint: "/delete/master/production-unit/:production_unit_id",
    service: "/master/productionUnit/delete",
    auth: true,
    task: "production-unit",
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/master/corporate-summary",
    service: "/master/details/summary",
    auth: true,
  },

  // CONFIG

  {
    type: "PATCH",
    endPoint: "/update/config/company-profile",
    service: "/config/companyProfile/updateCompanyTemplate",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/config/default-invoice-template",
    service: "/config/defaultInvoiceTemplate/updateInvoiceTemplate",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/config/payment-gateway-config",
    service: "/config/paymentGatewayConfig/insertKey",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/config/production-unit-config",
    service: "/config/productionUnitConfig/updatePUC",
    auth: true,
  },

  // ADMIN
  {
    type: "GET",
    endPoint: "/list/admin/payment-detail-transaction",
    service: "/admin/paymentGateway/listPaymentTransaction",
    auth: true,
  },

  // HELPER TESTING ENDPOINT
  {
    type: "POST",
    endPoint: "/test/midtrans/transaction",
    service: "/helper/transactionTesting/paymentMidtrans",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/test/notification-midtrans",
    service: "/helper/transactionTesting/notifikasiMidtrans",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/test/auto-generate-invoice",
    service: "/helper/transactionTesting/testInvoice",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/test/success-payment-verification",
    service: "/helper/transactionTesting/verifikasiPembayaran",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/invoice",
    service: "/invoice/createInvoice",
    auth: false,
  },
  {
    type: "GET",
    endPoint: "/test/invoice/invoice-payment",
    service: "/helper/transactionTesting/invoice/listInvoicePayment",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/test/company-profile",
    service: "/helper/configTesting/addCompanyTemplate",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/test/invoice-payment-detail",
    service: "/helper/transactionTesting/invoicePaymentDetail",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/test/invoice-template-subcribe",
    service: "/helper/configTesting/addInvoiceSubTemplate",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/test/invoice-template-installation",
    service: "/helper/configTesting/addInvoiceTemplate",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/test/payment-total",
    service: "/helper/transactionTesting/invoiceTotal",
    auth: true,
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/test/corporate-realization-plant",
    service: "/helper/realizationTesting/realizationPlant",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/master/production-unit-data",
    service: "/helper/masterTesting/showByPlant",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/list/master/corporate-data",
    service: "/helper/masterTesting/showByCompany",
    auth: true,
  },

  // ROUTE TESTING
  // ROLES
  // { type: "PATCH", endPoint: "/update/role", service: "/roles/updateRoles.js", auth: true, },
  // { type: "POST", endPoint: "/add-access/role", service: "/roles/addRoles.js", auth: true, },

  // MODULE
  // { type: "GET", endPoint: "/list/modules", service: "/module/listModule", auth: true, },
  // { type: "GET", endPoint: "/view/module-group", service: "/module/viewModuleGroup", auth: true, },
  // { type: "POST", endPoint: "/create/module", service: "/module/createRoleModule", auth: true, },
  // { type: "PATCH", endPoint: "/update/module", service: "/module/updateRoleModule", auth: true, },
  // { type: "GET", endPoint: "/view/module-task", service: "/module/viewModuleTask", auth: true, },

  // DELIVERY
  {
    type: "VIEW",
    endPoint: "/report/delivery",
    service: "/report/delivery/list",
    auth: true,
    task: "report-delivery",
  },

  // CORPORATE
  {
    type: "GET",
    endPoint: "/list/corporate",
    service: "/corporate/listCorporate",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/corporate/:id",
    service: "/corporate/viewCorporate",
    auth: true,
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/corporate",
    service: "/corporate/listCorporate",
    auth: false,
  },

  // PRODUCTION UNIT
  {
    type: "GET",
    endPoint: "/list/corporate/production-unit",
    service: "/corporate/production-unit/listProductionUnit",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/corporate/production-unit/:id",
    service: "/corporate/production-unit/viewProductionUnit",
    auth: true,
  },

  // PRODUCTION UNIT CONFIG
  {
    type: "GET",
    endPoint: "/list/corporate/production-unit/config",
    service: "/corporate/production-unit/listUnitConfig",
    auth: true,
  },
  {
    type: "POST",
    endPoint: "/create/corporate/production-unit/config",
    service: "/corporate/production-unit/addUnitConfig",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/corporate/production-unit/config",
    service: "/corporate/production-unit/updateUnitConfig",
    auth: true,
  },

  // PAYMENT CHANNEL

  {
    type: "POST",
    endPoint: "/create/payment/payment-channel",
    service: "/payment/payment-channel/addPaymentChannel",
    auth: true,
  },
  {
    type: "PATCH",
    endPoint: "/update/payment/payment-channel",
    service: "/payment/payment-channel/updatePaymentChannel",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/payment/payment-channel/:id",
    service: "/payment/payment-channel/viewPaymentChannel",
    auth: true,
  },

  // PAYMENT CHANNEL CATEGORY
  {
    type: "GET",
    endPoint: "/list/payment/payment-channel/category",
    service: "/payment/payment-channel-category/listChannelCategory",
    auth: true,
  },
  {
    type: "VIEW",
    endPoint: "/view/payment/payment-channel/category/:id",
    service: "/payment/payment-channel-category/viewChannelCategory",
    auth: true,
  },

  // DASHBOARD
  {
    type: "GET",
    endPoint: "/dashboard/statistic/subscription",
    service: "/dashboard/statistic/subscription",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/dashboard/statistic/production-trend",
    service: "/dashboard/statistic/productionTrend",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/dashboard/statistic/production-trend-chart",
    service: "/dashboard/statistic/productionTrendChart",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/dashboard/statistic/payment",
    service: "/dashboard/statistic/paymentStatus",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/dashboard/statistic/demographic-production-unit",
    service: "/dashboard/statistic/demographicProductionUnit",
    auth: true,
  },
  {
    type: "GET",
    endPoint: "/dashboard/production/history",
    service: "/dashboard/production/historyProduction",
    auth: true,
  },

  // PROFILE
  {
    type: "GET",
    endPoint: "/profile",
    service: "/profile/me",
    auth: true,
    task: "my-profile",
  },
  {
    type: "PATCH",
    endPoint: "/change/profile",
    service: "/profile/changeProfile",
    auth: true,
    task: "my-profile",
  },
  {
    type: "PATCH",
    endPoint: "/change/password",
    service: "/profile/changePassword",
    auth: true,
    task: "my-profile",
  },

  // ROLES
  {
    type: "GET",
    endPoint: "/list/role",
    service: "/setting/roles/list",
    auth: true,
    task: "roles",
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/role",
    service: "/setting/roles/list",
    auth: true,
    task: "roles",
  },
  {
    type: "POST",
    endPoint: "/create/role",
    service: "/setting/roles/create",
    auth: true,
    task: "roles",
  },
  {
    type: "PATCH",
    endPoint: "/update/role",
    service: "/setting/roles/update",
    auth: true,
    task: "roles",
  },
  {
    type: "VIEW",
    endPoint: "/view/role/:id",
    service: "/setting/roles/view",
    auth: true,
    task: "roles",
  },
  {
    type: "DELETE",
    endPoint: "/delete/role/:id",
    service: "/setting/roles/delete",
    auth: true,
    task: "roles",
  },

  // TASKS
  {
    type: "GET",
    endPoint: "/list/task",
    service: "/setting/tasks/list",
    auth: true,
    task: "tasks",
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/task",
    service: "/setting/tasks/list",
    auth: true,
    task: "tasks",
  },
  {
    type: "POST",
    endPoint: "/create/task",
    service: "/setting/tasks/create",
    auth: true,
    task: "tasks",
  },
  {
    type: "PATCH",
    endPoint: "/update/task",
    service: "/setting/tasks/update",
    auth: true,
    task: "tasks",
  },
  {
    type: "VIEW",
    endPoint: "/view/task/:id",
    service: "/setting/tasks/view",
    auth: true,
    task: "tasks",
  },
  {
    type: "DELETE",
    endPoint: "/delete/task/:id",
    service: "/setting/tasks/delete",
    auth: true,
    task: "tasks",
  },

  // MODULES
  {
    type: "GET",
    endPoint: "/list/module/all",
    service: "/setting/modules/listAll",
    auth: true,
    task: "modules",
  },
  {
    type: "GET",
    endPoint: "/list/module",
    service: "/setting/modules/list",
    auth: true,
    task: "modules",
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/module",
    service: "/setting/modules/list",
    auth: true,
    task: "modules",
  },
  {
    type: "POST",
    endPoint: "/create/module",
    service: "/setting/modules/create",
    auth: true,
    task: "modules",
  },
  {
    type: "PATCH",
    endPoint: "/update/module",
    service: "/setting/modules/update",
    auth: true,
    task: "modules",
  },
  {
    type: "VIEW",
    endPoint: "/view/module/:id",
    service: "/setting/modules/view",
    auth: true,
    task: "modules",
  },
  {
    type: "DELETE",
    endPoint: "/delete/module/:id",
    service: "/setting/modules/delete",
    auth: true,
    task: "modules",
  },

  // MODULE GROUPS
  {
    type: "GET",
    endPoint: "/list/module-group",
    service: "/setting/moduleGroup/list",
    auth: true,
    task: "module-groups",
  },
  {
    type: "LOOKUP",
    endPoint: "/lookup/module-group",
    service: "/setting/moduleGroup/list",
    auth: true,
    task: "module-groups",
  },
  {
    type: "POST",
    endPoint: "/create/module-group",
    service: "/setting/moduleGroup/create",
    auth: true,
    task: "module-groups",
  },
  {
    type: "PATCH",
    endPoint: "/update/module-group",
    service: "/setting/moduleGroup/update",
    auth: true,
    task: "module-groups",
  },
  {
    type: "VIEW",
    endPoint: "/view/module-group/:id",
    service: "/setting/moduleGroup/view",
    auth: true,
    task: "module-groups",
  },
  {
    type: "DELETE",
    endPoint: "/delete/module-group/:id",
    service: "/setting/moduleGroup/delete",
    auth: true,
    task: "module-groups",
  },

  // ROLES & PERMISSIONS
  // MODULE PERMISSIONS
  {
    type: "VIEW",
    endPoint: "/view/permissions/module/:module_id",
    service: "/setting/modulePermissions/view",
    auth: true,
    task: "module-permissions",
  },
  {
    type: "PATCH",
    endPoint: "/update/permissions/module",
    service: "/setting/modulePermissions/update",
    auth: true,
    task: "module-permissions",
  },

  // ROLE ACCESS MODULE
  {
    type: "VIEW",
    endPoint: "/view/role/access-module/:role_id",
    service: "/setting/roleAccess/view",
    auth: true,
    task: "role-access-module",
  },
  {
    type: "PATCH",
    endPoint: "/update/role/access-module",
    service: "/setting/roleAccess/update",
    auth: true,
    task: "role-access-module",
  },

  // USERS
  {
    type: "GET",
    endPoint: "/list/users",
    service: "/users/list",
    auth: true,
    task: "users",
  },
  {
    type: "VIEW",
    endPoint: "/view/users/:id",
    service: "/users/view",
    auth: true,
    task: "users",
  },
  {
    type: "POST",
    endPoint: "/create/users",
    service: "/users/create",
    auth: true,
    task: "users",
  },
  {
    type: "PATCH",
    endPoint: "/update/users",
    service: "/users/update",
    auth: true,
    task: "users",
  },

  // EXPORT
  {
    type: "EXPORT",
    endPoint: "/export/excel/:service",
    service: "/export/exportExcel",
    auth: true,
  },

  //#endregion END GENERIC ROUTE
];
module.exports = router;
