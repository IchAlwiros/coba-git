require("dotenv").config();
const { CoreResponse } = require("./core/CallService");
var express = require("express");
var path = require("path");
var cors = require("cors");
const morgan = require("morgan");
var app = express();
app.use(cors());
app.use(express.json());
app.use(morgan("tiny"));
const port = process.env.PORT || "7000";
var http = require("http");
var server = http.createServer(app);
const config = require("./config");
process.env = { ...process.env, config };

//SOCKET IO
var io = require("socket.io")(server);
io.on("connection", (socket) => {
  console.log(`SERVER RUNNING ON PORT ${port}`);
  console.log(`CLIENT SOCKET ${socket.id} CONNECTED`);
  io.emit("message", "Connect with server . . .");
});

// APP SERVER
// app.io = io
// server.listen(port, '0.0.0.0')
server.listen(port, process.env.PRODUCTION == "1" ? "localhost" : "0.0.0.0");
server.on("error", (onError) => {});
server.on("listening", (resp) => {
  console.log(`Server is running on port ${port}`);
});

var cron = require("node-cron");
cron.schedule(
  "0 0 0 * * *",
  () => {
    require("./cronJob/closeProdSchedule")();
  },
  {
    scheduled: true,
    timezone: "Asia/Jakarta",
  }
);

// CRON CALCULATE SUMMARY STOCK
cron.schedule(
  "1 0 0 1 * *",
  () => {
    require("./cronJob/calcSummaryStock")();
  },
  {
    scheduled: true,
    timezone: "Asia/Jakarta",
  }
);

app.use("/public", express.static(path.join(__dirname, "public")));
app.use("/tmp", express.static(path.join(__dirname, "tmp")));
// app.get("/hallo", (req, res) => {
//   let data = [{ nama: "str" }, { nama: "str" }, { nama: "str" }];
//   res.status(200).json(data);
// });
// REGISTER SERVICE
var commonRouter = require("./core/coreServiceProvider");
app.use("", commonRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  return CoreResponse.fail(
    res,
    "Page not found | Halaman tidak ditemukan",
    {},
    404
  );
});

// error handler
app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  return CoreResponse.fail(res, err.message, {}, err.status || 500);
});

global.is_blank = function (value) {
  return value === undefined || value == null || value == "";
};

module.exports = app;
