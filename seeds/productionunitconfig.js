const { Global } = require("../util/globalFunction");

exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("production_unit_config").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("production_unit_config").insert([
      {
        unit_price: 30000.0,
        installation_fee: 20000.0,
        tax: 1,
        billing_date: Global.currentDate(),
        tolerance: 5,
      },
      {
        unit_price: 40000.0,
        installation_fee: 25000.0,
        tax: 1,
        billing_date: Global.currentDate(),
        tolerance: 6,
      },
    ]);
    // Now that we have a clean slate, we can re-insert our paper data
    // Insert a single paper, return the paper ID, insert 2 footnotes
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
