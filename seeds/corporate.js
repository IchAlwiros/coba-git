exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("corporate").del()
  // Inserts seed entries
  await knex("corporate").insert([
    {
      uuid: "5cd980c7-9304-4bf6-8dae-0788f761365d",
      code: "123A",
      name: "PT Jaya Beton Readymix",
      phone: "08801252087",
      email: "info@jayabeton.com",
      address: "Jalan Raya Bogor KM. 27,5 No. 3, Cilangkap, Jakarta Timur, DKI Jakarta, Indonesia",
      registered_at: "2022-11-26 08:51:13",
      active: 0,
      created_at: knex.fn.now(),
      updated_at: knex.fn.now(),
    },
    {
      uuid: "adbb00b0-8ea9-4a2b-9d2e-4fd8f2d1a7db",
      code: "987B",
      name: "PT Indocement Tunggal Prakarsa Readymix",
      phone: "08884884266",
      email: "contact@indocement.co.id",
      address: "Jalan Raya Ragunan No. 51, Ragunan, Pasar Minggu, Jakarta Selatan, DKI Jakarta, Indonesia",
      registered_at: "2021-11-21 03:39:52",
      active: 0,
      created_at: knex.fn.now(),
      updated_at: knex.fn.now(),
    },
  ])
}
