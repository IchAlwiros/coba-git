exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("modules").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("modules").insert([
      {
        id: 1,
        code: "MODULE-1",
        name: "module satu",
        description: "module 1",
        active: 1,
        created_by: 1,
        updated_by: 1,
      },
      {
        id: 2,
        code: "MODULE-2",
        name: "module dua",
        description: "module 2",
        active: 1,
        created_by: 1,
        updated_by: 1,
      },
      {
        id: 3,
        code: "MODULE-3",
        name: "module tiga",
        description: "module 3",
        active: 1,
        created_by: 1,
        updated_by: 1,
      },
    ]);
    // Now that we have a clean slate, we can re-insert our paper data
    // Insert a single paper, return the paper ID, insert 2 footnotes
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
