/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("users").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("users").insert([
      {
        username: 'admin',
        email: 'admin@gmail.com',
        password: '$2a$10$F/tpQ1Vro/UJCAQO7nTBJuMgAidUuT2EplBvikv07ABglP55cji6.',
        name: 'Super Admin',
        role_id: '-1'
      },
      {
        name: "ichal",
        phone: "085853722837",
        gender: "L",
        role_id: 1,
        address: "Jl Dr Sutomo",
        email: "ichalwiradev@gmail",
        username: "ichalwira",
        password:
          "$2a$12$gRaAXYm/ZPm9Y0YYoy2YNuwXLWEqsJ1kxHsDYDD/S396NQ5hB/ahK",
        role_id: 1,
      },
      {
        name: "abdul",
        phone: "08333333333",
        gender: "L",
        role_id: 2,
        address: "Jl Brawijaya",
        email: "abdul@gmail",
        username: "abdul",
        password:
          "$2a$12$gRaAXYm/ZPm9Y0YYoy2YNuwXLWEqsJ1kxHsDYDD/S396NQ5hB/ahK",
        role_id: 1,
      },
      {
        name: "budi",
        phone: "0823647288",
        gender: "L",
        role_id: 2,
        address: "Jl Baru aja",
        email: "budi@gmail",
        username: "budi",
        password:
          "$2a$12$gRaAXYm/ZPm9Y0YYoy2YNuwXLWEqsJ1kxHsDYDD/S396NQ5hB/ahK",
        role_id: 1,
      },
    ]);
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
