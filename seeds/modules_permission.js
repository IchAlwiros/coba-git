exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("module_permissions").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("module_permissions").insert([
      {
        id: 1,
        action: "MODIFY",
      },
      {
        id: 2,
        action: "CREATED",
      },
      {
        id: 3,
        action: "DELETE",
      },
    ]);
    // Now that we have a clean slate, we can re-insert our paper data
    // Insert a single paper, return the paper ID, insert 2 footnotes
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
