
exports.seed = async function (knex) {
  try {
    // Deletes ALL existing entries first
    await knex('payment_channel_category').del()
    // Inserts seed entries
    await knex('payment_channel_category').insert([
      {
        id: 1,
        code: "CASH",
        name: "CASH",
        description: "Pembayaran tunai atau cash pakai ini",
        active: 1,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
      },
      {
        id: 2,
        code: "BANK-TRANSFER",
        name: "BANK TRANSFER",
        description: "Pembayaran via transfer bank",
        active: 1,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
      },
      {
        id: 3,
        code: "PAYMENT-GATEWAY",
        name: "PAYMENT GATEWAY",
        description: "Melalui pihak ketiga seperti Midtrans, Payce, Doku, dll",
        active: 1,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
      },
    ])

  } catch (error) {
    console.log("Error seeding data:", error);
  }
};
