exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("module_groups").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("module_groups").insert([
      {
        id: 1,
        code: "DASHBOARD",
        name: "Dashboard",
        description: "Berisi statistik data",
      },
      {
        id: 2,
        code: "MASTER-DATA",
        name: "Master Data",
        description: null,
      },
      {
        id: 3,
        code: "REALISASI",
        name: "Realisasi",
        description: null,
      },
      {
        id: 4,
        code: "TRANSAKSI",
        name: "Transaksi",
        description: null,
      },
      {
        id: 5,
        code: "CONFIG",
        name: "Config",
        description: null,
      },
      {
        id: 6,
        code: "ADMIN",
        name: "Admin",
        description: null,
      },
    ]);
    // Now that we have a clean slate, we can re-insert our paper data
    // Insert a single paper, return the paper ID, insert 2 footnotes
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
