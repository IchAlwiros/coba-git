
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('invoice_category').del()
  // Inserts seed entries
  await knex('invoice_category').insert([
    {
      id: 1,
      code: 'INSTALLATION',
      name: 'INSTALLATION',
      description: null,
      active: 1,
      created_at: knex.fn.now(),
      created_by: 1,
      updated_at: knex.fn.now(),
      updated_by: 1
    },
    {
      id: 2,
      code: 'SUBSCRIBE',
      name: 'SUBSCRIBE',
      description: null,
      active: 1,
      created_at: knex.fn.now(),
      created_by: 1,
      updated_at: knex.fn.now(),
      updated_by: 1
    }
  ]);
};
