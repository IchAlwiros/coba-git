
exports.seed = async function (knex) {
  try {
    // Deletes ALL existing entries
    await knex('payment_channel').del()
    // Inserts seed entries
    await knex('payment_channel').insert([
      {
        id: 1,
        uuid : "bdd23490-584a-4c0b-b3c9-812c12647fc2",
        payment_channel_category_id: 1,
        name: "Cash via Indomaret",
        bank_code: "123",
        bank_name: "BCA",
        account_name: "Udin Sedunia",
        account_number: "5145146952558019",
        admin_fee: 0,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
        active: 0,
      },
      {
        id: 2,
        uuid: "a61f7a63-13f2-46fd-998a-09b3e5e64be9",
        payment_channel_category_id: 2,
        name: "Credit Card BNI",
        bank_code: "009",
        bank_name: "BNI",
        account_name: "RBS Internal",
        account_number: "5450745708879410",
        admin_fee: 6500,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
        active: 1,
      },
      {
        id: 3,
        uuid: "f32e7b12-8e20-4d11-b118-9a4c1ef8905d",
        payment_channel_category_id: 3,
        name: "MidBank",
        bank_code: "085",
        bank_name: "MidBank",
        account_name: "Adi Karya",
        account_number: "9876543210",
        admin_fee: 2500,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
      },
    ]);
    
  } catch (error) {
    console.log("Error seeding data : ", error);
  }
};
