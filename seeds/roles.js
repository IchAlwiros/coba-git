exports.seed = async function (knex) {
  try {
    await knex("roles").del();
    let roles = [
      {
        id: -1,
        name: 'SUPER-ADMIN',
        code: 'SUPER-ADMIN'
      },
      {
        id: 0,
        name: 'GUEST',
        code: 'GUEST'
      },
      {
        id: 1,
        name: 'ADMIN',
        code: 'ADMIN'
      },
      {
        id: 2,
        name: 'DIRECTUR',
        code: 'DIRECTUR'
      },
      {
        id: 3,
        name: 'OPERATIONAL',
        code: 'OPERATIONAL'
      },
      {
        id: 4,
        name: 'FINANCE',
        code: 'FINANCE'
      },
    ]
    await knex("roles").insert(roles)
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
