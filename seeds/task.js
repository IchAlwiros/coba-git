exports.seed = async function (knex) {
  // Deletes ALL existing entries
  try {
    await knex("tasks").del(); // delete all footnotes first
    // await knex("papers").del(); // delete all papers
    await knex("tasks").insert([
      {
        id: 1,
        code: "TK-1",
        name: "Task-1",
      },
      {
        id: 2,
        code: "TK-2",
        name: "Task-2",
      },
      {
        id: 3,
        code: "TK-3",
        name: "Task-3",
      },
    ]);
    // Now that we have a clean slate, we can re-insert our paper data
    // Insert a single paper, return the paper ID, insert 2 footnotes
  } catch (error) {
    console.log(`Error seeding data: ${error}`);
  }
};
