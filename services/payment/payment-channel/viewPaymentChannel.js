const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")

/**
 * VIEW PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    return request.params
  },

  prepare: async function (input, db) {
    return input
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT A.id, A.uuid, A.payment_channel_category_id, B.name AS payment_channel_category_name, A.name, A.bank_code, A.bank_name, A.account_name, A.account_number, A.admin_fee, A.active 
               FROM payment_channel A 
               INNER JOIN payment_channel_category B
               ON A.payment_channel_category_id = B.id
               WHERE A.id = ?`
    let data = await db.row(sql, [input.id])
    if (!data) throw new CoreException("Channel Categoy not found|Kategori Channel Pembayaran tidak ditemukan")

    return data
  },
  validation: {
    id: "integer|required",
  },
}

module.exports = CoreService(service)
