const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")

/**
 * UPDATE PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    let input = request.body
    return input
  },

  prepare: async function (input, db) {
    // SET SESSION
    input.currentUserId = input.session.user_id

    // CHECK payment_channel by ID
    let sql = `SELECT * FROM payment_channel A
               WHERE A.id = ?`
    let isExist = await db.row(sql, input.id)

    if (!isExist) throw new CoreException("Payment Channel not found | Channel Pembayaran tidak ditemukan")

    // SET updated_at AND updated_by
    input.updated_at = Global.currentDateTime()
    input.updated_by = input.currentUserId
    return input
  },

  process: async function (input, OriginalInput, db) {
    const updateValue = {
      payment_channel_category_id: input.payment_channel_category_id,
      name: input.name,
      bank_code: input.bank_code,
      bank_name: input.bank_name,
      account_name: input.account_name,
      account_number: input.account_number,
      admin_fee: input.admin_fee,
      icon: input.icon,
      active: input.active,
      updated_at: input.updated_at,
      updated_by: input.updated_by
    }

    // Now, its time to update data in table payment_channel
    let result = await db.run_update("payment_channel", updateValue, { id: input.id })
    return result
  },
  validation: {
    name: "string",
    bank_code: "string",
    bank_name: "string",
    account_name: "string",
    account_number: "string",
    admin_fee: "decimal",
    active: "boolean",
  },
}

module.exports = CoreService(service)
