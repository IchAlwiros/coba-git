const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")
const { v4: uuid } = require("uuid")

/**
 * CREATE PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    let input = request.body
    return input
  },

  prepare: async function (input, db) {
    // SET SESSION
    input.currentUserId = input.session.user_id

    // CHECK existing data in table payment_channel
    let sql = `SELECT A.* FROM payment_channel A
               WHERE account_number = ?
               AND payment_channel_category_id = ?
               AND bank_name = ?`

    let paymentChannel = await db.row(sql, [input.account_number, input.payment_channel_category_id, input.bank_name])

    if (paymentChannel) throw new CoreException("Payment Channel already exist | Channel Pembayaran sudah ada")

    // GENERATE UUID
    input.uuid = uuid()

    // SET input.active with DEFAULT true
    input.active = input.active == null ? 1 : input.active

    // SET created_at, updated_at,
    input.created_at = Global.currentDateTime()
    input.updated_at = Global.currentDateTime()
    // AND updated_by
    input.created_by = input.currentUserId
    return input
  },

  process: async function (input, OriginalInput, db) {
    const insertValue = {
      uuid: input.uuid,
      payment_channel_category_id: input.payment_channel_category_id,
      name: input.name,
      bank_code: input.bank_code,
      bank_name: input.bank_name,
      account_name: input.account_name,
      account_number: input.account_number,
      admin_fee: input.admin_fee,
      icon: input.icon,
      active: input.active,
      created_at: input.created_at,
      updated_at: input.updated_at,
      created_by: input.created_by,
      updated_by: input.updated_by, // DEFAULT is null
    }

    // Now, its time to insert data to table payment_channel
    let data = await db.run_insert("payment_channel", insertValue)

    return data
  },
  validation: {
    payment_channel_category_id: "integer|required",
    name: "string",
    bank_code: "string",
    bank_name: "string",
    account_name: "string",
    account_number: "string",
    admin_fee: "decimal",
    icon: "string",
    active: "boolean",
  },
}

module.exports = CoreService(service)
