const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * VIEW CHANNEL CATEGORY
 */

const service = {
  input: function (request) {
    return request.params;
  },

  prepare: async function (input, db) {
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT A.id, A.code, A.name, A.description FROM payment_channel_category A
    WHERE A.id = ?`;
    let data = await db.row(sql, [input.id]);
    if (!data) throw new CoreException("Channel Categoy not found|Kategori Channel Pembayaran tidak ditemukan");

    return data
  },
  validation: {
    id: "integer",
  },
};

module.exports = CoreService(service);
