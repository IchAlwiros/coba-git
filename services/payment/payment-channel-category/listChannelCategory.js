const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")

/**
 * LIST PAYMENT CHANNEL CATEGORY
 */

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    // SET FILTER
    input.filter = []
    input.filterValue = []

    input.searchField = []
    input.searchValue = []

    // CONFIG
    let filter = [
      {
        field: "active",
        alias: "B",
        search: true,
        order: true,
        filter: true,
      },
    ]
    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "B.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""
    return input
  },

  process: async function (input, OriginalInput, db) {
    let columnName = `B.id, B.code, B.name, B.description, B.active`
    let sql = `SELECT ${columnName} FROM payment_channel_category B ${input.filter} 
               ORDER BY ${input.orderBy} 
               LIMIT ${input.limit} 
               OFFSET ${input.offset}`

    // run_select untuk return array
    let data = await db.run_select(sql, input.filterValue)

    // COUNT
    var sql2 = `SELECT COUNT(B.*) AS record
                FROM payment_channel_category B ${input.filter}`
    // row untuk return object
    let record = await db.row(sql2, input.filterValue)

    return {
      data: data,
      record: record ? record.record : 0,
    }
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    orderBy: "string",
  },
}

module.exports = CoreService(service)
