const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");

/**
 * Update Production Unit Config
 */

const service = {
  input: function (request) {
    let input = request.body;
    return input;
  },

  prepare: async function (input, db) {
    let { production_unit_id, unit_price, tax, tolerance, billing_date } =
      input;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { production_unit_id, unit_price, tax, tolerance, billing_date } =
      input;

    let dataUpdate = {
      unit_price,
      tax,
      tolerance,
      billing_date,
    };

    let update = await db.run_update(`production_unit_config`, dataUpdate, {
      production_unit_id,
    });

    if (update) {
      return {
        message: "production unit update successfully",
      };
    } else {
      throw new CoreException(
        "Update production unit config failed | production unit config gagal diperbarui"
      );
    }
  },

  validation: {
    unit_price: "required",
    tax: "required",
    tolerance: "required|integer",
    billing_date: "required",
  },
};

module.exports = CoreService(service);
