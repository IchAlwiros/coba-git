const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");

/**
 * Update & Create PAYMENT GATEWAY
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT pgc.* 
                FROM payment_gateway_config pgc 
                WHERE pgc.secret_key = ? OR pgc.client_key = ?`;
    let exitsSecret = await db.run_select(sql, [
      input.secret_key,
      input.client_key,
    ]);

    input.last_secret = exitsSecret;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { last_secret, secret_key, client_key, ip_address } = input;

    if (last_secret.length == 0) {
      ip_address = ip_address.join(",");
      let insertValue = {
        ip_address,
        secret_key,
        client_key,
      };
      let insert = await db.run_insert("payment_gateway_config", insertValue);

      return insert;
    } else {
      // TO GET CURRENT ID
      let current_secret_key = last_secret.filter(
        (el) => (el.secret_key = secret_key)
      );

      //   CURRENT ID
      let current_id = current_secret_key.map((el) => el.id);

      ip_address = ip_address.join(",");

      let updateValue = {
        ip_address,
        secret_key,
        client_key,
      };
      let result = {};
      for (const el of current_id) {
        let update = await db.run_update(
          "payment_gateway_config",
          updateValue,
          {
            id: el,
          }
        );

        result = { ...update };
      }

      return result;
    }
  },

  validation: {
    ip_address: "required|array",
    "ip_address.*": "ip",
    secret_key: "required|string",
    client_key: "required|string",
  },
};

module.exports = CoreService(service);
