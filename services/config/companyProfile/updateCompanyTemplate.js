const { CoreService, CoreException } = require("../../../core/CallService");
/**
 * Update Template Company
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    input.profile_id = input.profile_id || null;

    let sql = `SELECT * 
                FROM company_profile cp 
                WHERE cp.name = ? OR cp.id = ?`;

    let last_data = await db.row(sql, [input.name, input.profile_id]);

    input.last_data = last_data;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      profile_id,
      name,
      logo,
      tagline,
      address,
      email,
      suip_number,
      tdp_number,
      npwp_number,
      pic_name,
      pic_position,
      active,
      currentUserId,
    } = input;

    // INITIAL UPDATE INSERT
    const updateValue = {
      name,
      logo: logo || input.last_data.logo,
      tagline: tagline || input.last_data.tagline,
      address: address || input.last_data.address,
      email: email || input.last_data.email,
      suip_number: suip_number || input.last_data.suip_number,
      tdp_number: tdp_number || input.last_data.tdp_number,
      npwp_number: npwp_number || input.last_data.npwp_number,
      pic_name: pic_name || input.last_data.pic_name,
      pic_position: pic_position || input.last_data.pic_position,
      active: active || input.last_data.active,
      updated_by: currentUserId,
    };
    // UPDATE COMPANY PROFILE
    let data = await db.run_update("company_profile", updateValue, {
      name: name,
    });

    return data;
  },

  validation: {
    profile_id: "integer",
    name: "required|string",
    logo: "string",
    tagline: "string",
    address: "string",
    email: "email",
    suip_number: "string",
    tdp_number: "string",
    npwp_number: "string",
    pic_name: "string",
    pic_position: "string",
    tagline: "string",
    active: "string",
  },
};

module.exports = CoreService(service);
