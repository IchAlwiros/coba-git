const { CoreService, CoreException } = require("../../../core/CallService");
const { Extend } = require("../../../util/extendFunction");
const { Global } = require("../../../util/globalFunction");

/**
 * Update Template Company
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql1 = `SELECT * 
                FROM invoice_category ic 
                WHERE ic.id = ?`;

    let invoice_category = await db.row(sql1, [input.invoice_category_id]);

    if (!invoice_category) {
      throw new CoreException(
        "invoice category not found | invoice category tidak tersedia"
      );
    }

    let sql2 = `SELECT * 
                FROM default_invoice_config dic 
                WHERE dic.invoice_category_id = ?`;

    let last_data = await db.row(sql2, [input.invoice_category_id]);

    input.last_data = last_data;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      invoice_category_id,
      payment_channel_id,
      format_number,
      tax,
      price,
      tolerance,
      currentUserId,
    } = input;

    payment_channel_id =
      payment_channel_id == 0
        ? input.last_data.payment_channel_id
        : payment_channel_id;
    price = price || input.last_data.price;

    // UPDATE DATA
    const updateValue = {
      payment_channel_id,
      format_number: Extend.generateTemplateNumberInvoice(format_number),
      price,
      tolerance: tolerance || 30,
      tax: tax || "1",
      updated_by: currentUserId,
      updated_at: Global.currentDate(),
    };

    console.log(updateValue);
    // UPDATE COMPANY PROFILE
    let data = await db.run_update("default_invoice_config", updateValue, {
      invoice_category_id,
    });
    return data;
  },

  validation: {
    invoice_category_id: "required|integer",
    payment_channel_id: "required|integer",
    format_number: "string",
    tolerance: "integer",
    tax: "integer",
  },
};

module.exports = CoreService(service);
