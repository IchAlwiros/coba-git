const { CoreService, CoreException } = require("../../../core/CallService");
const { Extend } = require("../../../util/extendFunction");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * Statistic Production Unit
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.limit = input.limit ? input.limit : 20;
    input.offset = input.offset ? input.offset : 0;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "c",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { date } = input;

    const masterCompany = async () => {
      let sql = `SELECT pu.uuid , pu.id production_unit_id, pu."name" as plant, c.name, pu.phone, pu.address, MAX(pu.subscription_code) AS subscription_code
                  FROM corporate c
                  LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                  ${input.corporate_id ? input.filter : ``}
                  GROUP BY pu.uuid, pu.id, pu."name", c.name, pu.phone, pu.address`;

      let dataCompany = input.corporate_id
        ? await db.run_select(sql, input.filterValue)
        : await db.run_select(sql);

      return dataCompany;
    };

    let byCompany = await masterCompany();

    if (byCompany.length == 0) {
      throw new CoreException("corporate not found | corporate tidak tersedia");
    }

    // TO JOIN DATA FROM RBS & DATA TABLE
    const tampungData = (item, el) => {
      let dataInvoice = {};
      let total_success, total_cancel, total_order, total_schedule;
      total_success = item.total_success;
      total_cancel = item.total_cancel;
      total_order = item.total_order;
      total_schedule = item.total_schedule;

      return (dataInvoice = {
        ...dataInvoice,
        uuid: el.uuid,
        production_unit_id: el.production_unit_id,
        corporate_name: el.name,
        production_unit_name: el.plant,
        start_periode: date[0],
        end_periode: date[1],
        production_unit_contact: el.phone,
        production_unit_address: el.address,
        total_plan: el.total_production_units,
        total_order: total_order,
        total_schedule: total_schedule,
        production_success: total_success,
        production_cancel: total_cancel,
        subscription_status: el.subscription_code,
      });
    };

    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;

    // TO GET RBS DATA FROM EL UUID CORPORATE
    const fetchData = async () => {
      const results = [];

      for (const el of byCompany) {
        try {
          let endPoint = `/support/summary/production-by-plant/${el.uuid}?date[]=${date[0]}&date[]=${date[1]}`;

          const response = await Extend.fetchAPI(`${baseUrl}${endPoint}`);

          let datas = tampungData(response.data, el);

          results.push(datas);
        } catch (err) {
          throw new CoreException("something wrong | ada sesuatu yang salah");
        }
      }

      const totalObject = results.reduce(
        (acc, curr) => {
          acc.total_order += curr.total_order || 0;
          acc.production_success += curr.production_success || 0;
          acc.production_cancel += curr.production_cancel || 0;
          acc.total_schedule += curr.total_schedule || 0;
          return acc;
        },
        {
          total_order: 0,
          total_schedule: 0,
          production_success: 0,
          production_cancel: 0,
        }
      );

      let toPersetance = {
        percentage_schedule: totalObject.total_schedule,
        percentage_production_success: totalObject.production_success,
        percentage_production_cancel: totalObject.production_cancel,
      };

      let total = Object.values(toPersetance).reduce(
        (sum, value) => sum + value,
        0
      );

      let percentages = {};

      for (let key in toPersetance) {
        percentages[key] = (toPersetance[key] / total) * 100;
        percentages[key] = parseFloat(percentages[key].toFixed(2));
      }

      let sql = `SELECT COUNT(pu.*) as record
                   FROM production_unit pu 
                   LEFT JOIN corporate c ON pu.corporate_id = c.id 
                   ${input.corporate_id ? input.filter : ``}
                   `;

      let record = input.corporate_id
        ? await db.row(sql, input.filterValue)
        : await db.row(sql);

      let customStructure = {
        data: [...results],
        record: record ? record.record : 0,
        summary: { ...totalObject, ...percentages },
      };

      return { ...totalObject, ...percentages };
    };

    // RETURNING THE RESULT FROM THE DATA FECTH FUNCTION
    return fetchData()
      .then(async (data) => {
        return data;
      })
      .catch((error) => {
        throw new CoreException("something wrong | ada sesuatu yang salah");
      });
  },

  validation: {
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
