const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * List Summary Detail Company
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "c",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { date } = input;
    // FUNSI UNTUK MENDAPATKAN DATA CORPORATE YANG DI GABUNGKAN
    const masterCompany = async () => {
      let sql = `SELECT pu.uuid, CASE WHEN pu.active ='1' THEN TRUE ELSE FALSE END AS active , pu.pic_name, puc.unit_price, pu.pic_phone ,c.id , pu."name" as plant, c.name, pu.address, MAX(pu.subscription_code) AS subscription_code
                  FROM corporate c
                  LEFT JOIN production_unit pu ON pu.corporate_id = c.id left join production_unit_config puc on puc.production_unit_id = pu.id 
                  ${input.filter}
                  GROUP BY pu.uuid,pu.active , pu.pic_name, puc.unit_price,pu.pic_phone, c.id ,pu."name", c.name, pu.address
                  LIMIT ${input.limit} OFFSET ${input.offset};`;
      let dataCompany = await db
        .run_select(sql, input.filterValue)
        .then((result) => {
          return result;
        })
        .catch((e) => {
          throw new CoreException("something wrong | ada sesuatu yang salah");
        });

      return dataCompany;
    };

    let byCompany = await masterCompany();

    // UNTUK MENGGABUNGKAN DATA DARI RBS & DATA TABLE
    const tampungData = (item, el) => {
      let dataInvoice = {};
      let total_success, total_cancel, total_order, total_schedule;
      total_success = item.total_success;
      total_cancel = item.total_cancel;
      total_order = item.total_order;
      total_schedule = item.total_schedule;
      //   console.log(el);
      return (dataInvoice = {
        ...dataInvoice,
        production_unit: el.plant,
        production_unit_price: el.unit_price,
        production_unit_pic: el.pic_name,
        production_unit_contact: el.pic_phone,
        production_unit_address: el.address,
        total_production_unit: el.total_production_units,
        total_schedule: total_schedule,
        production_success: total_success,
        active: el.active,
      });
    };

    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;

    // UNTUK MENDAPATKAN DATA RBS YANG BERACUAN DARI EL UUID CORPORATE
    const fetchData = async () => {
      const results = [];

      for (const el of byCompany) {
        try {
          let endPoint = `/support/summary/production-by-plant/${el.uuid}?date[]=${date[0]}&date[]=${date[1]}`;

          const response = await Extend.fetchAPI(`${baseUrl}${endPoint}`);

          let datas = tampungData(response.data, el);

          results.push(datas);
        } catch (err) {
          throw new CoreException("something wrong | ada sesuatu yang salah");
        }
      }

      return results;
    };

    // MENGEMBALIKAN HASIL DARI FUNGSI FECTH DATA
    return fetchData()
      .then(async (data) => {
        // COUNT
        var sql2 = `SELECT COUNT(c.*) AS record
                    FROM corporate c
                    LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                    LEFT JOIN production_unit_config puc on puc.production_unit_id = pu.id 
                    ${input.filter}
                    GROUP BY c.*`;

        let record = await db.row(sql2, input.filterValue);

        return {
          data,
          record: record ? record.record : 0,
        };
      })
      .catch((error) => {
        throw new CoreException("something wrong | ada sesuatu yang salah");
      });
  },

  validation: {
    corporate_id: "integer",
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
