const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * View Detail Plant Unit
 */

const service = {
  input: function (request) {
    let input = request.params;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "pu",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" OR ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // CORPORATE ALL INVOICE BY PLANT
    let sql = `SELECT pu.id production_unit_id, c.id corporate_id, pu."name" production_unit_name, c."name" corporate_name, c.email corporate_email, pu.pic_name production_unit_pic_name, pu.pic_phone production_unit_pic_contact, pu.address production_unit_address, pu.subscription_code 
                FROM production_unit pu 
                INNER join corporate c ON c.id = pu.corporate_id 
                ${input.filter}`;
    const invoice = await db.row(sql, input.filterValue);
    return invoice;
  },

  validation: {
    production_unit_id: "integer",
  },
};

module.exports = CoreService(service);
