const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const { v4: uuid } = require("uuid");

/**
 * Service * PRODUCTION UNIT CREATE*
 */

const service = {
  input: function (request) {
    return request.body;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    input.currentDateTime = Global.currentDateTime();

    var sql = ` SELECT A.id, A.code,A.name
                FROM production_unit A WHERE A.id = ?`;
    let data = await db.row(sql, [input.production_unit_id]);
    if (!data) throw new CoreException(`data unavailable|data tidak tersedia`);

    let check = await db.row(
      `SELECT*FROM corporate WHERE name = ? AND code = ?`,
      [input.name, input.code]
    );

    if (check) {
      if (check.name == input.name)
        throw new CoreException(`Name has been taken|nama telah terdaftar`);
      if (check.code == input.code)
        throw new CoreException(`code already taken|code telah terdaftar`);
    }

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let updateValue = {
      name: input.name,
      phone: input.phone,
      email: input.email,
      address: input.address,
      pic_name: input.pic_name,
      pic_phone: input.pic_phone,
      pic_position: input.pic_position,
      active: "1",
      updated_at: input.currentDateTime,
      updated_by: input.currentUserId,
    };
    await db
      .run_update(`production_unit`, updateValue, {
        id: input.production_unit_id,
      })
      .catch((err) => {
        console.log(err);
      });

    return {
      message: "Data saved successfully",
      data: {
        name: input.name,
        phone: input.phone,
        email: input.email,
        address: input.address,
        pic_name: input.pic_name,
        pic_phone: input.pic_phone,
        pic_position: input.pic_position,
      },
    };
  },
  validation: {
    name: "required",
    phone: "required|phone",
    email: "required|email",
    address: "string",
    code: "string",
  },
};

module.exports = CoreService(service);
