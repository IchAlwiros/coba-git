const { CoreService, CoreException } = require("../../../core/CallService");

/**
 * Service * DELETED CORPORATE*
 */

const service = {
  input: function (request) {
    return request.params;
  },

  prepare: async function (input, db) {
    var sql = ` SELECT A.id, A.code,A.name
                    FROM production_unit A WHERE A.id = ?`;
    let data = await db.row(sql, [input.production_unit_id]);
    if (!data) throw new CoreException(`data unavailable|data tidak tersedia`);
    return input;
  },

  process: async function (input, OriginalInput, db) {
    await db.run_delete(`production_unit`, { id: input.production_unit_id });
    return {
      message: "Data deleted successfully",
      data: {
        id: input.production_unit_id,
      },
    };
  },
  validation: {
    production_unit_id: "required|integer",
  },
};

module.exports = CoreService(service);
