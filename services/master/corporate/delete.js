const { CoreService, CoreException } = require("../../../core/CallService");

/**
 * Service * DELETED CORPORATE*
 */

const service = {
  input: function (request) {
    return request.params;
  },

  prepare: async function (input, db) {
    var sql = ` SELECT A.id, A.code,A.name
                    FROM corporate A WHERE A.id = ?`;
    let data = await db.row(sql, [input.corporate_id]);
    if (!data) throw new CoreException(`data unavailable|data tidak tersedia`);
    return input;
  },

  process: async function (input, OriginalInput, db) {
    console.log(input.corporate_id);
    await db.run_delete(`corporate`, { id: input.corporate_id });
    return {
      message: "Data deleted successfully",
      data: {
        id: input.corporate_id,
      },
    };
  },
  validation: {
    corporate_id: "required|integer",
  },
};

module.exports = CoreService(service);
