const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 *  Detail Company Profile
 */

const service = {
  input: function (request) {
    let input = request.params;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];
    let filter = [
      {
        field: "id",
        alias: "c",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT c.id corporate_id,c.name corporate_name, c.email corporate_email, c.phone corporate_phone, c.address corporate_address, c.logo_preview corporate_logo
               FROM corporate c ${input.filter}`;
    let data = await db.run_select(sql, input.filterValue);

    return data[0];
  },

  validation: {
    corporate_id: "required|integer",
  },
};

module.exports = CoreService(service);
