const { CoreService, CoreException } = require("../../../core/CallService");
const { Extend } = require("../../../util/extendFunction");

/**
 * List Corporate Realization
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.limit = input.limit ? input.limit : 20;
    input.offset = input.offset ? input.offset : 0;

    const masterCompany = async () => {
      let sql = `SELECT c.uuid, c.id corporate_id, c.name, c.phone, c.address, COUNT(pu.id) AS total_production_units, MAX(pu.subscription_code) AS subscription_code
                  FROM corporate c
                  LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                  GROUP BY c.uuid, c.id, c.name, c.phone, c.address
                  LIMIT ${input.limit} OFFSET ${input.offset};`;

      let dataCompany = await db
        .run_select(sql)
        .then((result) => {
          return result;
        })
        .catch((e) => {
          throw new CoreException("something wrong | terjadi kesalahan");
        });

      return dataCompany;
    };

    let byCompany = await masterCompany();

    input.company_data = byCompany;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { date } = input;

    // TO COMBINE DATA FROM RBS & DATA TABLES
    const tampungData = (item, el) => {
      let dataInvoice = {};
      let total_success,
        total_cancel,
        total_order,
        total_schedule,
        average_production_success;
      total_success = item.total_success;
      total_cancel = item.total_cancel;
      total_order = item.total_order;
      total_schedule = item.total_schedule;

      average_production_success =
        item.total_success / el.total_production_units;

      return (dataInvoice = {
        ...dataInvoice,
        uuid: el.uuid,
        corporate_id: el.corporate_id,
        corporate_name: el.name,
        corporate_contact: el.phone,
        corporate_address: el.address,
        total_production_unit: el.total_production_units,
        average_production_success,
        total_order: total_order,
        total_schedule: total_schedule,
        production_success: total_success,
        production_cancel: total_cancel,
        subscription_status: el.subscription_code,
      });
    };

    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;

    // TO GET RBS REFERENCE DATA FROM EL UUID CORPORATE
    const fetchData = async () => {
      const results = [];
      for (const el of input.company_data) {
        try {
          let endPoint = `/support/summary/production-by-corporate/${el.uuid}?date[]=${date[0]}&date[]=${date[1]}`;

          const response = await Extend.fetchAPI(`${baseUrl}${endPoint}`);

          let datas = tampungData(response.data, el);

          results.push(datas);
        } catch (err) {
          throw new CoreException("something wrong | terjadi kesalahan");
        }
      }

      return results;
    };

    // RETURNING THE RESULT FROM THE DATA FECTH FUNCTION
    return fetchData()
      .then(async (data) => {
        // COUNT
        var sql2 = `SELECT COUNT(c.*) AS record
                    FROM corporate c
                    LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                    GROUP BY c.*`;

        let record = await db.row(sql2);

        return {
          data,
          record: record ? record.record : 0,
        };
      })
      .catch((error) => {
        throw new CoreException("something wrong | terjadi kesalahan");
      });
  },

  validation: {
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
