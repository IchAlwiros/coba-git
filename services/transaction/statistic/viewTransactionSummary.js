const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * View Transaction Summary
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.body = request.body;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    let { filterDinamis } = Extend;

    // SET FILTER
    if (!input.status) {
      input.filter = [
        `i.invoice_category_id = ? OR i.invoice_category_id = ? `,
      ];
      input.filterValue = [1, 2];
    } else {
      // FUNTION TO HENDLE FIND CATEGORY INVOICE
      const selectCategoryInvoice = async (status) => {
        let dinamic = filterDinamis([`ic."name" = ?`], [status]);
        let sql = `select ic.id , ic."name", ic.code from invoice_category ic ${dinamic.filter}`;
        const unit_config = await db
          .row(sql, dinamic.filterValue)
          .then((result) => {
            return result;
          })
          .catch((err) => {
            throw new CoreException("something wrong | terjadi kesalahan");
          });
        return unit_config;
      };

      let dataCategory = await selectCategoryInvoice(input.status);
      input.category = dataCategory.name;
      input.filter = [`i.invoice_category_id = ?`];
      input.filterValue = [dataCategory.id];
    }
    let filter = [
      {
        field: "corporate_id",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "created_at",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // CORPORATE SUBCRIBE INVOICE
    let invoceSubcribe = `SELECT 
      i.id, ip.receipt_number, c."name" corporate_name , pu.name production_unit_name,ic.name invoice_category, i.unit_price production_unit_price, i.tax_amount, ip.amount, ip.paid_at due_date, ip.status_code payment_status
      FROM invoice_payment ip
      LEFT JOIN invoice i ON i.id = ip.invoice_id
      INNER JOIN invoice_category ic ON ic.id = i.invoice_category_id
      LEFT JOIN corporate c ON c.id = i.corporate_id
      LEFT JOIN production_unit pu ON i.production_unit_id = pu.id
      ${input.filter}
      GROUP BY i.id, ip.receipt_number,c."name", pu.name, ic.name , i.unit_price, i.tax_amount, ip.amount, ip.paid_at, ip.status_code
      LIMIT ${input.limit} OFFSET ${input.offset};`;

    const invoice = await db.run_select(invoceSubcribe, input.filterValue);

    // COUNT
    var sql2 = `SELECT COUNT(ip.*) AS record FROM invoice_payment ip
                LEFT JOIN invoice i ON i.id = ip.invoice_id
                LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id
                LEFT JOIN corporate c ON c.id = i.corporate_id
                ${input.filter}`;

    let record = await db.row(sql2, input.filterValue);

    let groupedData = {};
    invoice.forEach((item) => {
      let status = item.invoice_category;
      if (!groupedData[status]) {
        groupedData[status] = {
          status_installation: status,
          transaction_pending: 0,
          total_amount_pending: 0,
          transaction_success: 0,
          total_amount_success: 0,
          transaction_expired: 0,
          total_amount_expired: 0,
          total_transaction: 0,
          total_amount_transaction: 0,
        };
      }
      let group = groupedData[status];
      group.total_transaction++;
      group.total_amount_transaction += item.amount;

      switch (item.payment_status) {
        case "PENDING":
          group.transaction_pending++;
          group.total_amount_pending += item.amount;
          break;
        case "SUCCESS":
          group.transaction_success++;
          group.total_amount_success += item.amount;
          break;
        case "EXPIRED":
          group.transaction_expired++;
          group.total_amount_expired += item.amount;
          break;
      }
    });

    let output = Object.values(groupedData);

    //   let response = {
    //     data: invoice,
    //     record: record ? record.record : 0,
    //     summary,
    //   };

    return output;
  },

  validation: {
    corporate_id: "integer",
    date: "required|array",
    "date.*": "date",
  },
};

module.exports = CoreService(service);
