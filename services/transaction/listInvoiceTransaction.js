const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");
const { Extend } = require("../../util/extendFunction");

/**
 * List Invoice Subcription & Installation
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.body = request.body;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    let { filterDinamis } = Extend;

    // FUNTION TO HENDLE FIND CATEGORY INVOICE
    const selectCategoryInvoice = async (status) => {
      let dinamic = filterDinamis([`ic."name" = ?`], [status]);
      let sql = `select ic.id , ic."name", ic.code from invoice_category ic ${dinamic.filter}`;
      const unit_config = await db
        .row(sql, dinamic.filterValue)
        .then((result) => {
          return result;
        })
        .catch((err) => {
          throw new CoreException("something wrong | terjadi kesalahan");
        });
      return unit_config;
    };

    let dataCategory = await selectCategoryInvoice(input.status);
    input.category = dataCategory.name;

    // SET FILTER
    input.filter = [`i.invoice_category_id = ?`];
    input.filterValue = [dataCategory.id];

    let filter = [
      {
        field: "created_at",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // CORPORATE INTALLATION INVOICE
    if (input.category === "INSTALLATION") {
      let invoceIntallasi = `SELECT 
                              ip.receipt_number,c."name" ,ic.name invoice_category, i.unit_price production_unit_price, i.tax_amount, ip.amount, ip.paid_at due_date, ip.status_code payment_status
                              FROM invoice_payment ip
                              LEFT JOIN invoice i ON i.id = ip.invoice_id
                              LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id
                              LEFT JOIN corporate c ON c.id = i.corporate_id
                              ${input.filter}
                              GROUP BY ip.receipt_number,c."name" ,ic.name , i.unit_price, i.tax_amount, ip.amount, ip.paid_at, ip.status_code
                              LIMIT ${input.limit} OFFSET ${input.offset};`;

      const invoice = await db.run_select(invoceIntallasi, input.filterValue);

      // COUNT
      var sql2 = `SELECT COUNT(ip.*) AS record FROM invoice_payment ip
                  LEFT JOIN invoice i ON i.id = ip.invoice_id
                  LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id
                  LEFT JOIN corporate c ON c.id = i.corporate_id
                  ${input.filter}`;

      let record = await db.row(sql2, input.filterValue);

      let summary = {
        transaction_pending: 0,
        total_amount_pending: 0,
        transaction_success: 0,
        total_amount_success: 0,
        transaction_expired: 0,
        total_amount_expired: 0,
        total_transaction: 0,
        total_amount_transaction: 0,
      };

      invoice.forEach((item) => {
        const { status_code, amount } = item;
        if (status_code === "SUCCESS") {
          summary.transaction_success++;
          summary.total_amount_success += amount;
        } else if (status_code === "PENDING") {
          summary.transaction_pending++;
          summary.total_amount_pending += amount;
        } else if (payment_status === "EXPIRED") {
          summary.transaction_expired++;
          summary.total_amount_expired += amount;
        }
        summary.total_transaction++;
        summary.total_amount_transaction += amount;
      });

      let response = {
        data: invoice,
        record: record ? record.record : 0,
        summary,
      };

      return response;
    } else {
      // CORPORATE SUBCRIBE INVOICE
      let invoceSubcribe = `SELECT 
                              i.id, ip.receipt_number, c."name" corporate_name , pu.name production_unit_name,ic.name invoice_category, i.unit_price production_unit_price, i.tax_amount, ip.amount, to_char(ip.paid_at ,'YYYY-MM-DD hh24:MI:ss') AS due_date, ip.status_code payment_status
                              FROM invoice_payment ip
                              LEFT JOIN invoice i ON i.id = ip.invoice_id
                              INNER JOIN invoice_category ic ON ic.id = i.invoice_category_id
                              LEFT JOIN corporate c ON c.id = i.corporate_id
                              LEFT JOIN production_unit pu ON i.production_unit_id = pu.id
                              ${input.filter}
                              GROUP BY i.id, ip.receipt_number,c."name", pu.name, ic.name , i.unit_price, i.tax_amount, ip.amount, ip.paid_at, ip.status_code
                              LIMIT ${input.limit} OFFSET ${input.offset};`;

      const invoice = await db.run_select(invoceSubcribe, input.filterValue);

      // COUNT
      var sql2 = `SELECT COUNT(ip.*) AS record FROM invoice_payment ip
                  LEFT JOIN invoice i ON i.id = ip.invoice_id
                  LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id
                  LEFT JOIN corporate c ON c.id = i.corporate_id
                  ${input.filter}`;

      let record = await db.row(sql2, input.filterValue);

      let invoiceSummarySubcribe = `SELECT 
      i.id, ip.receipt_number, c."name" corporate_name , pu.name production_unit_name,ic.name invoice_category, i.unit_price production_unit_price, i.tax_amount, ip.amount, ip.paid_at due_date, ip.status_code payment_status
      FROM invoice_payment ip
      LEFT JOIN invoice i ON i.id = ip.invoice_id
      INNER JOIN invoice_category ic ON ic.id = i.invoice_category_id
      LEFT JOIN corporate c ON c.id = i.corporate_id
      LEFT JOIN production_unit pu ON i.production_unit_id = pu.id
      ${input.filter}
      GROUP BY i.id, ip.receipt_number,c."name", pu.name, ic.name , i.unit_price, i.tax_amount, ip.amount, ip.paid_at, ip.status_code
      `;

      const invoice_summary = await db.run_select(
        invoiceSummarySubcribe,
        input.filterValue
      );

      console.log(invoice_summary);
      let summary = {
        transaction_pending: 0,
        total_amount_pending: 0,
        transaction_success: 0,
        total_amount_success: 0,
        transaction_expired: 0,
        total_amount_expired: 0,
        total_transaction: 0,
        total_amount_transaction: 0,
      };

      invoice_summary.forEach((item) => {
        const { payment_status, amount } = item;
        if (payment_status === "SUCCESS") {
          summary.transaction_success++;
          summary.total_amount_success += amount;
        } else if (payment_status === "PENDING") {
          summary.transaction_pending++;
          summary.total_amount_pending += amount;
        } else if (payment_status === "EXPIRED") {
          summary.transaction_expired++;
          summary.total_amount_expired += amount;
        }
        summary.total_transaction++;
        summary.total_amount_transaction += amount;
      });

      let response = {
        data: invoice,
        record: record ? record.record : 0,
        summary,
      };

      return response;
    }
  },

  validation: {
    status: "required|string|accepted:SUBSCRIBE,INSTALLATION",
    date: "required|array",
    "date.*": "date",
  },
};

module.exports = CoreService(service);
