const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * View Payment By Corporate
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.body = request.body;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "c",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "created_at",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "ip.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // CORPORATE SUBCRIBE INVOICE
    let sql = `SELECT pu."name" production_unit_name, to_char(i.created_at,'YYYY-MM-DD hh24:MI:ss') AS payment_date, i.invoice_number, i.unit_price production_unit_price, i.quantity, i.amount, to_char(ip.paid_at,'YYYY-MM-DD hh24:MI:ss') AS due_date , ip.status_code payment_status
                FROM invoice_payment ip 
                LEFT JOIN invoice i ON i.id = ip.invoice_id 
                LEFT JOIN production_unit pu on pu.id = i.production_unit_id 
                LEFT JOIN corporate c on c.id = i.corporate_id ${input.filter}
                LIMIT ${input.limit} OFFSET ${input.offset}`;

    const invoice = await db.run_select(sql, input.filterValue);

    // ALL SUMMARY
    let sql2 = `SELECT pu."name" production_unit_name, i.created_at payment_date, i.invoice_number, i.unit_price production_unit_price, i.quantity, i.amount, ip.paid_at due_date, ip.status_code payment_status
                FROM invoice_payment ip 
                LEFT JOIN invoice i ON i.id = ip.invoice_id 
                LEFT JOIN production_unit pu on pu.id = i.production_unit_id 
                LEFT JOIN corporate c on c.id = i.corporate_id ${input.filter}`;
    const invoice_summary = await db.run_select(sql2, input.filterValue);

    // SUMMARY FOR GIVING PAYMENT STATUS PERCENTAGE
    let summary = {};
    let totalEntries = invoice_summary.length;
    let paymentStatusCount = {};

    invoice_summary.forEach((item) => {
      let status = "persentace_".concat(item.payment_status.toLowerCase()); // FORMATING KEY OF OBJECT

      if (paymentStatusCount[status]) {
        paymentStatusCount[status]++;
      } else {
        paymentStatusCount[status] = 1;
      }
    });

    Object.keys(paymentStatusCount).forEach((key) => {
      let presentase = ((paymentStatusCount[key] / totalEntries) * 100).toFixed(
        1
      );
      summary[key] = parseFloat(presentase);
    });

    // SUMMARY FOR GET TOTAL TRANSACTION PAYMENT INVOICE
    let transaction_amount = {
      transaction_pending: 0,
      total_amount_pending: 0,
      transaction_success: 0,
      total_amount_success: 0,
      transaction_expired: 0,
      total_amount_expired: 0,
      total_transaction: 0,
      total_amount_transaction: 0,
    };

    invoice_summary.forEach((item) => {
      const { payment_status, amount } = item;
      if (payment_status === "SUCCESS") {
        transaction_amount.transaction_success++;
        transaction_amount.total_amount_success += amount;
      } else if (payment_status === "PENDING") {
        transaction_amount.transaction_pending++;
        transaction_amount.total_amount_pending += amount;
      } else if (payment_status === "EXPIRED") {
        transaction_amount.transaction_expired++;
        transaction_amount.total_amount_expired += amount;
      }
      transaction_amount.total_transaction++;
      transaction_amount.total_amount_transaction += amount;
    });

    // COUNT
    let sql3 = `SELECT COUNT(ip.*) as record
                FROM invoice_payment ip
                LEFT join invoice i ON i.id = ip.invoice_id
                LEFT join production_unit pu on pu.id = i.production_unit_id
                LEFT join corporate c on c.id = i.corporate_id ${input.filter}`;
    const record = await db.row(sql3, input.filterValue);

    return {
      data: invoice,
      record: record ? record.record : 0,
      summary: { ...summary, ...transaction_amount },
    };
  },

  validation: {
    corporate_id: "required|string",
    date: "required|array",
    "date.*": "date",
  },
};

module.exports = CoreService(service);
