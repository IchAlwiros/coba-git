const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const { Extend } = require("../../../util/extendFunction");

/**
 * Create Invoice
 */

const service = {
  transaction: true,
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      corporate_id,
      production_unit_id,
      description,
      payment_channel_id,
      currentUserId,
    } = input;
    let { currentDate } = Global;
    let { generateTemplateNumberInvoice, generatePeriod } = Extend;

    // FUNTION TO HENDLE FIND THE PRODUCTION UNIT WHERE CORPORATE AND ACTIVE 1
    const selectPUC = async (corporateId, plantId) => {
      let sql = `select puc.corporate_id ,puc.production_unit_id ,puc.billing_date ,puc.unit_price, puc.tolerance, puc.active, puc.installation_fee from production_unit_config puc where puc.corporate_id = ? AND puc.production_unit_id = ? AND puc.active = '1'`;
      const unit_config = await db
        .raw(sql, [corporateId, plantId])
        .then((result) => {
          // Menampilkan hasil query
          return result.rows;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          throw new CoreException("something wrong | terjadi kesalahan");
        });
      return unit_config;
    };

    // FUNTION TO HENDLE INSERT INVOICE PAYMENT AFTER INVOICE CREATED

    const insertInvoicePayment = async (item) => {
      let paymentInvoice = {
        uuid: uuidv4(),
        invoice_id: item.id,
        payment_channel_id: payment_channel_id,
        receipt_number: item.invoice_number,
        amount: item.amount,
        paid_at: item.end_periode,
        description: description,
        status_code: "PENDING",
        active: "1",
        created_by: currentUserId,
        created_at: currentDate(),
      };
      let invoice_payment = await db
        .run_insert("invoice_payment", paymentInvoice)
        .then((resuslt) => {
          // console.log(resuslt);
          return resuslt;
          // Global.autoExiredStatus(db, user_id, resuslt.uuid);
        })
        .catch((err) => {
          throw new CoreException(
            "invoice key number already exists | nomor invoice tersedia"
          );
        });

      return {
        invoice: item,
        invoice_payment: invoice_payment,
      };
    };

    // FIND THE CORPORATE
    let sql = `select c.id, c."name", pu.id plant_id, pu."name", puc.tax from production_unit pu left join corporate c on c.id = pu.corporate_id left join production_unit_config puc on puc.production_unit_id = pu.id where c.uuid = ? and pu.uuid = ?`;
    const corporate_unit_cfg = await db
      .row(sql, [corporate_id, production_unit_id])
      .then(async (corporate) => {
        // Menampilkan hasil query
        let dataPuc = await selectPUC(corporate.id, corporate.plant_id);
        return dataPuc;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("corporate production config not found");
      });

    // console.log(corporate_unit_cfg);
    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;
    let endPoint = `support/summary/production-by-plant/${production_unit_id}?date[]=2023-04-01&date[]=2023-04-30&status_code`;
    let dataPlant = await Extend.fetchAPI(`${baseUrl}${endPoint}`)
      .then((e) => {
        return e.data;
      })
      .catch((err) => {
        throw new CoreException("something wrong | terjadi kesalahan");
      });

    // FUNTION TO HENDLE DATA INVOICE
    const insertInvoice = (item) => {
      let dataInvoice = {};
      let generateInvoice = generateTemplateNumberInvoice(currentDate());
      let periode = generatePeriod(item.billing_date);
      let unitPrice = item.unit_price;
      let quantity = dataPlant.total_success;
      let ammount = quantity * unitPrice;
      let taxAmount = item.tax == 1 ? (11 / 100) * ammount || 0 : 0;
      let total_amount = item.tax == 1 ? ammount : taxAmount + ammount;
      return (dataInvoice = {
        ...dataInvoice,
        uuid: uuidv4(),
        corporate_id: item.corporate_id ? item.corporate_id : null,
        production_unit_id: item.production_unit_id
          ? item.production_unit_id
          : null,
        invoice_number: generateInvoice,
        unit_price: unitPrice,
        quantity: quantity,
        amount: ammount,
        tax: item.tax || 0,
        tax_amount: item.tax == 1 ? taxAmount : 0,
        total_amount: total_amount,
        total_paid: total_amount + taxAmount,
        periode: currentDate(),
        start_periode: periode.startDate,
        end_periode: periode.endDate,
        overdue: periode.endDate,
        status_code: "PENDING",
        created_by: currentUserId,
        created_at: currentDate(),
      });
    };

    if (corporate_unit_cfg.length == 0) {
      throw new CoreException(
        "unit active config not found | unit active config tidak tersedia"
      );
    }
    // JIKA CORPORATE UNIT CONFIG DITEMUKAN MAKA PENJADWALAN PEMBUATAN INVOICE OTOMATIS AKAN DILAKUKAN SETIAP BULAN
    let response = {};
    for (const item of corporate_unit_cfg) {
      // INSERT INVOICE
      let dataInvoice = insertInvoice(item);

      if (Object.keys(dataInvoice).length === 0) {
        throw new CoreException(
          "production unit not active | production unit tidak aktif"
        );
      } else {
        // console.log(dataInvoice);
        let insertInvoice = await db
          .run_insert("invoice", dataInvoice)
          .then(async (resuslt) => {
            // console.log(resuslt);
            // INSERT INVOCE PAYMENT
            let output = await insertInvoicePayment(resuslt);
            return output;
          })
          .catch((err) => {
            throw new CoreException(
              "invoice key number already exists | nomor invoice tersedia"
            );
          });
        response = { ...response, ...insertInvoice };
      }
    }

    return response;
  },

  validation: {
    corporate_id: "required|string",
    production_unit_id: "required|string",
  },
};

module.exports = CoreService(service);
