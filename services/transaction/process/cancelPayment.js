const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");

/**
 * Update Cancel Status Invoice
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT ip.id 
                FROM invoice_payment ip 
                WHERE ip.invoice_id = ?`;

    let existPayment = await db.row(sql, [input.invoice_id]);

    if (!existPayment) {
      throw new CoreException(
        " your payment is not available | pembayaran anda tidak tersedia"
      );
    }

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { status_code, invoice_id, currentUserId } = input;

    // FIND INVOICE PAYMENT WILL UPDATE
    let sql = `SELECT i.created_at, i.status_code, ip.created_at created_at_payment, ip.status_code status_payment
                FROM invoice i
                INNER JOIN invoice_payment ip ON ip.invoice_id = i.id
                WHERE i.id = ?`;

    let payment = await db.row(sql, [invoice_id]);
    const currentDate = moment();
    const createdAt = moment(payment.created_at);
    const diffDays = currentDate.diff(createdAt, "days"); // Menghitung selisih hari antara tanggal saat ini dan tanggal pembuatan

    // Memeriksa apakah selisih hari lebih dari 7 hari
    if (diffDays > 7) {
      // ALLOW TO UPDATE DATA
      // PAYMENT VALUE;
      let updateValue = {
        status_code,
        updated_by: currentUserId,
        updated_at: Global.currentDateTime(),
      };
      let data = await db
        .run_update("invoice_payment", updateValue, {
          invoice_id: invoice_id,
        })
        .then(async (e) => {
          await db.run_update("invoice", updateValue, { id: e.invoice_id });
          return e;
        })
        .catch((err) => {
          throw new CoreException(
            "something wrong | terjadi kesalahan saat update"
          );
        });
      return data;
    } else {
      // NOT ALLOW TO UPDATE DATA
      throw new CoreException(
        "payments can only be canceled after 7 days | pembayaran hanya bisa dibatalkan setelah 7 hari"
      );
    }
  },

  validation: {
    invoice_id: "required|integer",
    status_code: "required|string|accepted:PROGRESS,PENDING,SUCCESS,CANCEL",
  },
};

module.exports = CoreService(service);
