const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const { v4: uuidv4 } = require("uuid");

/**
 * ADD PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    let {
      active,
      bank_name,
      account_name,
      account_number,
      payment_channel_category,
    } = input;
    // CHECK existing data in table payment_channel
    let sql = `SELECT A.* FROM payment_channel A
               WHERE payment_channel_category_id = ?
               AND account_number = ?
               AND bank_name = ?
               AND account_name = ?`;

    let paymentChannel = await db.row(sql, [
      payment_channel_category,
      account_number,
      bank_name,
      account_name,
    ]);
    console.log(paymentChannel);
    if (paymentChannel)
      throw new CoreException(
        "Payment Channel already exist | Channel Pembayaran sudah ada"
      );

    // SET input.active with DEFAULT true
    active = active == null ? 1 : active;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { user_id } = input.session;
    let {
      active,
      payment_channel_category,
      name,
      bank_code,
      bank_name,
      account_name,
      account_number,
      admin_fee,
    } = input;
    let { currentDateTime } = Global;
    const insertValue = {
      uuid: uuidv4(),
      payment_channel_category_id: payment_channel_category,
      name,
      bank_code,
      bank_name,
      account_name,
      account_number,
      admin_fee,
      active,
      created_at: currentDateTime(),
      created_by: user_id,
    };

    // INSERT TO PAYMENT CHANNEL
    let Insertdata = await db.run_insert("payment_channel", insertValue);

    // console.log(insertValue, input.session);
    return Insertdata;
  },
  validation: {
    payment_channel_category: "required|integer",
    name: "string",
    bank_code: "string",
    bank_name: "string",
    account_name: "string",
    account_number: "string",
    admin_fee: "decimal",
    active: "boolean",
  },
};

module.exports = CoreService(service);
