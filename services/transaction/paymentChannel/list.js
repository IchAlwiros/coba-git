const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * LIST PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    input.searchField = [];
    input.searchValue = [];

    // CONFIG
    let filter = [
      {
        field: "payment_channel_category_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "name",
        alias: "A",
        search: true,
        order: true,
        filter: false,
      },
      {
        field: "bank_name",
        alias: "A",
        search: true,
        order: true,
        filter: false,
      },
      {
        field: "active",
        alias: "A",
        search: true,
        order: true,
        filter: true,
      },
    ];
    input = filterBuilder(input, filter);

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "B.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let columnName = `A.id, A.uuid, A.payment_channel_category_id, B.name AS payment_channel_category_name, A.name, A.bank_code, A.bank_name, A.account_name, A.account_number, A.admin_fee, A.active`;
    let sql = `SELECT ${columnName} FROM payment_channel A 
                    INNER JOIN payment_channel_category B
                    ON A.payment_channel_category_id = B.id ${input.filter} 
                    ORDER BY ${input.orderBy} LIMIT ${input.limit} OFFSET ${input.offset}`;

    // run_select untuk return array
    let data = await db.run_select(sql, input.filterValue);

    // COUNT
    var sql2 = `SELECT COUNT(A.*) AS record
                FROM payment_channel A ${input.filter}`;
    // row untuk return object
    let record = await db.row(sql2, input.filterValue);

    return {
      data: data,
      record: record ? record.record : 0,
    };
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    orderBy: "string",
  },
};

module.exports = CoreService(service);
