const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");

/**
 * UPDATE PAYMENT CHANNEL
 */

const service = {
  input: function (request) {
    let input = request.params;
    input.body = request.body;
    input.id = request.params.id;
    return input;
  },

  prepare: async function (input, db) {
    let sql = `SELECT * FROM payment_channel A
               WHERE A.id = ?`;
    let isExist = await db.row(sql, input.id);

    if (!isExist)
      throw new CoreException(
        "Payment Channel not found | Channel Pembayaran tidak ditemukan"
      );

    input.updated_at = Global.currentDateTime();
    // input.updated_by =
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { user_id } = input.session;
    let {
      name,
      bank_code,
      bank_name,
      account_name,
      account_number,
      admin_fee,
      active,
    } = input.body;

    const updateValue = {
      name: name,
      bank_code: bank_code,
      bank_name: bank_name,
      account_name: account_name,
      account_number: account_number,
      admin_fee: admin_fee,
      active: active,
      updated_at: Global.currentDateTime(),
      updated_by: user_id,
    };

    // UPDATE PAYMENT CHANNEL
    let data = await db.run_update("payment_channel", updateValue, {
      id: input.id,
    });

    console.log(updateValue);
    return data;
  },
  validation: {
    name: "string",
    bank_code: "string",
    bank_name: "string",
    account_name: "string",
    account_number: "string",
    admin_fee: "decimal",
    active: "boolean",
  },
};

module.exports = CoreService(service);
