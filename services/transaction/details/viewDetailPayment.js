const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * View Detail Invoice Payment
 */

const service = {
  input: function (request) {
    let input = request.params;
    return input;
  },

  prepare: async function (input, db) {
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "invoice_id",
        alias: "ip",
        search: false,
        order: false,
        filter: true,
        param: "invoice_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `select c."name" corporate_name, pu.name production_unit_name, ip.receipt_number, ip.amount , to_char(ip.paid_at ,'YYYY-MM-DD hh24:MI:ss') AS paid_at , ip.description, pc.bank_name , pc.account_number bank_account_number, pc.account_name bank_account_name, ip.status_code payment_status 
                from invoice_payment ip 
                INNER JOIN invoice i on i.id = ip.invoice_id 
                LEFT JOIN corporate c on c.id = i.corporate_id 
                LEFT JOIN production_unit pu on pu.id = i.production_unit_id
                LEFT JOIN payment_channel pc ON ip.payment_channel_id = pc.id ${input.filter}`;

    const invoice_detail = await db.row(sql, input.filterValue);

    // IF INVOICE DETAIL PAYMENT UNDEFINED || NOT FOUND
    if (invoice_detail == undefined) {
      throw new CoreException(
        "payment transaction not found | transaksi pembayaran tidak ditemukan"
      );
    }

    return invoice_detail;
  },

  validation: {
    invoice_id: "required|integer",
  },
};

module.exports = CoreService(service);
