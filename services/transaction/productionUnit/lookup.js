const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * List Invoice Data Pembayaran
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "corporate_id",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "production_unit_id",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
      {
        field: "created_at",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // CORPORATE ALL INVOICE BY PLANT
    let sql = `SELECT pu.id production_unit_id, pu.name production_unit_name, ic."name" payment_type ,to_char(ip.paid_at ,'YYYY-MM-DD hh24:MI:ss') AS paid_at, 
                to_char(i.start_periode,'YYYY-MM-DD hh24:MI:ss') AS start_periode , to_char(i.end_periode,'YYYY-MM-DD hh24:MI:ss') AS end_periode, ip.amount, to_char(i.overdue,'YYYY-MM-DD hh24:MI:ss') AS overdue, pc.bank_name, 
                pc.account_name bank_account_name, pc.account_number bank_account_number, 
                ip.description payment_description, CASE WHEN ip.active ='1' THEN TRUE ELSE FALSE END AS active, ip.status_code payment_status
                from invoice_payment ip 
                LEFT JOIN invoice i on i.id = ip.invoice_id 
                INNER JOIN production_unit pu on i.production_unit_id = pu.id 
                LEFT JOIN payment_channel pc on pc.id = ip.payment_channel_id 
                LEFT JOIN invoice_category ic on ic.id = i.invoice_category_id 
                ${input.filter} ORDER BY ${input.orderBy} ${input.sort}
                LIMIT ${input.limit} OFFSET ${input.offset}`;
    const invoice = await db.run_select(sql, input.filterValue);

    var sql3 = `SELECT COUNT(i.*) AS record
                from invoice_payment ip 
                LEFT JOIN invoice i on i.id = ip.invoice_id  
                LEFT JOIN payment_channel pc on pc.id = ip.payment_channel_id 
                LEFT JOIN invoice_category ic on ic.id = i.invoice_category_id 
                ${input.filter}`;
    let record = await db.row(sql3, input.filterValue);

    return {
      data: invoice,
      record: record ? record.record : 0,
    };
  },

  validation: {
    corporate_id: "integer",
    production_unit_id: "integer",
    date: "required|array",
    "date.*": "date",
  },
};

module.exports = CoreService(service);
