const { CoreService, CoreException } = require("../../core/CallService");
const { Extend } = require("../../util/extendFunction");
const filterBuilder = require("../../util/filterBuilder");
const { Global } = require("../../util/globalFunction");

/**
 * VIEW Module Roles
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    let { code } = input;

    let validation = {};

    // VALIDASI KODE GRUP HARUS DICANTUMKAN
    if (!code) {
      validation = {
        ...validation,
        code: `code module group required | kode module grup di perlukan`,
      };
    }
    if (Object.keys(validation).length > 0) {
      throw new CoreException("BAD REQUEST", validation, 400);
    }
  },

  process: async function (input, OriginalInput, db) {
    let { code } = input;

    let sql = `select  mg."name" module_group, mg.code grup_code, m."name" modules, m.code module_code from module_groups mg join modules m on m.module_group_id = mg.id where mg.code = ?`;

    // SELECT ALL MODULE USER
    // const moduleGroup = await db.row(sql, [code]);

    const moduleGroup = await db
      .raw(sql, [code])
      .then((result) => {
        // Menampilkan hasil query
        return result.rows;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("Invalid code | code tidak ditemukan");
      });

    // console.log(moduleGroup);
    let result = Extend.nestedModule(moduleGroup);

    return result[0];
  },

  validation: {
    code: "required|string",
  },
};

module.exports = CoreService(service);
