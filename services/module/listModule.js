const { CoreService, CoreException } = require("../../core/CallService");
const { Extend } = require("../../util/extendFunction");
const filterBuilder = require("../../util/filterBuilder");

/**
 * List Module Roles
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "name",
        alias: "r",
        search: false,
        order: false,
        filter: true,
        param: "role",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "m.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `select u."name" user,r."name",r.active , r.code, r.description, m."name" module_name, m.code module_code, rd.allowed
                 from modules m join role_details rd on rd.module_id = m.id 
                 join roles r on r.id = rd.role_id 
                 join users u on r.id = u.role_id 
                 ${input.filter}`;

    // SELECT ALL MODULE USER
    const roleUser = await db
      .raw(sql, input.filterValue)
      .then((result) => {
        // Menampilkan hasil query
        return result.rows;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("Invalid role | role tidak valid");
      });

    let result = Extend.nestedData(roleUser);

    return result;
  },

  validation: {
    role: "required|string|accepted:ADMIN,USER",
  },
};

module.exports = CoreService(service);
