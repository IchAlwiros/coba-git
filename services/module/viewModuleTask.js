const { CoreService, CoreException } = require("../../core/CallService");
const { Extend } = require("../../util/extendFunction");
const filterBuilder = require("../../util/filterBuilder");
const { Global } = require("../../util/globalFunction");

/**
 * view Module Permission Task
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    input.query = request.query;
    return input;
  },

  prepare: async function (input, db) {
    let { role } = input.query;
    // let { user_id, email } = input.user;

    // VALIDATION REQUIREMENT
    let requirement = {
      checkrole: role === "USER" || role === "ADMIN",
    };
    let validation = {};

    if (!requirement.checkrole) {
      validation = {
        ...validation,
        role: `role not valid | role tidak valid `,
      };
    }

    // if (!requirement.checkuser) {
    //   validation = {
    //     ...validation,
    //     role: `access denied only for admin | access ditolak hanya untuk admin`,
    //   };
    // }

    if (Object.keys(validation).length > 0) {
      throw new CoreException("BAD REQUEST", validation, 400);
    }
  },

  process: async function (input, OriginalInput, db) {
    let { module_code } = input;

    let sql = `select m.id ,m."name" module_name,m.code module_code ,t."name" task , t.code task_code, mp."action", mp.task_id , mp.module_id from modules m left join module_permissions mp on m.id = mp.module_id left join tasks t on t.id = mp.task_id where m.code = ?`;

    // MENDAPAT SEMUA DATA DARI RELASI MODULE dengan TASK
    const moduleTask = await db
      .raw(sql, [module_code])
      .then((result) => {
        // Menampilkan hasil query
        return result.rows;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("something wrong | ada yang salah");
      });

    let nestedModuleTask = Extend.generateNestedTask(moduleTask);

    console.log(moduleTask);

    return nestedModuleTask[0];
  },

  validation: {
    module_code: "required|string",
  },
};

module.exports = CoreService(service);
