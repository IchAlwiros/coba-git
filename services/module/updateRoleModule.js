const { CoreService, CoreException } = require("../../core/CallService");
const { Global } = require("../../util/globalFunction");

/**
 * Update Module Roles Detail
 */

const service = {
  transaction: true,
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT m.id, m.code, m.name, rd.role_id
                FROM modules m 
                JOIN role_details rd on rd.module_id = m.id 
                JOIN roles r on r.id = rd.role_id 
                WHERE m.id = ? AND rd.allowed = '1'`;

    // SELECT ALL MODULE ROLE
    const roleModule = await db.run_select(sql, [input.module_id]);
    input.existRoleModule = roleModule;

    let sql2 = `SELECT r.*
                FROM roles r
                WHERE r.id = ?`;

    // TO FIND ROLE EXISTS
    let existsRole = [];
    for (const el of input.role_access_ids) {
      //   console.log(el);
      const role = await db.run_select(sql2, [el]);
      existsRole = [...existsRole, ...role];
    }

    console.log(existsRole);

    // UNTUK MENGHANDLE ROLE YANG AKAN DI PROSESS ITU TERSEDIA/TIDAK
    let isMatched = input.role_access_ids.every((num) =>
      existsRole.some((item) => item.id === num)
    );

    if (!isMatched) {
      throw new CoreException(
        " role to be processed was not found | role yang akan di proses tidak tersedia"
      );
    }

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      module_group,
      module_name,
      module_id,
      role_access_ids,
      active,
      allowed,
      currentUserId,
      existRoleModule,
    } = input;

    // TO CHECK IF HAVE MODULE WITH RELATION ROLE
    if (existRoleModule) {
      let dataModule = {
        name: module_name,
        module_group_id: module_group,
        active: active,
        // updated_by: currentUserId,
      };
      try {
        //   MENDAPATKAN CURRENT ROLE_DETAIL MODULE
        let currentRoleModule = existRoleModule.map((e) => e.role_id);

        // MENGUPDATE DATA MODULE
        let updateModule = await db.run_update("modules", dataModule, {
          id: module_id,
        });

        // Mendapatkan ID Role Module Yang Akan Di Hapus & Di Masukan
        let [deleted_role_id, new_role_id] = Global.getNewIdsAndDeletedIds(
          currentRoleModule,
          role_access_ids
        );

        // Menghapus role_details
        deleted_role_id.forEach(async (el) => {
          await db("role_details")
            .where("role_id", el)
            .andWhere("module_id", parseInt(existRoleModule[0].id))
            .del();
          //   console.log(el, existRoleModule[0].id, "delete role_detail");
        });

        //   TO INSERT IN ROLES DETAIL
        let insertRoleResponse = [];
        for (const item of new_role_id) {
          let roleInsert = await db.run_insert("role_details", {
            role_id: item,
            module_id: module_id,
            allowed: 1,
            active: 1,
            created_by: currentUserId,
          });
          //   console.log(item, "insert new role detail");
          insertRoleResponse = [...insertRoleResponse, roleInsert];
        }
        // await db.raw("select setval('role_id', max(id)) from role_details");

        //   DATA RESULTING
        let resultDataInsert = {
          message: "module roles success updated | module berhasil di update",
          module: !updateModule
            ? "nothing module to update | tidak ada module yang di update"
            : updateModule,
          roles_detail:
            insertRoleResponse.length == 0
              ? "nothing roles detail to update | roles detail tidak perlu di update"
              : insertRoleResponse,
        };

        return resultDataInsert;
      } catch (error) {
        console.log(error);
        throw new CoreException("something wrong | terjadi kesalahan");
      }
    }

    return "module does not exist | data module tidak ditemukan ";
  },

  validation: {
    module_id: "required|integer",
    module_name: "required|string",
    role_access_ids: "required|array",
  },
};

module.exports = CoreService(service);
