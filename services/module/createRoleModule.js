const { CoreService, CoreException } = require("../../core/CallService");
const { Global } = require("../../util/globalFunction");

/**
 * Create Module Roles Detail
 */

const service = {
  transaction: true,
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT m.id, m.code, m.name  
                FROM modules m 
                JOIN role_details rd on rd.module_id = m.id 
                JOIN roles r on r.id = rd.role_id 
                WHERE m.code = ? AND rd.allowed = '1'`;

    // SELECT ALL MODULE ROLE
    const roleModule = await db.row(sql, [input.module_code]);
    input.existRoleModule = roleModule;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      module_group,
      module_name,
      module_code,
      role_access_ids,
      existRoleModule,
      currentUserId,
    } = input;

    // TO CHECK IF HAVE MODULE WITH RELATION ROLE
    if (existRoleModule === undefined) {
      let dataModule = {
        code: module_code,
        name: module_name,
        module_group_id: module_group,
        active: 1,
        created_by: currentUserId,
      };
      try {
        //   INSERT DATA MODUELE
        let insertModule = await db.run_insert("modules", dataModule);

        let [deleted_id, new_role_id] = Global.getNewIdsAndDeletedIds(
          [],
          role_access_ids
        );

        //   TO INSERT IN ROLES DETAIL
        let dataRole = [];
        for (const item of new_role_id) {
          let roleInsert = await db.run_insert("role_details", {
            role_id: item,
            module_id: insertModule.id,
            allowed: 1,
            active: 1,
            created_by: currentUserId,
          });
          dataRole = [...dataRole, roleInsert];
        }

        // await db.raw("select setval('role_id', max(id)) from role_details");

        //   DATA RESULTING
        let resultDataInsert = {
          message: "module roles created | module dibuat",
          module: insertModule,
          roles: dataRole,
        };

        return resultDataInsert;
      } catch (error) {
        throw new CoreException("something wrong | terjadi kesalahan");
      }
    }

    return "module data exist | data module tersedia ";
  },

  validation: {
    module_code: "required|string",
    // role_code: "required|string",
    module_name: "required|string",
    role_access_ids: "required|array",
  },
};

module.exports = CoreService(service);
