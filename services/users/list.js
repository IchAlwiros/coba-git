const { CoreService, CoreException } = require('../../core/CallService')
const filterBuilder = require('../../util/filterBuilder')

/**
 * Service * SERVICE NAME*
 */

const service = {
    input: function (request) {
        let input = request.query
        return input
    },

    prepare: async function (input, db) {

        // CONFIG
        let filter = [
            { field: 'religion_id', alias: 'A', search: false, order: false, filter: true },
            { field: 'role_id', alias: 'A', search: false, order: false, filter: true },
            { field: 'name', alias: 'A', search: true, order: true, filter: false },
            { field: 'username', alias: 'A', search: true, order: true, filter: false },
            { field: 'phone', alias: 'A', search: true, order: true, filter: false },
            { field: 'email', alias: 'A', search: true, order: true, filter: false },
            { field: 'created_at', alias: 'A', search: false, order: true, filter: false },
        ]
        input = filterBuilder(input, filter)

        // INIT VALUE
        input.orderBy = input.orderBy ? input.orderBy : 'A.id'
        input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(' AND ')}` : ''
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.name,A.username, A.phone, A.email, A.gender,A.address,
                    A.active, B.code AS role_code, B.name AS role_name, C.name AS religion_name,
                    CASE WHEN A.photo IS NOT NULL AND A.photo !='' THEN CONCAT('${process.env.config.BASE_URL}/public/users/',A.photo) ELSE NULL END AS photo_preview,
                    to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM users A
                    LEFT JOIN roles B ON B.id = A.role_id
                    LEFT JOIN religion C ON C.id = A.religion_id
                    ${input.filter} ORDER BY ${input.orderBy} ${input.sort}  LIMIT ${input.limit} OFFSET ${input.offset}`
        let data = await db.run_select(sql, input.filterValue)

        // COUNT
        var sql2 = `SELECT COUNT(A.*) AS record 
                    FROM users A
                    LEFT JOIN roles B ON B.id = A.role_id
                    LEFT JOIN religion C ON C.id = A.religion_id ${input.filter} `
        let record = await db.row(sql2, input.filterValue)

        return {
            data: data,
            record: record ? record.record : 0
        }
    },
    validation: {
        limit: "integer|min:0",
        offset: "integer|min:0",
        role_id: "integer",
        religion_id: "integer",
    }
}

module.exports = CoreService(service)