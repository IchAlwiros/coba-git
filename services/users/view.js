const { CoreService, CoreException } = require('../../core/CallService')

/**
 * Service * USERS VIEW*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.name,A.username, A.phone, A.email, A.gender,A.address,
                    A.active, B.code AS role_code, B.name AS role_name, C.name AS religion_name,
                    A.photo,A.role_id, A.religion_id,
                    CASE WHEN A.photo IS NOT NULL AND A.photo !='' THEN CONCAT('${process.env.config.BASE_URL}/public/users/',A.photo) ELSE NULL END AS photo_preview,
                    to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM users A
                    LEFT JOIN roles B ON B.id = A.role_id
                    LEFT JOIN religion C ON C.id = A.religion_id
                    WHERE A.id = ? LIMIT 1`
        let data = await db.row(sql, [input.id])

        return data
    },
    validation: {
        id: "required|integer",
    }
}

module.exports = CoreService(service)