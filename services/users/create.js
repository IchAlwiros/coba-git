const { CoreService, CoreException } = require('../../core/CallService')
const { Global } = require('../../util/globalFunction')
const bcrypt = require('bcryptjs')

/**
 * Service * USERS CREATE*
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        if (input.photo) {
            if (!Global.fileExist(`tmp/${input.photo}`)) throw new CoreException("File not found|file tidak ditemukan")
        }

        let check = await db.row(`SELECT*FROM users WHERE username = ? AND email = ? AND phone  = ?`, [input.username, input.email, input.phone])
        if (check) {
            if (check.email == input.email) throw new CoreException(`Email has been registered|email telah terdaftar`)
            if (check.phone == input.phone) throw new CoreException(`phone number has been registered|nomor telepon telah terdaftar`)
            if (check.username == input.username) throw new CoreException(`username has been registered|username telah terdaftar`)
        }

        let checkRole = await db.row(`SELECT*FROM roles WHERE id = ? AND active ='1' AND id >0`, [input.role_id])
        if (!checkRole) throw new CoreException(`role unavailable|peran tidak tersedia`)

        if (input.religion_id) {
            let religion = await db.row(`SELECT*FROM religion WHERE id = ? `, [input.religion_id])
            if (!religion) throw new CoreException(`religion unavailable|agama tidak tersedia`)
        }
        return input
    },

    process: async function (input, OriginalInput, db) {
        let password = bcrypt.hashSync(input.password)
        await db.run_insert(`users`, {
            name: input.name,
            phone: input.phone,
            email: input.email,
            username: input.username,
            password: password,
            role_id: input.role_id,
            religion_id: input.religion_id || null,
            address: input.address,
            gender: input.gender,
            photo: input.photo,
            active: '1',
            created_at: input.currentDateTime,
            created_by: input.currentUserId
        })
        if (input.photo) await Global.moveFile(`tmp/${input.photo}`, 'public/users', input.photo)
        return {
            message: 'Data saved successfully',
            data: {
                name: input.name,
                phone: input.phone,
                email: input.email,
                username: input.username,
                address: input.address,
                gender: input.gender,
                photo: input.photo,
            }
        }
    },
    validation: {
        name: "required",
        phone: "required|phone",
        email: "required|email",
        username: "required|minLength:5",
        password: "required|minLength:8",
        gender: "accepted:L,P,O",
        role_id: 'required|integer',
        religion_id: "integer"
    }
}

module.exports = CoreService(service)