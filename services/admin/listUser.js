const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");
const { Extend } = require("../../util/extendFunction");

/**
 * List Roles User
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "r",
        search: false,
        order: false,
        filter: true,
        param: "role_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "A.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { role_id } = input;

    if (!role_id) {
      // IF role_id NOT DEFINE, SHOW USER LIST WITHOUT FILTER
      let sql = `SELECT  A.id, A."name", A.email , A.username, A.gender, A.phone, A.active as user_active, r."name" as role_name , r.code role_code , r.active role_active
                  FROM roles r 
                  INNER JOIN users A on A.role_id = r.id 
                  ORDER BY ${input.orderBy} ${input.sort}
                  LIMIT ${input.limit} OFFSET ${input.offset}`;
      let user = await db.run_select(sql);

      var sql2 = `SELECT COUNT(r.*) AS record 
                  FROM roles r 
                  INNER join users A on A.role_id = r.id 
                ${input.filter}`;

      let record = await db.row(sql2, input.filterValue);

      return {
        data: user,
        record: record ? record.record : 0,
      };
    } else if (role_id) {
      let sql = `SELECT A.id, A."name", A.email , A.username, A.gender, A.phone, A.active as user_active, r."name" as role_name , r.code role_code , r.active role_active
                  FROM roles r 
                  INNER JOIN users A on A.role_id = r.id 
                  ${input.filter} 
                  ORDER BY ${input.orderBy} ${input.sort}
                  LIMIT ${input.limit} OFFSET ${input.offset}`;

      // SELECT ROLE BY ROLE NAME
      const roleUser = await db.run_select(sql, input.filterValue);

      // COUNT
      var sql2 = `SELECT COUNT(r.*) AS record 
                  FROM roles r 
                  INNER join users A on A.role_id = r.id
                  ${input.filter}`;

      let record = await db.row(sql2, input.filterValue);

      return {
        data: roleUser,
        record: record ? record.record : 0,
      };
    }
  },

  validation: {
    role_id: "integer",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
