const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
/**
 *  Payment Gateway
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "created_at",
        alias: "ipd",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "ipd.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT ipd.*
               FROM invoice_payment_detail ipd
               ${input.filter}
               ORDER BY ${input.orderBy} ${input.sort} 
               LIMIT ${input.limit} OFFSET ${input.offset}`;

    let invoicePaymentDetail = await db.run_select(sql, input.filterValue);

    let sql2 = `SELECT COUNT(ipd.*) as record
                FROM invoice_payment_detail ipd
                ${input.filter}`;

    let record = await db.row(sql2, input.filterValue);

    let response = {
      data: invoicePaymentDetail,
      record: record ? record.record : 0,
    };
    return response;
  },

  validation: {
    date: "array",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
