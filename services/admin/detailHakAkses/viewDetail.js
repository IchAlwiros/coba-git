const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * View Hak Akses User
 */

const service = {
  input: function (request) {
    let input = request.session;
    return input;
  },

  prepare: async function (input, db) {
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "u",
        search: false,
        order: false,
        filter: true,
        param: "user_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `select u."name" user,r."name", r.id role_id, r.code, r.description, r.active , mg.name module_group, m."name" module_name, m.code module_code, rd.allowed  
                from modules m join role_details rd on rd.module_id = m.id 
                join module_groups mg on mg.id = m.module_group_id 
                join roles r on r.id = rd.role_id 
                join users u on r.id = u.role_id ${input.filter}`;

    // SELECT ALL MODULE USER
    const roleUser = await db.run_select(sql, input.filterValue);

    let result = Extend.nestedData(roleUser);

    return result[0];
  },

  validation: {},
};

module.exports = CoreService(service);
