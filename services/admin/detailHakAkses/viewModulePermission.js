const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * view Module Permission Task
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "code",
        alias: "m",
        filter: true,
        param: "module_code",
      },
      {
        field: "code",
        alias: "mg",
        filter: true,
        param: "group_code",
      },
      {
        field: "role_id",
        alias: "rd",
        filter: true,
        param: "role_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { group_code, module_code } = input;

    module_code = module_code || "";
    group_code = module_code == "" ? group_code : "";

    let sql = `select rd.role_id, mg.code module_group_code, mg.name module_group, m.created_by , mg.name module_group, m."name" module,m.code module_code ,t."name" task , mp."action", mp.task_id , mp.module_id
                from module_groups mg
                LEFT join modules m on m.module_group_id = mg.id
                LEFT join module_permissions mp on mp.module_id = m.id
                LEFT join tasks t on t.id = mp.task_id 
                INNER JOIN role_details rd on rd.module_id = m.id
                ${input.filter}`;
    console.log(sql);
    // MENDAPAT SEMUA DATA DARI RELASI MODULE dengan TASK
    const moduleTask = await db.run_select(sql, input.filterValue);

    function transformData(data) {
      const transformedData = [];

      data.forEach((item) => {
        const existingModule = transformedData.find(
          (module) =>
            module.module === item.module &&
            module.module_code === item.module_code
        );

        if (existingModule) {
          existingModule.permission.push({
            task: item.task,
            action: item.action,
            task_id: item.task_id,
            module_id: item.module_id,
          });
        } else {
          transformedData.push({
            module_group: item.module_group,
            role_id: item.role_id,
            module_name: item.module,
            module_code: item.module_code,
            permission: [
              {
                task: item.task,
                action: item.action,
                task_id: item.task_id,
                module_id: item.module_id,
              },
            ],
          });
        }
      });

      return transformedData;
    }

    let nestedModuleTask = transformData(moduleTask);

    // console.log(nestedModuleTask);

    if (module_code) {
      let nestedModuleTask = Extend.generateNested(moduleTask);
      return nestedModuleTask[0];
    }

    return nestedModuleTask;
  },

  validation: {
    group_code: "string",
    module_code: "string",
  },
};

module.exports = CoreService(service);
