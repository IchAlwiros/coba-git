const { CoreService, CoreException } = require("../../../core/CallService");
const { Extend } = require("../../../util/extendFunction");
const { Global } = require("../../../util/globalFunction");

/**
 * Update Permission Module User
 */

const service = {
  transaction: true,
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    let { task_ids } = input;

    // UNTUK MEMFILTER DATA TASK YANG DI CARI ITU EXITS
    let sql = `select t.id ,t."name" from tasks t where t.id = ?`;
    for (let i = 0; i < task_ids.length; i++) {
      const taskIds = task_ids[i];
      let fromTask = await db.run_select(sql, [taskIds]);
      if (fromTask.length === 0 || fromTask.length == null) {
        throw new CoreException(
          "task data not found | task data tidak tersedia"
        );
      }
    }

    let hasDuplicates =
      task_ids.filter((value, index) => task_ids.indexOf(value) !== index)
        .length > 0;

    if (hasDuplicates) {
      throw new CoreException(
        "bad request | data yang anda masukan ganda",
        400
      );
    }

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { action, task_ids, module_code } = input;
    let { getNewIdsAndDeletedIds } = Global;

    let sql = `select m.id ,m."name",m.code module_code ,t."name" task , t.code task_code, mp."action", mp.task_id , mp.module_id from modules m left join module_permissions mp on m.id = mp.module_id left join tasks t on t.id = mp.task_id where m.code = ?`;

    // MENDAPAT SEMUA DATA DARI RELASI MODULE dengan TASK
    const moduleTask = await db
      .raw(sql, [module_code])
      .then((result) => {
        // Menampilkan hasil query
        return result.rows;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("something wrong | ada yang salah");
      });

    let nestedRelationTask = Extend.generateNestedTask(moduleTask);

    // Mengecek Jika Module Code Tidak Ditemukan
    if (nestedRelationTask.length == 0) {
      // Menampilkan pesan error jika terjadi kesalahan
      throw new CoreException(
        "module code not found | module code tidak tersedia"
      );
    }

    // MENDAPATKAN TASK ID TERAKHIR
    let current_task_id = nestedRelationTask[0].task.map((e) => e.task_id);

    // MENDAPATKAN ID TASK YANG HARUS DI HAPUS DAN DI MASUKAN
    let [deleted_task_id, new_task_id] = getNewIdsAndDeletedIds(
      current_task_id,
      task_ids
    );

    // JIKA DATA YANG AKAN DIHAPUS & DATA BARU KOSONG
    if (deleted_task_id.length === 0 && new_task_id.length === 0)
      return {
        message: `nothing to be update from task module | tidak ada yang diupdate dari module task`,
      };

    // console.log(deleted_task_id, "delet id");
    // console.log(new_task_id, "new id");

    // MELAKUKAN PROSES PEMBARUAN DARI MODULE YANG MEMILIKI BANYAK TASK
    try {
      //   MENGHAPUS DATA MODULE PERMISSION DENGAN FOREACH
      //   deleted_task_id.forEach(async (el) => {
      //     await db("module_permissions")
      //       .where("task_id", el)
      //       .andWhere("module_id", parseInt(nestedRelationTask[0].id))
      //       .del();
      //   });
      //   MENGINSERT DATA MODULE PERMISSION DENGAN FOREACH
      //   new_task_id.forEach(async (el) => {
      //     await db.run_insert("module_permissions", {
      //       task_id: el,
      //       module_id: parseInt(nestedRelationTask[0].id),
      //       action: action,
      //     });
      //   });

      // MELAKUKAN DELETE TASK
      for (const el of deleted_task_id) {
        await db("module_permissions")
          .where("task_id", el)
          .andWhere("module_id", parseInt(nestedRelationTask[0].id))
          .del();
      }
      //   FUNSI MENGHANDLE INPUT PADA MODULE PERMISSIONS
      const fetchData = async () => {
        const results = [];
        // INSERT DATA PERMISSION MODULE
        for (const el of new_task_id) {
          try {
            let insertData = await db.run_insert("module_permissions", {
              task_id: el,
              module_id: parseInt(nestedRelationTask[0].id),
              action: action,
            });
            results.push(insertData);
          } catch (error) {
            throw new CoreException("something wrong | ada yang salah");
          }
        }
        return results;
      };
      // MENGEMBALIKAN HASIL DARI FUNGSI FECTH DATA
      let data = await fetchData();
      //   FUNSI MEMANIPULSAI HASIL RESPONSE INSERT
      let responseManipulation = async (item) => {
        let task = [];
        let permmission = [];
        // PERULANGAN UNTUK MENDAPATKAN DATA TASK DAN ACTION
        for (let i = 0; i < data.length; i++) {
          let selectTask = `select t."name" from tasks t where t.id = ?`;
          let fromTask = await db.row(selectTask, [data[i].task_id]);
          task.push(fromTask);
          permmission.push({ action: data[i].action });
        }
        // PERULANGAN UNTUK MENGGABUNGKAN DATA TASK DAN PERMISSION
        let result = new Set();
        for (const p of permmission) {
          for (const t of task) {
            result.add(`${t.name}-${p.action}`);
          }
        }
        return [...result];
      };

      //   DATA MANIPULASI UNTUK MENGEMBALIKAN RESPONSE
      let response = await responseManipulation(data);
      //   JIKA RESPONSE DATA TIDAK DITEMUKAN MAKA PROSES YANG BERJALAN ADALAH DELETE TASK_IDS
      if (response.length == 0 || response == null) {
        return {
          message:
            "module permissions action & task has been updated | berhasil memperbaharui module & task",
        };
      }
      //   KEMBALIAN DATA JIKA RESPONSE BERISI SEBUAH DATA
      return {
        message:
          "success update task module | berhasil memperhabarui module task",
        data: response,
      };
      //   await db.raw("select setval('task_id', max(id)) from module_permissions"); //aktifkan ini untuk melakukan trigger insert pada sebuah table
    } catch (error) {
      throw new CoreException("something wrong | ada yang salah");
    }
  },

  validation: {
    task_ids: "required|array",
    action: "required|string",
    module_code: "required|string",
  },
};

module.exports = CoreService(service);
