const { CoreService, CoreException } = require("../../core/CallService");
const { Global } = require("../../util/globalFunction");

/**
 * Add Roles
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT r.* 
                FROM roles r 
                WHERE r.code = ? OR r.name = ?`;
    let existRole = await db.run_select(sql, [
      input.roles_code,
      input.roles_name,
    ]);

    if (existRole.length > 0) {
      throw new CoreException(
        "role code or name already taken | peran code atau nama sudah digunakan"
      );
    }

    input.existRole = existRole;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { roles_code, description, roles_name, existRole, currentUserId } =
      input;

    // ADD ROLES
    let data = {
      code: roles_code,
      name: roles_name,
      description: description,
      active: 1,
      created_by: currentUserId,
    };
    if (existRole == 0) {
      let insertRole = await db.run_insert("roles", data);
      let response = {
        message: "success insert roles",
        data: insertRole,
      };
      return response;
    }
  },

  validation: {
    roles_name: "required|string",
    roles_code: "required|string",
  },
};

module.exports = CoreService(service);
