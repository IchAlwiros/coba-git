const { CoreService, CoreException } = require("../../core/CallService");
const { Extend } = require("../../util/extendFunction");
const { Global } = require("../../util/globalFunction");

/**
 * Update Roles
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    let sql = `SELECT r.* 
                FROM roles r 
                WHERE r.code = ? OR r.name = ?`;
    let existRole = await db.run_select(sql, [
      input.roles_code,
      input.roles_name,
    ]);

    // if (existRole.length > 0) {
    //   throw new CoreException(
    //     "role code or name already taken | peran code atau nama sudah digunakan"
    //   );
    // }

    input.existRole = existRole;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      actived,
      roles_code,
      roles_name,
      existRole,
      description,
      currentUserId,
      roles_id,
    } = input;

    let data = {
      code: roles_code,
      name: roles_name,
      description: description,
      active: actived,
      updated_by: currentUserId,
    };
    if (existRole.length == 0) {
      let updateRoles = await db.run_update("roles", data, {
        id: roles_id,
      });

      let response = {
        message: "roles data success updated | data role berhasil di ubah",
        data: updateRoles,
      };

      return response;
    }
  },

  validation: {
    roles_id: "required|integer",
    roles_name: "required|string",
    roles_code: "required|string",
  },
};

module.exports = CoreService(service);
