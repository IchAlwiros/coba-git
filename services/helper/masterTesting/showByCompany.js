const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * List By Company Master
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [];
    input = filterBuilder(input, filter);

    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // FUNCTION TO OBTAIN COMPANY DATA
    const masterCompany = async () => {
      let sql = `SELECT c.id, c.uuid, c.name, c.phone, c.address, COUNT(pu.id) AS total_production_units, MAX(pu.subscription_code) AS subscription_code
                  FROM corporate c
                  LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                  GROUP BY c.id, c.uuid, c.name, c.phone, c.address
                  LIMIT ${input.limit} OFFSET ${input.offset};`;
      let dataCompany = await db.run_select(sql);

      return dataCompany;
    };

    let byCompany = await masterCompany();

    // FUNTION TO ACCOMPLISH DATA FROM RBS & DB DEV
    const tampungData = (item, el) => {
      let dataInvoice = {};
      let total_success = item.total_success;

      return (dataInvoice = {
        ...dataInvoice,
        id: el.id,
        uuid: el.uuid,
        corporate_name: el.name,
        corporate_contact: el.phone,
        corporate_address: el.address,
        total_production_unit: el.total_production_units,
        production_success: total_success,
        subscription_status: el.subscription_code,
      });
    };

    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;

    // TO GET RBS DATA FROM UUID CORPORATE
    const fetchData = async () => {
      const results = [];

      for (const el of byCompany) {
        try {
          let endPoint = `/support/summary/production-by-corporate/${el.uuid}?date[]=${input.date[0]}&date[]=${input.date[1]}`;

          const response = await Extend.fetchAPI(`${baseUrl}${endPoint}`);

          let datas = tampungData(response.data, el);

          results.push(datas);
        } catch (err) {
          throw new CoreException("something wrong | terjadi kesalahan");
        }
      }

      return results;
    };

    // RETURNING THE RESULT FROM THE DATA FECTH FUNCTION
    return fetchData()
      .then(async (data) => {
        // COUNT
        var sql2 = `SELECT COUNT(c.*) AS record
                    FROM corporate c
                    LEFT JOIN production_unit pu ON pu.corporate_id = c.id 
                    GROUP BY c.*`;

        let record = await db.row(sql2);

        return {
          data,
          record: record ? record.record : 0,
        };
      })
      .catch((error) => {
        console.log(error);
        throw new CoreException(
          " corporate was not found | corporate tidak ditemukan"
        );
      });
  },

  validation: {
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
