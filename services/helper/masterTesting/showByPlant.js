const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * List By Plant Master
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "created_at",
        alias: "pu",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `select pu.id produksi_unit_id,c."name" corporate_name,pu."name" produksi_unit_name, pu.phone produksi_unit_phone, pu.address produksi_unit_address, puc.unit_price production_unit_price, pu.subscription_code subscription_status
                from production_unit pu 
                left join corporate c on c.id = pu.corporate_id 
                left join production_unit_config puc ON puc.production_unit_id = pu.id
                ${input.filter}
                LIMIT ${input.limit} OFFSET ${input.offset}`;

    const data = await db.run_select(sql, input.filterValue);

    var sql2 = `SELECT COUNT(pu.*) AS record 
                FROM production_unit pu
                left join corporate c on c.id = pu.corporate_id 
                left join production_unit_config puc ON puc.production_unit_id = pu.id
                ${input.filter}`;

    let record = await db.row(sql2, input.filterValue);

    return {
      data,
      record: record ? record.record : 0,
    };
  },

  validation: {
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
