const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");

/**
 * Create Template Invoice Installation
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    // let { profile_id } = input;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { user_id } = input.session;
    let {
      invoice_category_id = 1,
      payment_channel_id = 3,
      number_invoice,
      price = 10000,
      tolerance,
      tax,
      billing_date = null,
    } = input;

    const insertValue = {
      invoice_category_id,
      payment_channel_id,
      number_invoice,
      price,
      tolerance,
      tax,
      billing_date,
      created_by: user_id,
      created_at: Global.currentDateTime(),
      updated_at: null,
    };

    // Insert Invoice Installation Template Config
    console.log(insertValue);
    let data = await db.run_insert("invoice_template_config", insertValue);

    return data;
  },

  validation: {
    number_invoice: "required|string",
    tolerance: "required|integer",
    tax: "required|integer",
  },
};

module.exports = CoreService(service);
