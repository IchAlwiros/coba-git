const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");

/**
 * Create Template Company
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      name,
      logo,
      tagline,
      address,
      email,
      suip_number,
      tdp_number,
      npwp_number,
      pic_name,
      pic_position,
      currentUserId,
    } = input;
    const insertValue = {
      name,
      logo,
      tagline,
      address: address || null,
      email,
      suip_number: suip_number || null,
      tdp_number: tdp_number || null,
      npwp_number,
      pic_name,
      pic_position,
      active: 1,
      created_by: currentUserId,
    };

    // COMPANY PROFILE INSERT
    let data = await db.run_insert("company_profile", insertValue);

    return data;
  },

  validation: {
    name: "required|string",
    logo: "string",
    tagline: "string",
    address: "string",
    email: "email",
    suip_number: "string",
    tdp_number: "string",
    npwp_number: "required|string",
    pic_name: "required|string",
    pic_position: "required|string",
    tagline: "required|string",
  },
};

module.exports = CoreService(service);
