const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");

/**
 * Create Template Invoice Subcribe
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let {
      invoice_category_id,
      payment_channel_id,
      format_number,
      tax,
      active,
      price,
      tolerance,
      currentUserId,
    } = input;

    const insertValue = {
      invoice_category_id,
      payment_channel_id,
      format_number,
      tax: tax || "1",
      active: active || "1",
      price: price || 10000,
      tolerance: tolerance || 30,
      created_by: currentUserId,
      created_at: Global.currentDateTime(),
    };

    // Insert Invoice Template Config
    let data = await db.run_insert("default_invoice_config", insertValue);

    return data;
  },

  validation: {
    format_number: "required|string",
    tolerance: "integer",
    tax: "integer",
  },
};

module.exports = CoreService(service);
