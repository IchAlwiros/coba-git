const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");

/**
 * List Summary Detail Company
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    let filter = [
      {
        field: "id",
        alias: "c",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { date } = input;
    // FUNSI UNTUK MENDAPATKAN DATA CORPORATE YANG DI GABUNGKAN
    const masterCompany = async () => {
      let sql = `SELECT pu.uuid, pu.active , puc.unit_price, pu.phone ,c.id , pu."name" as plant, c.name, c.address, MAX(pu.subscription_code) AS subscription_code
                  FROM corporate c
                  LEFT JOIN production_unit pu ON pu.corporate_id = c.id left join production_unit_config puc on puc.production_unit_id = pu.id 
                  ${input.filter}
                  GROUP BY pu.uuid,pu.active ,puc.unit_price,pu.phone, c.id ,pu."name", c.name, c.address`;
      let dataCompany = await db
        .run_select(sql, input.filterValue)
        .then((result) => {
          return result;
        })
        .catch((e) => {
          throw new CoreException("something wrong | ada sesuatu yang salah");
        });

      return dataCompany;
    };

    let byCompany = await masterCompany();

    // UNTUK MENGGABUNGKAN DATA DARI RBS & DATA TABLE
    const tampungData = (item, el) => {
      let dataInvoice = {};
      let total_success, total_cancel, total_order, total_schedule;
      total_success = item.total_success;
      total_cancel = item.total_cancel;
      total_order = item.total_order;
      total_schedule = item.total_schedule;

      //   console.log(el);
      return (dataInvoice = {
        ...dataInvoice,
        production_unit_name: el.plant,
        start_periode: date[0],
        end_periode: date[1],
        total_order: total_order,
        total_schedule: total_schedule,
        production_success: total_success,
        production_cancel: total_cancel,
        subscription_status: el.subscription_code,
        active: el.active,
      });
    };

    let baseUrl = `${process.env.config.INTEGRATION.BASE_URL}/`;

    // UNTUK MENDAPATKAN DATA RBS YANG BERACUAN DARI EL UUID CORPORATE
    const fetchData = async () => {
      const results = [];

      for (const el of byCompany) {
        try {
          let endPoint = `/support/summary/production-by-plant/${el.uuid}?date[]=${date[0]}&date[]=${date[1]}`;

          const response = await Extend.fetchAPI(`${baseUrl}${endPoint}`);

          let datas = tampungData(response.data, el);

          results.push(datas);
        } catch (err) {
          throw new CoreException("something wrong | ada sesuatu yang salah");
        }
      }

      const totalObject = results.reduce(
        (acc, curr) => {
          acc.total_order += curr.total_order || 0;
          acc.production_success += curr.production_success || 0;
          acc.production_cancel += curr.production_cancel || 0;
          acc.total_schedule += curr.total_schedule || 0;
          return acc;
        },
        {
          total_order: 0,
          total_schedule: 0,
          production_success: 0,
          production_cancel: 0,
        }
      );

      let sql = `SELECT COUNT(pu.*) as record
                   FROM production_unit pu 
                   LEFT JOIN corporate c ON pu.corporate_id = c.id 
                   ${input.filter}
                   `;

      let record = await db.row(sql, input.filterValue);

      let customStructure = {
        data: [...results],
        record: record ? record.record : 0,
        summary: totalObject,
      };

      return customStructure;
    };

    // MENGEMBALIKAN HASIL DARI FUNGSI FECTH DATA
    return fetchData()
      .then((data) => {
        // console.log(data); // Data hasil permintaan HTTP untuk setiap perusahaan
        return data;
      })
      .catch((error) => {
        throw new CoreException("something wrong | terjadi kesalahan");
      });
  },

  validation: {
    date: "required|array",
    "date.*": "date",
    limit: "integer|min:0",
    offset: "integer|min:0",
    order: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
