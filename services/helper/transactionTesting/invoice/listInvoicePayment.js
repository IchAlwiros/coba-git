const { CoreService, CoreException } = require("../../../../core/CallService");
const filterBuilder = require("../../../../util/filterBuilder");

/**
 * LIST INVOICE PAYMENT
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    // input.currentUserId = input.session.user_id

    input.filter = [];
    input.filterValue = [];

    input.searchField = [];
    input.searchValue = [];

    // CONFIG
    let filter = [
      //   {
      //     field: "invoice_payment",
      //     alias: "A",
      //     search: false,
      //     order: false,
      //     filter: true,
      //   },
    ];
    input = filterBuilder(input, filter);

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT A.id, A.uuid, A.invoice_id, B.periode AS periode_invoice, A.payment_channel_id, C.name AS payment_channel_name, A.receipt_number, A.amount, A.paid_at, A.status_code, A.attachment, A.description, A.active
              FROM invoice_payment A
              INNER JOIN invoice B ON B.id = A.invoice_id
              INNER JOIN payment_channel C ON C.id = A.payment_channel_id
              ${input.filter}          
              ORDER BY ${input.orderBy} ${input.sort}  LIMIT ${input.limit} OFFSET ${input.offset}`;
    let data = await db.run_select(sql, input.filterValue);

    // COUNT
    var sql2 = `SELECT COUNT(A.*) AS record 
                FROM invoice_payment A
                ${input.filter}`;
    let record = await db.row(sql2, input.filterValue);

    return {
      data: data,
      record: record ? record.record : 0,
    };
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
  },
};

module.exports = CoreService(service);
