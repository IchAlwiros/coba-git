const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");
const { Global } = require("../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const { Extend } = require("../../util/extendFunction");

/**
 * Create Invoice
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    let { corporate_id } = input;

    let validation = {};

    if (!corporate_id) {
      validation = {
        ...validation,
        corporate_id: `number not valid | number not valid `,
      };
    }

    if (Object.keys(validation).length > 0) {
      throw new CoreException("BAD REQUEST", validation, 400);
    }
  },

  process: async function (input, OriginalInput, db) {
    let { corporate_id, uuid_plant } = input;
    let { currentDate } = Global;
    let { generateTemplateNumberInvoice, generatePeriod } = Extend;

    // FUNTION TO HENDLE FIND THE PRODUCTION UNIT WHERE CORPORATE AND ACTIVE 1
    const selectPUC = async (corporateId) => {
      let sql = `select puc.corporate_id ,puc.production_unit_id ,puc.billing_date ,puc.unit_price, puc.tolerance, puc.active, puc.installation_fee from production_unit_config puc where puc.corporate_id = ? AND puc.active = '1'`;
      const unit_config = await db
        .raw(sql, [corporateId])
        .then((result) => {
          // Menampilkan hasil query
          return result.rows;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan

          throw new CoreException("something wrong | terjadi kesalahan");
        });
      return unit_config;
    };

    // FUNTION TO HENDLE INSERT INVOICE PAYMENT AFTER INVOICE CREATED
    const insertInvoicePayment = async (item) => {
      let paymentInvoice = {
        uuid: uuidv4(),
        invoice_id: item.id,
        payment_channel_id: null,
        receipt_number: item.invoice_number,
        amount: item.amount,
        paid_at: item.end_periode,
        status_code: "PENDING",
        active: 1,
      };
      await db
        .run_insert("invoice_payment", paymentInvoice)
        .then((resuslt) => {
          console.log(resuslt);
        })
        .catch((err) => {
          throw new CoreException(
            "invoice key number already exists | nomor invoice tersedia"
          );
        });
    };

    // FIND THE CORPORATE
    let sql = `select c.id ,c.code,c."name" from corporate c where c.uuid = ?`;

    const corporate_unit_cfg = await db
      .row(sql, [corporate_id])
      .then(async (corporate) => {
        // Menampilkan hasil query
        let dataPuc = await selectPUC(corporate.id);
        return dataPuc;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("something wrong | terjadi kesalahan");
      });

    let baseUrl = "https://api.teknologireadymix.com/rbs/dev/";
    let token = "14093f30c292a7686b34ced731cf7295";
    let dataPlant = await axios
      .get(
        `${baseUrl}/support/summary/production-by-plant/${uuid_plant}?date[]=2023-04-01&date[]=2023-04-30&status_code`,
        { headers: { Authorization: token } }
      )
      .then((e) => {
        return e.data.data;
      })
      .catch((err) => {
        throw new CoreException("something wrong | terjadi kesalahan");
      });

    // FUNTION TO HENDLE DATA INVOICE
    const insertInvoice = (item) => {
      let dataInvoice = {};
      let generateInvoice = generateTemplateNumberInvoice(currentDate());
      let periode = generatePeriod(item.billing_date);
      let unitPrice = item.unit_price;
      let quantity = dataPlant.total_success;
      let ammount = quantity * unitPrice;
      let taxAmount = (11 / 100) * ammount;
      let total_amount = taxAmount + ammount;
      return (dataInvoice = {
        ...dataInvoice,
        uuid: uuidv4(),
        corporate_id: item.corporate_id ? item.corporate_id : null,
        production_unit_id: item.production_unit_id
          ? item.production_unit_id
          : null,
        invoice_number: generateInvoice,
        unit_price: unitPrice,
        quantity: quantity,
        amount: ammount,
        tax: 1,
        tax_amount: taxAmount,
        total_amount: total_amount,
        total_paid: total_amount + taxAmount,
        periode: currentDate(),
        start_periode: periode.startDate,
        end_periode: periode.endDate,
        overdue: periode.endDate,
        status_code: "PENDING",
      });
    };

    // JIKA CORPORATE UNIT CONFIG DITEMUKAN MAKA PENJADWALAN PEMBUATAN INVOICE OTOMATIS AKAN DILAKUKAN SETIAP BULAN
    corporate_unit_cfg.forEach((item) => {
      let startMonth = moment(item.billing_date).format("M");
      let startDate = moment(item.billing_date).format("DD");
      //   CRON RUNNING
      const cron = require("node-cron");
      // console.log(startMonth);
      // console.log(startDate);
      //   let perPeriodMount = `0 0 ${startDate} ${startMonth + 1} *`;  //Penjadwalan perbulan
      const task = cron.schedule(
        "*/10 * * * * *",
        // perPeriodMount, //Aktifkan Untuk Schjule perbulan
        async () => {
          // INSERT INVOICE
          let dataInvoice = insertInvoice(item);
          //   console.log("cron berjalan");
          if (Object.keys(dataInvoice).length === 0) {
            throw new CoreException(
              "unit active config null | unit active config kosong"
            );
          } else {
            // console.log(dataInvoice);
            await db
              .run_insert("invoice", dataInvoice)
              .then((resuslt) => {
                // console.log(resuslt);
                // INSERT INVOCE PAYMENT
                insertInvoicePayment(resuslt);
              })
              .catch((err) => {
                throw new CoreException("something wrong | terjadi kesalahan");
              });
          }
        },
        {
          scheduled: false, // Menjadwalkan task secara manual
        }
      );
      // Menjalankan cron job setelah 10 detik
      // setTimeout(() => {
      task.start();
      // }, 10000);
    });

    // 0 0 1 * *
    // | | | | |
    // | | | | +---- Hari dalam seminggu (0-7, di sini 0 dan 7 merepresentasikan Minggu)
    // | | | +------ Bulan (1-12)
    // | | +-------- Tanggal (1-31)
    // | +---------- Jam (0-23)
    // +------------ Menit (0-59)
  },

  validation: {},
};

module.exports = CoreService(service);
