const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");
const { Extend } = require("../../../util/extendFunction");
const moment = require("moment");

const { v4: uuidv4 } = require("uuid");

/**
 * Total Invoice Pembayaran Subcribe & Insatallation
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    let { filterDinamis } = Extend;
    if (input.status === "ALL") {
      let sql = `SELECT  COUNT(*) AS total_records, SUM(ip.amount) AS total_invoice 
                  FROM invoice_payment ip 
                  left join invoice i on i.id = ip.invoice_id
                  left join corporate c on c.id = i.corporate_id`;
      const invoice_total = await db
        .row(sql)
        .then((result) => {
          // Menampilkan hasil query
          return result;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          throw new CoreException("something wrong | terjadi kesalahan");
        });

      input.invoice_total = invoice_total;
    }
    const selectCategoryInvoice = async (status) => {
      let dinamic = filterDinamis([`ic."name" = ?`], [status]);
      let sql = `select ic.id , ic."name", ic.code from invoice_category ic ${dinamic.filter}`;
      const unit_config = await db
        .row(sql, dinamic.filterValue)
        .then((result) => {
          // Menampilkan hasil query
          return result;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          throw new CoreException("something wrong | terjadi kesalahan");
        });
      return unit_config;
    };

    let dataCategory = await selectCategoryInvoice(input.status_subcribe);
    // SET FILTER
    input.filter = [`i.invoice_category_id = ?`];
    input.filterValue = [dataCategory.id];

    let filter = [
      {
        field: "status_code",
        alias: "ip",
        search: false,
        order: false,
        filter: true,
        param: "status_payment",
      },
      {
        field: "created_at",
        alias: "i",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ];
    input = filterBuilder(input, filter);

    input.orderBy = input.orderBy ? input.orderBy : "i.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { status_payment } = input;

    // SUM TOTAL BELUM LUNAS
    if (status_payment === "PENDING") {
      console.log("disini-PENDING");
      let sql = `SELECT COUNT(*) AS total_records, SUM(ip.amount) AS total_invoice 
                  FROM invoice_payment ip 
                  left join invoice i on i.id = ip.invoice_id
                  LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id            
                  left join corporate c on c.id = i.corporate_id 
                  ${input.filter}`;
      const invoice_total = await db
        .row(sql, input.filterValue)
        .then((result) => {
          // Menampilkan hasil query
          return result;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          throw new CoreException("something wrong | terjadi kesalahan");
        });

      return invoice_total;
    }
    // SUM TOTAL INVOICE LUNAS
    if (status_payment === "SUCCESS") {
      let sql = `SELECT COUNT(*) AS total_records, SUM(ip.amount) AS total_invoice 
                  FROM invoice_payment ip 
                  left join invoice i on i.id = ip.invoice_id
                  LEFT JOIN invoice_category ic ON ic.id = i.invoice_category_id            
                  left join corporate c on c.id = i.corporate_id 
                  ${input.filter}`;
      const invoice_total = await db
        .row(sql, input.filterValue)
        .then((result) => {
          // Menampilkan hasil query
          return result;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          throw new CoreException("something wrong | terjadi kesalahan");
        });

      return invoice_total;
    }

    return input.invoice_total;
  },

  validation: {
    corporate_id: "required|string",
    status_subcribe: "required|string|accepted:SUBSCRIBE,INSTALLATION",
    status_payment: "required|string|accepted:ALL,PENDING,SUCCESS",
  },
};

module.exports = CoreService(service);
