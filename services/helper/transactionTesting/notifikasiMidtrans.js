const { CoreService, CoreException } = require("../../../core/CallService");
const midtransClient = require("midtrans-client");

/**
 * Testing Midtrans
 */

const service = {
  input: function (request) {
    let input = request.query;

    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;
    return input;
  },

  process: async function (input, OriginalInput, db) {
    console.log(input.order_id);
    // Create Core API instance
    let apiClient = new midtransClient.CoreApi({
      isProduction: false,
      serverKey: "SB-Mid-server-1Bk-zVmiRTboviPjvzKMGgwC",
      clientKey: "SB-Mid-client-K7GXPcnTJsQMvtq4",
    });
    let midtransResponse = await apiClient.transaction
      .status(input.order_id)
      .then((response) => {
        // do something to `response` object
        // let responseMidtrans = JSON.stringify(response);
        // console.log(responseMidtrans);
        return response;
      });
    return midtransResponse;
  },

  validation: {
    // corporate_id: "required|string",
    // uuid_plant: "required|string",
  },
};

module.exports = CoreService(service);
