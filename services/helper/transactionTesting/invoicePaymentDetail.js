const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");
const { v4: uuidv4 } = require("uuid");

/**
 * Invoice Payment Gateway
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    // MENCARI DATA INVOICE PAYMENT
    let sql = `select * from invoice_payment ip where ip.uuid = ?`;
    let invoice_payment = await db
      .run_select(sql, [input.payment_id])
      .then((e) => {
        return e[0];
      })
      .catch((err) => {
        throw new CoreException(" data not found | data tidak tersedia");
      });

    input.invoice_payment = invoice_payment;

    // MENCARI DATA PAYMENT_CHANNEL
    let sql_pc = `select * from payment_channel ip where ip.uuid = ? AND payment_channel_category_id = 3`;
    let payment_channel = await db
      .run_select(sql_pc, [input.payment_channel])
      .then((e) => {
        return e[0];
      })
      .catch((err) => {
        throw new CoreException("data not found | data tidak tersedia");
      });

    input.payment_channel = payment_channel;

    // MENCARI DATA DETAIL PAYMENT UNTUK PROSES PENGUBAHAN DETAIL PAYMENT LAMA DARI PEMILIHAN CHANNEL PAYMENT LALU MEMBUAT DETAIL BARU
    let sql_payment_detail = `select ipd.id, ipd.uuid, ipd.payment_id, ipd.payment_channel_id from invoice_payment_detail ipd where ipd.payment_id = ? AND ipd.payment_channel_id = ?  `;
    let new_invoice_payment_detail = await db
      .run_select(sql_payment_detail, [invoice_payment.id, payment_channel.id])
      .then((e) => {
        return e;
      })
      .catch((err) => {
        throw new CoreException("data not found | data tidak tersedia");
      });
    // console.log(invoice_payment_detail);
    input.new_invoice_payment_detail = new_invoice_payment_detail;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { status } = input;
    let { user_id } = input.session;

    // console.log(input.new_invoice_payment_detail);

    if (input.new_invoice_payment_detail.length == 0) {
      // DATA BARU JIKA MENGUBAH PILIHAN BANK
      let insertNewDetailPaymentChoose = {
        uuid: uuidv4(),
        payment_id: input.invoice_payment.id,
        payment_channel_id: input.payment_channel.id,
        payment_number: input.invoice_payment.receipt_number,
        amount: input.invoice_payment.amount,
        admin_fee: 6500,
        total_amount: 6500 + input.invoice_payment.amount,
        expired_at: input.invoice_payment.paid_at,
        paid_at: input.invoice_payment.paid_at,
        status_code: input.invoice_payment.status_code,
        description: "pembayaran melalui payment gateway",
        active: "1",
        created_by: user_id,
        created_at: Global.currentDate(),
      };

      // MEINSERT DATA BARU UNTUK INVOICE PAYMENT DETAIL
      console.log("invoice detail baru harus dibuat");
      let data = await db.run_insert(
        "invoice_payment_detail",
        insertNewDetailPaymentChoose
      );

      // MENGUPDATE DATA ACTIVE INVOICE YANG LAMA
      let sql_payment_detail = `select ipd.id, ipd.uuid, ipd.payment_id, ipd.payment_channel_id from invoice_payment_detail ipd where ipd.payment_id = ? AND ipd.active = '1'`;
      let detail_last_payment_active = await db.run_select(sql_payment_detail, [
        input.invoice_payment.id,
      ]);

      if (detail_last_payment_active.length > 0) {
        await db.run_update(
          "invoice_payment_detail",
          { active: "0" },
          {
            id: parseInt(detail_last_payment_active[0].id),
          }
        );
      }
      return data;
    }

    if (input.new_invoice_payment_detail.length > 0) {
      // UPDATE DATA UNTUK MENGUBAH STATUS SUCCESS
      let updated_at = Global.currentDate();
      let updateValue = {
        status_code: status,
        updated_by: user_id,
        updated_at,
      };

      let data = await db.run_update("invoice_payment_detail", updateValue, {
        id: parseInt(input.new_invoice_payment_detail[0].id),
      });

      return data;
    }
  },

  validation: {
    payment_id: "required|string",
    payment_channel: "required|string",
    status: "required|string",
  },
};

module.exports = CoreService(service);
