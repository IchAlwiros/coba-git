const { CoreService, CoreException } = require("../../../core/CallService");
const { Global } = require("../../../util/globalFunction");

/**
 * Update Verifikasi Status Invoice
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    let sql = `select * from invoice_payment ip where ip.uuid = ?`;
    let last_data = await db
      .run_select(sql, [input.invoice_payment_id])
      .then((e) => {
        return e;
      })
      .catch((err) => {
        throw new CoreException("data not found | data tidak tersedia");
      });

    input.data_exists = last_data;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { status_code, invoice_payment_id } = input;
    let { user_id } = input.session;

    let updateValue = {
      status_code,
      updated_by: user_id,
      updated_at: Global.currentDateTime(),
    };

    let data = await db
      .run_update("invoice_payment", updateValue, {
        uuid: invoice_payment_id,
      })
      .then(async (e) => {
        await db.run_update("invoice", updateValue, { id: e.invoice_id });
        return e;
      })
      .catch((err) => {
        throw new CoreException(
          "something wrong | terjadi kesalahan saat update"
        );
      });
    return data;
  },

  validation: {
    invoice_payment_id: "required|string",
    status_code: "required|string|accepted:PROGRESS,PENDING,SUCCESS,CANCEL",
  },
};

module.exports = CoreService(service);
