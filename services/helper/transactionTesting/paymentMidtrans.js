const { CoreService, CoreException } = require("../../../core/CallService");
const midtransClient = require("midtrans-client");

/**
 * Testing Midtrans
 */

const service = {
  input: function (request) {
    let input = request.body;

    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    // Create Core API instance

    let coreApi = new midtransClient.CoreApi({
      isProduction: false,
      serverKey: "SB-Mid-server-1Bk-zVmiRTboviPjvzKMGgwC",
      clientKey: "SB-Mid-client-K7GXPcnTJsQMvtq4",
    });

    let parameter = {
      payment_type: "bank_transfer",
      bank_transfer: {
        bank: input.bank_name,
      },
      transaction_details: {
        gross_amount: input.gross_amount,
        order_id: input.order_id,
      },
    };

    // console.log(parameter);
    let midtransCore = await coreApi
      .charge(parameter)
      .then((chargeResponse) => {
        console.log("chargeResponse:", JSON.stringify(chargeResponse));
        // var dataOrder = {
        //   id: chargeResponse.order_id,
        //   tiket_id: req.body.tiket_id,
        //   nama: req.body.nama,
        //   response_midtrans: JSON.stringify(chargeResponse),
        // };
        // console.log(dataOrder);
        return chargeResponse;
      })
      .catch((e) => {
        return e.ApiResponse;
      });
    return midtransCore;
  },

  validation: {
    // corporate_id: "required|string",
    // uuid_plant: "required|string",
  },
};

module.exports = CoreService(service);
