const { CoreService, CoreException } = require("../../../core/CallService");
const { Extend } = require("../../../util/extendFunction");
const { Global } = require("../../../util/globalFunction");
const moment = require("moment");

/**
 * Testing Auto Generate Invoice
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    input.currentUserId = input.session.user_id;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { corporate_id, uuid_plant, date } = input;
    let { user_id } = input.session;
    let { generatePeriod, autoGenerateInvoice, generateTemplateNumberInvoice } =
      Extend;

    // let currentDate = moment().locale("id").utcOffset(7).format("YYYY-MM-DD");
    // let inputStr = "INV/2023/05";
    // let invNumber = generateTemplateNumberInvoice(currentDate);
    // console.log(invNumber);

    let sql = `SELECT i.created_at, i.status_code, ip.created_at created_at_payment, ip.status_code status_payment
                FROM invoice i
                INNER JOIN invoice_payment ip ON ip.invoice_id = i.id
                WHERE i.id = ?`;

    let select = await db.row(sql, [134]);

    console.log(select);
    // const invoice = {
    //   status: "pending",
    //   created_at: "2023-06-09",
    // };

    // Mendapatkan tanggal saat ini menggunakan Moment.js
    // const currentDate = new Date();
    const currentDate = moment();

    // Mendapatkan tanggal pembuatan faktur dari properti invoice menggunakan Moment.js
    // const createdAt = new Date(select.created_at);
    const createdAt = moment(select.created_at);

    // const timeDiff = currentDate.getTime() - createdAt.getTime();

    // Menghitung selisih hari antara tanggal saat ini dan tanggal pembuatan faktur
    // const diffDays = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    const diffDays = currentDate.diff(createdAt, "days");
    console.log(diffDays);
    // Memeriksa apakah selisih hari lebih dari 7 hari
    if (diffDays > 7) {
      // Mengizinkan pengubahan status
      // invoice.status = "changed";
      console.log("boleh di ubah");
    } else {
      // Menolak pengubahan status
      console.log("Status tidak dapat diubah sebelum 7 hari berlalu.");
    }

    // console.log(invoice.status);

    // return autoGenerateInvoice(
    //   db,
    //   corporate_id,
    //   uuid_plant,
    //   user_id,
    //   date,
    //   generateTemplateNumberInvoice,
    //   generatePeriod
    // );
  },

  validation: {
    corporate_id: "required|string",
    uuid_plant: "required|string",
  },
};

module.exports = CoreService(service);
