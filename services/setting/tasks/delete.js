const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service * TASKS*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        var sql = ` SELECT A.id, A.code,A.name
                    FROM tasks A WHERE A.id = ?`
        let data = await db.row(sql, [input.id])
        if (!data) throw new CoreException(`data unavailable|data tidak tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        await db.run_delete(`tasks`, { id: input.id })
        return {
            message: 'Data deleted successfully',
            data: {
                id: input.id
            }
        }
    },
    validation: {
        id: "required|integer"
    }
}

module.exports = CoreService(service)