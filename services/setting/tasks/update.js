const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service UPDATE TASKS
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        let checkData = await db.row(`SELECT*FROM tasks WHERE id = ?`, [input.id])
        if (!checkData) throw new CoreException('data unavailable|data tidak tersedia')

        let checkDuplicate = await db.row(`SELECT*FROM tasks WHERE code = ? AND id != ?`, [input.code, input.id])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_update(`tasks`, {
            code: input.code,
            name: input.name,
            description: input.description
        }, {
            id: input.id
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        id: 'required|integer',
        code: 'required',
        name: 'required',
    }
}


module.exports = CoreService(service)