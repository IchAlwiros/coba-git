const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service * VIEW TASKS*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.code,A.name,A.description
                    FROM tasks A WHERE A.id = ?`
        let data = await db.row(sql, [input.id])
        if (!data) throw new CoreException(`data unavailable|data tidak tersedia`)
        return data
    },
    validation: {
        id: "required|integer"
    }
}

module.exports = CoreService(service)