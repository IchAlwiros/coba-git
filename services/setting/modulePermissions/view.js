const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service PERMISSIONS MODULE UPDATE
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        // CHECK MODULE
        var sql = ` SELECT A.module_group_id, A.id AS module_id,B.code AS module_group_code, 
                    B.name AS module_group_name,A.display_number AS module_order, B.display_number AS group_module_order,
                    A.code, A.name, CASE WHEN A.active ='1' THEN TRUE ELSE FALSE END AS active,
                    to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM modules A
                    INNER JOIN module_groups B ON B.id = A.module_group_id
                    WHERE A.id = ?`
        input.dataModule = await db.row(sql, [input.module_id])
        if (!input.dataModule) throw new CoreException(`Module unavailable|Modul tidak tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.code, A.name,B.action FROM tasks A
                    LEFT JOIN module_permissions B ON B.task_id = A.id AND B.module_id = ?
                    ORDER BY A.name ASC`
        let data = await db.run_select(sql, [input.module_id])

        let accessTasks = {}
        for (let item of data) {
            let acc = accessTasks[item.id]
            if (acc) {
                if (item.action == 'create') accessTasks[item.id].create = true
                if (item.action == 'read') accessTasks[item.id].read = true
                if (item.action == 'view') accessTasks[item.id].view = true
                if (item.action == 'lookup') accessTasks[item.id].lookup = true
                if (item.action == 'delete') accessTasks[item.id].delete = true
                if (item.action == 'update') accessTasks[item.id].update = true
                if (item.action == 'export') accessTasks[item.id].export = true
                if (item.action == 'cancel') accessTasks[item.id].cancel = true
            } else {
                accessTasks[item.id] = {
                    task_id: item.id,
                    task_code: item.code,
                    task_name: item.name,
                    create: item.action == 'create' ? true : false,
                    read: item.action == 'read' ? true : false,
                    view: item.action == 'view' ? true : false,
                    lookup: item.action == 'lookup' ? true : false,
                    delete: item.action == 'delete' ? true : false,
                    update: item.action == 'update' ? true : false,
                    cancel: item.action == 'cancel' ? true : false,
                    export: item.action == 'export' ? true : false,
                }
            }
        }
        let permissions = []
        for (let item in accessTasks) {
            permissions.push(accessTasks[item])
        }
        return {
            info: input.dataModule,
            permissions: permissions
        }
    },
    validation: {
        module_id: "required|integer"
    }
}


module.exports = CoreService(service)