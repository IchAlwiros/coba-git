const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service PERMISSIONS MODULE UPDATE
 */

const service = {
    transaction: true,
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentDateTime = Global.currentDateTime()
        input.currentUserId = input.session.user_id

        // CHECK MODULE
        let module = await db.row(`SELECT*FROM modules WHERE id = ? `, [input.module_id])
        if (!module) throw new CoreException(`Module unavailable|module tidak ditemukan`)

        // BUILD DATA TASKS
        let inputPermissions = {}
        input.dataPermissions = []
        for (let item of input.permissions) { inputPermissions[item.task_id] = item }
        let data = await db.run_select(`SELECT*FROM tasks ORDER BY id ASC`)
        for (let item of data) {
            let val = ['create', 'read', 'view', 'lookup', 'delete', 'update', 'export','cancel']
            let acc = inputPermissions[item.id]
            if (acc) {
                for (let i of val) {
                    if (acc[i]) {
                        input.dataPermissions.push({
                            module_id: input.module_id,
                            task_id: item.id,
                            action: i,
                            attribute: '*',
                            possession: 'any'
                        })
                    }
                }
            }
        }

        return input
    },

    process: async function (input, OriginalInput, db) {
        await db.run_delete(`module_permissions`, { module_id: input.module_id })
        await db.run_insert(`module_permissions`, input.dataPermissions, true)
        return {
            message: 'Data saved successfully'
        }
    },
    validation: {
        module_id: "required|integer",
        permissions: "required|array",
        "permissions.*.task_id": "required|integer",
        "permissions.*.create": "required|boolean",
        "permissions.*.read": "required|boolean",
        "permissions.*.view": "required|boolean",
        "permissions.*.lookup": "required|boolean",
        "permissions.*.delete": "required|boolean",
        "permissions.*.update": "required|boolean",
        "permissions.*.export": "required|boolean",
        "permissions.*.cancel": "required|boolean",
    }
}


module.exports = CoreService(service)