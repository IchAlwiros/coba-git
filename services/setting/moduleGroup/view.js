const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service * VIEW ROLES*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.code,A.name,A.display_number
                    FROM module_groups A WHERE A.id = ?`
        let data = await db.row(sql, [input.id])
        if (!data) throw new CoreException(`data unavailable|data tidak tersedia`)
        return data
    },
    validation: {
        id: "required|integer"
    }
}

module.exports = CoreService(service)