const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service * LIST ROLES*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        var sql = ` SELECT A.id, A.code,A.name
                    FROM module_groups A WHERE A.id = ?`
        let data = await db.row(sql, [input.id])
        if (!data) throw new CoreException(`data unavailable|data tidak tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        await db.run_delete(`module_groups`, { id: input.id })
        return {
            message: 'Data deleted successfully',
            data: {
                id: input.id
            }
        }
    },
    validation: {
        id: "required|integer"
    }
}

module.exports = CoreService(service)