const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service CREATE MODULE GROUP
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()
        let checkDuplicate = await db.row(`SELECT*FROM module_groups WHERE code = ?`, [input.code])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_insert(`module_groups`, {
            code: input.code,
            name: input.name,
            display_number: input.display_number || 1,
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        code: 'required',
        name: 'required',
        display_number: 'integer',
    }
}


module.exports = CoreService(service)