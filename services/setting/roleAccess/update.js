const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service PERMISSIONS UPDATE
 */

const service = {
    transaction: true,
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentDateTime = Global.currentDateTime()
        input.currentUserId = input.session.user_id

        if (input.role_id < 0) throw new CoreException(`Can't Update permissions, please use another roles`)

        // CHECK ROLES 
        let roles = await db.row(`SELECT*FROM roles WHERE id = ?  AND id >= 0  AND active ='1'`, [input.role_id])
        if (!roles) throw new CoreException(`Role unavailable|Hak akses tidak tersedia`)

        // VALIDATION MODULE
        let inputModule = {}
        input.accessModule = []
        for (let item of input.access_module) { inputModule[item.module_id] = item }
        let dataModule = await db.run_select(`SELECT * FROM modules WHERE module_group_id = ? `, [input.module_group_id])

        for (let item of dataModule) {
            inputModule[item.id] = item
        }

        for (let item of input.access_module) {
            if (!inputModule[item.module_id]) throw new CoreException(`Module unavailable`)
            if (item.allowed) {
                input.accessModule.push({
                    role_id: input.role_id,
                    module_id: item.module_id,
                    allowed: '1',
                    active: '1',
                    created_at: input.currentDateTime,
                    created_by: input.currentUserId
                })
            }
        }

        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = `DELETE FROM role_details 
                   USING modules A WHERE A.id = role_details.module_id AND role_details.role_id = ? AND A.module_group_id = ?`
        await db.row(sql,[input.role_id, input.module_group_id])

        if (input.accessModule.length > 0) await db.run_insert(`role_details`, input.accessModule, true)
        return {
            message: 'Data saved successfully',
        }
    },
    validation: {
        role_id: "required|integer",
        module_group_id: 'required|integer',
        access_module: 'required|array',
        "access_module.*.module_id": "required|integer",
        "access_module.*.allowed": "required|boolean",
    }
}


module.exports = CoreService(service)