const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service VIEW ROLE PERMISSIONS
 */

const service = {
    input: function (request) {
        let input = request.query
        input.role_id = request.params.role_id
        return input
    },

    prepare: async function (input, db) {
        input.filter = [`A.active ='1'`]
        input.filterValue = [input.role_id]

        if (input.module_group_id) {
            input.filter.push(`B.id = ?`)
            input.filterValue.push(input.module_group_id)
        }

        input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(' AND ')}` : ''
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.module_group_id,A.id AS module_id,B.code AS module_group_code, B.name AS module_group_name,
                    A.code, A.name, A.description,
                    CASE WHEN C.allowed ='1' THEN TRUE ELSE FALSE END AS allowed
                    FROM modules A
                    INNER JOIN module_groups B ON B.id = A.module_group_id
                    LEFT JOIN role_details C ON C.module_id = A.id AND C.role_id = ? AND C.active ='1'
                    ${input.filter}  ORDER BY A.display_number ASC`
        let accessModule = await db.run_select(sql, input.filterValue)
        return accessModule
    },
    validation: {
        role_id: 'required|integer',
        module_group_id: 'integer'
    }
}


module.exports = CoreService(service)