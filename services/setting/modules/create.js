const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service CREATE MODULE
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        let checkDuplicate = await db.row(`SELECT*FROM modules WHERE code = ?`, [input.code])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)

        let checkGroup = await db.row(`SELECT*FROM module_groups WHERE id = ?`, [input.module_group_id])
        if (!checkGroup) throw new CoreException(`Module group unavailable|grup module tidak tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_insert(`modules`, {
            code: input.code,
            name: input.name,
            module_group_id:input.module_group_id,
            display_number:input.display_number || 1,
            description:input.description,
            active: ['0', '1'].includes(input.active) ? input.active : '1',
            created_at: input.currentDateTime,
            created_by: input.currentUserId
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        module_group_id: 'required|integer',
        code: 'required',
        name: 'required',
        display_number: 'integer',
        active: 'accepted:0,1'
    }
}


module.exports = CoreService(service)