const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service LIST MODULE ALL
 */

const service = {
    input: function (request) {
        return request.query
    },

    prepare: async function (input, db) {
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id,A.module_group_id, B.code AS module_group_code,B.name AS module_group_name,
                    A.code, A.name, A.description,A.display_number, A.active 
                    FROM modules A
                    INNER JOIN module_groups B ON B.id = A.module_group_id
                    WHERE A.active ='1' ORDER BY B.display_number, A.display_number ASC`
        let data = await db.run_select(sql)
        return {
            data: data,
            record: data.length
        }
    },
    validation: {

    }
}


module.exports = CoreService(service)