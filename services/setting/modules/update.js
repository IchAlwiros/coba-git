const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service UPDATE MODULES
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        let checkData = await db.row(`SELECT*FROM modules WHERE id = ?`, [input.id])
        if (!checkData) throw new CoreException('data unavailable|data tidak tersedia')

        let checkDuplicate = await db.row(`SELECT*FROM modules WHERE code = ? AND id != ?`, [input.code, input.id])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)

        let checkGroup = await db.row(`SELECT*FROM module_groups WHERE id = ?`, [input.module_group_id])
        if (!checkGroup) throw new CoreException(`Module group unavailable|grup module tidak tersedia`)

        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_update(`modules`, {
            code: input.code,
            name: input.name,
            module_group_id: input.module_group_id,
            display_number: input.display_number,
            description:input.description,
            active: ['0', '1'].includes(input.active) ? input.active : '1',
            updated_at: input.currentDateTime,
            updated_by: input.currentUserId
        }, {
            id: input.id
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        id: 'required|integer',
        module_group_id: 'required|integer',
        code: 'required',
        name: 'required',
        display_number: 'required|integer',
        active: 'required|accepted:0,1'
    }
}


module.exports = CoreService(service)