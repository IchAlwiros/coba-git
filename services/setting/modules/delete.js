const { CoreService, CoreException } = require('../../../core/CallService')

/**
 * Service * MODULES*
 */

const service = {
    input: function (request) {
        return request.params
    },

    prepare: async function (input, db) {
        var sql = ` SELECT A.id, A.code,A.name, A.active,to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM modules A WHERE A.id = ?`
        let data = await db.row(sql, [input.id])
        if (!data) throw new CoreException(`data unavailable|data tidak tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        await db.run_delete(`modules`, { id: input.id })
        return {
            message: 'Data deleted successfully',
            data: {
                id: input.id
            }
        }
    },
    validation: {
        id: "required|integer"
    }
}

module.exports = CoreService(service)