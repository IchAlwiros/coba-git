const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service CREATE ROLES
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        let checkDuplicate = await db.row(`SELECT*FROM roles WHERE code = ?`, [input.code])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_insert(`roles`, {
            code: input.code,
            name: input.name,
            active: ['0', '1'].includes(input.active) ? input.active : '1',
            created_at: input.currentDateTime,
            created_by: input.currentUserId
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        code: 'required',
        name: 'required',
        active: 'accepted:0,1'
    }
}


module.exports = CoreService(service)