const { CoreService } = require('../../../core/CallService')
const filterBuilder = require('../../../util/filterBuilder')

/**
 * Service * LIST ROLES*
 */

const service = {
    input: function (request) {
        let input = request.query
        return input
    },

    prepare: async function (input, db) {
        // CONFIG
        let filter = [
            { field: 'name', alias: 'A', search: true, order: true, filter: false },
            { field: 'code', alias: 'A', search: true, order: true, filter: false },
            { field: 'active', alias: 'A', search: false, order: true, filter: true },
        ]
        input = filterBuilder(input, filter)

        // INIT VALUE
        input.orderBy = input.orderBy ? input.orderBy : 'A.id'
        input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(' AND ')}` : ''
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.code,A.name, A.active,to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM roles A
                    ${input.filter} ORDER BY ${input.orderBy} ${input.sort}  LIMIT ${input.limit} OFFSET ${input.offset}`
        let data = await db.run_select(sql, input.filterValue)

        // COUNT
        var sql2 = `SELECT COUNT(A.*) AS record 
                    FROM roles A ${input.filter} `
        let record = await db.row(sql2, input.filterValue)
        return {
            data: data,
            record: record ? record.record : 0
        }
    },
    validation: {
        limit: "integer|min:0",
        offset: "integer|min:0",
    }
}

module.exports = CoreService(service)