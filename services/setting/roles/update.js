const { CoreService, CoreException } = require('../../../core/CallService')
const { Global } = require('../../../util/globalFunction')

/**
 * Service UPDATE ROLES
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentDateTime = Global.currentDateTime()

        let checkData = await db.row(`SELECT*FROM roles WHERE id = ?`, [input.id])
        if (!checkData) throw new CoreException('data unavailable|data tidak tersedia')

        if (checkData.id < 1) throw new CoreException(`Role can't updated|peran tidak dapat diubah`)

        let checkDuplicate = await db.row(`SELECT*FROM roles WHERE code = ? AND id != ?`, [input.code, input.id])
        if (checkDuplicate) throw new CoreException(`Code has been available|kode telah tersedia`)
        return input
    },

    process: async function (input, OriginalInput, db) {
        let data = await db.run_update(`roles`, {
            code: input.code,
            name: input.name,
            active: ['0', '1'].includes(input.active) ? input.active : '1',
            updated_at: input.currentDateTime,
            updated_by: input.currentUserId
        }, {
            id: input.id
        })
        return {
            message: 'Data saved successfully',
            data: data
        }
    },
    validation: {
        id: 'required|integer',
        code: 'required',
        name: 'required',
        active: 'required|accepted:0,1'
    }
}


module.exports = CoreService(service)