const { CoreService, CoreException } = require("../../../core/CallService")
const { Extend } = require("../../../util/extendFunction")
const filterBuilder = require("../../../util/filterBuilder")

/**
 * HISTORY PRODUCTION REALIZATION
 */

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    // DATE VALIDATION 
    input.date = Extend.dateValidation(input.date)

    // SET SESSION
    input.currentUserId = input.session.user_id

    // SET FILTER
    input.filter = ["A.invoice_category_id = ?"]
    input.filterValue = ["2"]
    let filter = [
      {
        field: "id",
        alias: "B",
        search: false,
        order: true,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "id",
        alias: "C",
        search: false,
        order: true,
        filter: true,
        param: "production_unit_id",
      },
      {
        field: "start_periode",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        type: "range-date",
        param: "date",
      },
    ]
    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""

    // GET data BETWEEN range-date
    let sql = `SELECT A.id, A.corporate_id, B.uuid AS corporate_uuid, B.name AS corporate_name, A.production_unit_id, C.uuid AS production_unit_uuid, C.name AS production_unit_name, C.subscription_code, C.pic_name, C.pic_phone, A.unit_price, A.quantity, A.amount AS amount_non_tax, A.start_periode, A.overdue AS subscribe_end
                FROM invoice A
                INNER JOIN corporate B 
                ON B.id = A.corporate_id
                INNER JOIN production_unit C
                ON C.id = A.production_unit_id
                ${input.filter}
                ORDER BY ${input.orderBy} ${input.sort}  LIMIT ${input.limit} OFFSET ${input.offset}`
    input.invoices = await db.run_select(sql, input.filterValue)

    // console.log(input.filter);
    // console.log(input.filterValue);

    return input
  },

  process: async function (input, OriginalInput, db) {
    const { invoices } = input
    const result = []
    // return invoices

    for (const item of invoices) {
      result.push({
        id: item.production_unit_id,
        corporate_id: item.corporate_id,
        corporate_name: item.corporate_name,
        production_unit_name: item.production_unit_name,
        production_volume: item.quantity,
        unit_price: item.unit_price,
        invoice_amount: item.amount_non_tax,
        pic_name: item.pic_name,
        pic_phone: item.pic_phone,
        subscribe_status: item.subscription_code,
        subscribe_end: item.subscribe_end,
      })
    }

    return {
      data: result,
      record: result.length,
    }
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    corporate_id: "integer",
    production_unit_id: "integer",
    date: "array",
    sort: "string",
  },
}

module.exports = CoreService(service)
