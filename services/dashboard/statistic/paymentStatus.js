const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")

/*
  PAYMENT STATUS CHART
*/

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    // DATE VALIDATION
    input.date = Extend.dateValidation(input.date)

    // SET SESSION
    input.currentUserId = input.session.user_id

    // SET FILTER
    input.filter = []
    input.filterValue = []

    input.searchField = []
    input.searchValue = []

    // CONFIG
    let filter = [
      {
        field: "corporate_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "production_unit_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "start_periode",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "date",
        type: "range-date",
      },
    ]

    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""
    return input
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT 
               COUNT(*) AS total_payment,
               SUM(CASE WHEN A.status_code = 'SUCCESS' THEN 1 ELSE 0 END) AS success,
               SUM(CASE WHEN A.status_code = 'PROGRESS' THEN 1 ELSE 0 END) AS progress,
               SUM(CASE WHEN A.status_code = 'PENDING' THEN 1 ELSE 0 END) AS pending,
               SUM(CASE WHEN A.status_code = 'CANCEL' THEN 1 ELSE 0 END) AS cancel,
               (SUM(CASE WHEN A.status_code = 'SUCCESS' THEN 1 ELSE 0 END)::decimal / COUNT(*)) * 100 AS percent_success,
               (SUM(CASE WHEN A.status_code = 'PROGRESS' THEN 1 ELSE 0 END)::decimal / COUNT(*)) * 100 AS percent_progress,
               (SUM(CASE WHEN A.status_code = 'PENDING' THEN 1 ELSE 0 END)::decimal / COUNT(*)) * 100 AS percent_pending,
               (SUM(CASE WHEN A.status_code = 'CANCEL' THEN 1 ELSE 0 END)::decimal / COUNT(*)) * 100 AS percent_cancel
               FROM invoice A
               ${input.filter}
               `
    //  LIMIT ${input.limit}
    //  OFFSET ${input.offset}

    // console.log(sql)
    // console.log(input.filter)
    // console.log(input.filterValue)
    let data = await db.row(sql, input.filterValue)

    if (data.total_payment == 0) {
      data.success = 0
      data.progress = 0
      data.pending = 0
      data.cancel = 0
      data.percent_success = 0
      data.percent_progress = 0
      data.percent_pending = 0
      data.percent_cancel = 0
    }
    return data
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    corporate_id: "integer",
    production_unit_id: "integer",
    date: "array",
    sort: "string",
  },
}

module.exports = CoreService(service)
