const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")

/*
  DEMOGRAPHIC PRODUCTION UNIT
*/

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    const { BASE_URL } = process.env.config

    // SET FILTER
    input.filter = ["A.active = ?"]
    input.filterValue = ["1"]

    input.searchField = []
    input.searchValue = []

    // CONFIG
    let filter = [
      {
        field: "id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "id",
        alias: "B",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
    ]
    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""

    input.dummyAddress = [
      {
        uuid: "AAA",
        name: "Plant A",
        province_name: "Jawa Tengah",
      },
      {
        uuid: "BBB",
        name: "Plant B",
        province_name: "Jawa Tengah",
      },
      {
        uuid: "CCC",
        name: "Plant C",
        province_name: "Jawa Barat",
      },
      {
        uuid: "DDD",
        name: "Plant D",
        province_name: "Bali",
      },
      {
        uuid: "EEE",
        name: "Plant E",
        province_name: "Bali",
      },
      {
        uuid: "EEE",
        name: "Plant E",
        province_name: "Bali",
      },
    ]
    let dataProvince = {}
    let totalProvince = 0
    input.dummyAddress.forEach((item) => {
      let province = item.province_name.toLowerCase().replace(/\s+/g, '_')
      console.log(province)
      dataProvince[province] = (dataProvince[province] || 0) + 1
      totalProvince++
    })

    for (const name in dataProvince) {
      const value = dataProvince[name]
      const percentage = (value / totalProvince) * 100
      dataProvince[`percent_${name}`] = percentage.toFixed(2)
    }

    return dataProvince

    // FETCH data from RBS
    const corporates = await db.run_select(`SELECT A.uuid FROM corporate A`)

    input.production_unit = []
    for (const item of corporates) {
      let url = `${process.env.config.INTEGRATION.BASE_URL}/rbs/dev/support/data/production-unit/${item.uuid}`

      const result = await Global.fetchAPI(url)
      for (const unit of result.data.production_unit) {
        input.production_unit.push(unit)
      }
    }

    return input
  },

  process: async function (input, OriginalInput, db) {
    return input

    // const addressGraph = {
    //   province_name: '',
    //   unit_sum : 0
    // }

    // return input.production_unit
    // let columnName = `B.id, B.uuid, B.corporate_id, A.name AS corporate_name, B.code, B.name, B.phone, B.email, B.address, B.registered_at, B.active, B.subscription_code`;
    // let sql = `SELECT ${columnName}
    //            FROM production_unit B
    //            INNER JOIN corporate A
    //            ON A.id = B.corporate_id
    //            `;

    // let result = await db.run_select(sql);
    // let addressPart, province = []

    // for (const item of result) {
    //   addressPart = item.address
    //   province.push(item.address)
    // }

    // // console.log(addressPart);

    // return result
  },
  validation: {},
}

module.exports = CoreService(service)
