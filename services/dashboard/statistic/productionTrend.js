const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const axios = require("axios")
const { Extend } = require("../../../util/extendFunction")
require("dotenv").config()

/**
 * PRODUCTION REALIZATION TREND
 */

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    const { corporate_id, production_unit_id, date } = input

    // DATE VALIDATION
    input.date = Extend.dateValidation(input.date)

    // SET SESSION
    input.currentUserId = input.session.user_id

    // SET FILTER
    input.filter = ["A.status_code IN (?,?,?)"]
    input.filterValue = ["PENDING", "PROGRESS", "SUCCESS"]

    let filter = [
      {
        field: "corporate_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "production_unit_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
      {
        field: "start_periode",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "date",
        type: "range-date",
      },
    ]

    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""

    // console.log(input.filter)
    // console.log(input.filterValue)

    let sql = `SELECT A.id, A.quantity, A.corporate_id, A.production_unit_id, A.total_amount, A.status_code
               FROM invoice A
               ${input.filter}
               `
    //  ORDER BY ${input.orderBy} ${input.sort}
    //  LIMIT ${input.limit}
    //  OFFSET ${input.offset}
    input.realization = await db.run_select(sql, input.filterValue)

    return input
  },

  process: async function (input, OriginalInput, db) {
    const { realization, date } = input

    const data = {
      total_data: realization.length,
      total_realization: 0,
      average_realization_per_plant: 0,
      median_realization: 0,
      total_invoice: 0,
      average_invoice: 0,
      median_invoice: 0,
      trend_chart: [],
    }

    if (realization.length == 0) {
      data.total_data = 0
      data.total_realization = 0
      data.average_realization_per_plant = 0
      data.median_realization = 0
      data.total_invoice = 0
      data.average_invoice = 0
      data.median_invoice = 0
      return data
    }

    // =========================== CALCULATE PRODUCTION REALIZATION
    const realizationPerUnit = []
    const invoicePerUnit = []

    for (const item of realization) {
      // TOTAL REALIZATION
      if (isNaN(item.quantity)) throw new CoreException("Data realization must be a number|Data realisasi harus berupa angka")
      data.total_realization += Number(item.quantity)

      // TOTAL INVOICE
      if (isNaN(item.total_amount)) throw new CoreException("Total Amount must be a number|Total Harga harus berupa angka")
      data.total_invoice += Number(item.total_amount)

      // array data that used to calculate median later
      realizationPerUnit.push(item.quantity)
      invoicePerUnit.push(item.total_amount)
    }

    //  AVERAGE REALIZATION
    data.average_realization_per_plant += parseFloat((data.total_realization / realization.length).toFixed(2))

    //  MEDIAN REALIZATION
    data.median_realization = Extend.calculateMedian(realizationPerUnit)

    // =========================== CALCULATE INVOICE

    // AVERAGE INVOICE
    data.average_invoice = parseFloat((data.total_invoice / realization.length).toFixed(2))

    // MEDIAN INVOICE
    data.median_invoice = Extend.calculateMedian(invoicePerUnit)

    return data
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    sort: "string",
    corporate_id: "integer",
    production_unit_id: "integer",
    date: "array",
  },
}

module.exports = CoreService(service)
