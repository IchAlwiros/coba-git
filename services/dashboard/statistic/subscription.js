const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")

/**
 * SUBSCRIPTION CORPORATE AND PRODUCTION UNIT
 */

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    // DATE VALIDATION
    input.date = Extend.dateValidation(input.date)

    // SET SESSION
    input.currentUserId = input.session.user_id

    // SET FILTER
    input.filter = []
    input.filterValue = []

    input.searchField = []
    input.searchValue = []

    // CONFIG
    let filter = [
      {
        field: "id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "id",
        alias: "B",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
    ]
    input = filterBuilder(input, filter)

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id"
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""
    return input
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT B.id, B.corporate_id, B.name, B.subscription_code, B.active, A.active AS corporate_active
                FROM production_unit B
                INNER JOIN corporate A 
                ON A.id = B.corporate_id
                ${input.filter}
                `
    // ORDER BY ${input.orderBy} ${input.sort}
    // LIMIT ${input.limit}
    // OFFSET ${input.offset}
    let data = await db.run_select(sql, input.filterValue)
    // console.log(data)

    let sql2 = `SELECT COUNT (CASE WHEN C.active = '1' THEN 1 ELSE 0 END) AS user_active
                FROM users C`

    let user = await db.row(sql2)

    let dataActive = {},
      totalProductionUnit = 0
    data.forEach((element) => {
      let corporateID = element.corporate_id

      if (Object.hasOwn(dataActive, corporateID)) {
        element.active == 1 ? dataActive[corporateID].push(element.id) : null
      } else if (element.corporate_active == 1) {
        dataActive[corporateID] = []
        if (element.active == 1) dataActive[corporateID].push(element.id)
      }
    })
    // console.log(dataActive)

    for (const key in dataActive) {
      totalProductionUnit += dataActive[key].length
    }

    return {
      corporate_active: Object.keys(dataActive).length,
      production_unit_active: Number(totalProductionUnit),
      user_active: user.user_active,
    }
  },

  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    corporate_id: "integer",
    production_unit_id: "integer",
  },
}

module.exports = CoreService(service)
