const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const axios = require("axios")
const { Extend } = require("../../../util/extendFunction")
require("dotenv").config()

/**
 * PRODUCTION REALIZATION TREND
 */

const service = {
  input: function (request) {
    let input = request.query
    return input
  },

  prepare: async function (input, db) {
    const { corporate_id, production_unit_id, date } = input

    // DATE VALIDATION
    input.date = Extend.dateValidation(input.date)

    // SET SESSION
    input.currentUserId = input.session.user_id

    // SET FILTER
    input.filter = ["A.status_code IN (?,?,?)"]
    input.filterValue = ["PENDING", "PROGRESS", "SUCCESS"]

    let filter = [
      {
        field: "corporate_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "corporate_id",
      },
      {
        field: "production_unit_id",
        alias: "A",
        search: false,
        order: false,
        filter: true,
        param: "production_unit_id",
      },
      {
        field: "start_periode",
        alias: "A",
        search: false,
        order: true,
        filter: true,
        param: "date",
        type: "range-date",
      },
    ]

    input = filterBuilder(input, filter)

    if (!date) {
      let currentDate = new Date()
      input.filter.push("to_char(A.start_periode, 'YYYY-MM-DD') BETWEEN ? AND ?")
      input.filterValue.push(`${currentDate.getFullYear()}-01-01`)
      input.filterValue.push(`${currentDate.getFullYear()}-12-31`)
      // INIT VALUE
      input.orderBy = input.orderBy ? input.orderBy : "TO_CHAR(A.start_periode, 'YYYY-MM')"
      input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""

      let sql = `SELECT
                 TO_CHAR(A.start_periode, 'YYYY-MM') AS month,
                 ARRAY_AGG(DISTINCT A.production_unit_id) AS production_unit_id,
                 SUM(A.quantity) AS total_realization_per_month,
                 AVG(A.quantity) AS average_realization_per_month
                 FROM invoice A
                 ${input.filter}
                 GROUP BY TO_CHAR(A.start_periode, 'YYYY-MM')
                 ORDER BY ${input.orderBy} ${input.sort}
                 `
      //  ORDER BY TO_CHAR(start_periode, 'YYYY-MM')
      //  LIMIT ${input.limit}
      //  OFFSET ${input.offset}
      input.realization = await db.run_select(sql, input.filterValue)
    } else {
      // INIT VALUE
      input.orderBy = input.orderBy ? input.orderBy : "TO_CHAR(A.start_periode, 'YYYY-MM')"
      input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : ""

      let sql = `SELECT
               A.id,
               A.quantity,
               A.corporate_id,
               A.production_unit_id,
               A.status_code,
               TO_CHAR(A.start_periode, 'YYYY-MM-DD') AS start_periode
               FROM invoice A
               ${input.filter}
               ORDER BY ${input.orderBy} ${input.sort}
               `
      input.realization = await db.run_select(sql, input.filterValue)
    }

    return input
  },

  process: async function (input, OriginalInput, db) {
    const { realization, date } = input

    let data = {
      total_plant: 0,
      total_volume: 0,
      average_volume: 0,
      trend_chart: [],
    }

    if (realization.length == 0) {
      data.total_plant = 0
      data.total_volume = 0
      data.average_volume = 0
      data.trend_chart = []
      return data
    }

    if (!date) {
      let totalPlant = {}
      for (const item of realization) {
        // {
        //   month: '2023-06',
        //   production_unit_id: [ '16', '18' ],
        //   total_realization_per_month: 5398,
        //   average_realization_per_month: 1349.5
        // },
        item.production_unit_id.forEach((element) => {
          let unitID = element

          if (Object.hasOwn(totalPlant, unitID)) {
            totalPlant[unitID]++
          } else {
            totalPlant[unitID] = 1
          }
        })

        // TOTAL REALIZATION
        data.total_volume += item.total_realization_per_month

        data.trend_chart.push({
          month: item.month,
          total_plant_per_month: item.production_unit_id.length,
          total_volume_per_month: item.total_realization_per_month,
          average_volume_per_month: item.average_realization_per_month,
        })
      }

      // TOTAL PLANT
      data.total_plant += Object.keys(totalPlant).length

      // AVERAGE REALIZATION
      data.average_volume = parseFloat((data.total_volume / realization.length).toFixed(2))
    } else {
      const uniqueDates = [...new Set(realization.map((item) => item.start_periode))]

      uniqueDates.forEach((date) => {
        const itemsOnDate = realization.filter((item) => item.start_periode === date)

        const totalPlantThisDay = new Set(itemsOnDate.map((item) => item.production_unit_id)).size

        const totalVolumeThisDay = itemsOnDate.reduce((sum, item) => sum + item.quantity, 0)

        const averageVolumeThisDay = totalVolumeThisDay / itemsOnDate.length

        data.trend_chart.push({
          date: date,
          total_plant_daily: totalPlantThisDay,
          total_volume_daily: totalVolumeThisDay,
          average_volume_daily: averageVolumeThisDay,
        })
      })

      // GET TOTAL PLANT, TOTAL VOLUME, AND AVERAGE

      let totalPlant = {}
      let realizationByDay = {}
      for (const item of realization) {
        /*
      {
          "id": 90,
          "quantity": 1349.5,
          "corporate_id": 14,
          "production_unit_id": 16,
          "status_code": "PENDING",
          "start_periode": "2023-06-01"
      },
      {
          "id": 91,
          "quantity": 1349.5,
          "corporate_id": 14,
          "production_unit_id": 16,
          "status_code": "PENDING",
          "start_periode": "2023-06-01"
      },
      */

        let unitID = item.production_unit_id

        if (Object.hasOwn(totalPlant, unitID)) {
          totalPlant[unitID]++
        } else {
          totalPlant[unitID] = 1
        }

        // TOTAL VOLUME IN 1 MONTH
        data.total_volume += item.quantity
      }
      // TOTAL PLANT IN 1 MONTH
      data.total_plant = Object.keys(totalPlant).length

      // AVERAGE VOLUME IN 1 MONTH
      data.average_volume = parseFloat((data.total_volume / realization.length).toFixed(2))
    }

    return data
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    sort: "string",
    corporate_id: "integer",
    production_unit_id: "integer",
    date: "array",
  },
}

module.exports = CoreService(service)
