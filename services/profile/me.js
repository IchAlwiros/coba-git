const { CoreService, CoreException } = require('../../core/CallService')


/**
 * Service My Profile
 */

const service = {
    input: function (request) {
        return request.query
    },

    prepare: async function (input, db) {
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.username,A.name, A.phone,A.email,A.role_id,
                    B.code AS role_code,C.name AS role_name,A.religion_id, C.name AS religion_name,
                    A.gender, A.address, 
                    A.photo,
                    CASE WHEN A.active = '1' THEN TRUE ELSE FALSE END AS active,
                    to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at,
                    to_char(A.updated_at,'YYYY-MM-DD hh24:MI:ss') AS last_update
                    FROM users A 
                    INNER JOIN roles B ON B.id = A.role_id
                    LEFT JOIN religion C ON C.id = A.religion_id
                    WHERE A.id = ? `
        let profile = await db.row(sql, [input.session.user_id])

        if (profile) {
            profile.photo_preview = profile.photo ? `${process.env.config.BASE_URL}/public/users/${profile.photo}` : ''
            profile.photo_thumbnail = profile.photo ? `${process.env.config.BASE_URL}/public/users/thumbnail_${profile.photo}` : ''
        }
   
        return profile
    },
    validation: {

    }
}


module.exports = CoreService(service)