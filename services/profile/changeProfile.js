const { CoreService, CoreException } = require('../../core/CallService')
const UserProfile = require("./me")
const { Global } = require('../../util/globalFunction')

/**
 * Service Change Profile
 */

const service = {
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        let checkAuth = await db.row(`SELECT*FROM users WHERE id != ? AND (username = ? OR email = ? OR phone = ?)`, [input.session.user_id, input.username, input.email, input.phone])
        if (checkAuth) {
            if (checkAuth.email == input.email) throw new CoreException(`Email has been registered|email telah terdaftar`)
            if (checkAuth.phone == input.phone) throw new CoreException(`phone number has been registered|nomor telepon telah terdaftar`)
            if (checkAuth.username == input.username) throw new CoreException(`username has been registered|username telah terdaftar`)
            throw new CoreException(`Change auth failed|ubah autentikasi gagal`)
        }
        let usersCheck = await db.row(`SELECT*FROM users where id = ? `, [input.session.user_id])
        input.last_photo = usersCheck.photo
        return input
    },

    process: async function (input, OriginalInput, db) {
        if (input.photo && input.photo != input.last_photo) {
            if (!Global.fileExist(`tmp/${input.photo}`)) throw new CoreException("File not found|file tidak ditemukan")
        }
        let dataUpdate = {
            email: input.email,
            phone: input.phone,
            name: input.name,
            username: input.username,
            gender: input.gender,
            religion_id: input.religion_id,
            address: input.address,
            photo: input.photo || null,
            updated_at: Global.currentDateTime(),
            updated_by: input.session.user_id
        }
        await db.run_update(`users`, dataUpdate, { id: input.session.user_id })

        if (input.photo && input.photo != input.last_photo) {
            let dir = 'public/users'
            await Global.moveFile(`tmp/${input.photo}`, dir, input.photo)
            await Global.imageSharp(`${dir}/${input.photo}`, `${dir}/thumbnail_${input.photo}`)
        }
        let me = UserProfile.call(input, db)
        return me
    },
    validation: {
        name: 'required',
        email: 'required|email',
        username: 'required',
        phone: 'required|phone',
        gender: 'string|maxLength:1|accepted:L,P,O',
        religion_id: 'integer',
    }
}


module.exports = CoreService(service)