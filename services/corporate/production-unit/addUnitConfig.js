const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")

/**
 * CREATE PRODUCTION UNTIT CONFIG
 */

const service = {
  input: function (request) {
    let input = request.body
    return input
  },

  prepare: async function (input, db) {
    // SET SESSION
    input.currentUserId = input.session.user_id

    let idValue = await db.row(`SELECT B.id AS production_unit_id, B.corporate_id, C.id AS production_unit_config_id
                                    FROM production_unit B
                                    LEFT JOIN production_unit_config C
                                    ON B.id = C.production_unit_id
                                    WHERE B.id =${input.production_unit_id}`)
    if (!idValue) throw new CoreException("Production Unit not found | Unit Produksi tidak ditemukan")
    if (idValue.corporate_id != input.corporate_id) throw new CoreException("Corporate and Production Unit do not match | Perusahaan dan Unit Produksi tidak sesuai")

    // CHECK IS_EXIST production_unit_config ?
    if (idValue.production_unit_config_id) throw new CoreException("Production Unit Config already exist | Config Unit Produksi sudah ada")

    // If active and tax is null, SET with DEFAULT
    input.active = input.active == null ? 1 : input.active
    input.tax = input.active == null ? 1 : input.tax

    // SET billing_date based active
    if (input.active == 0) input.billing_date = null

    // SET created_at, updated_at,
    input.created_at = Global.currentDateTime()
    input.updated_at = Global.currentDateTime()
    // AND created_by
    input.created_by = input.currentUserId

    return input
  },

  process: async function (input, OriginalInput, db) {
    const insertValue = {
      corporate_id: input.corporate_id,
      production_unit_id: input.production_unit_id,
      unit_price: input.unit_price,
      installation_fee: input.installation_fee,
      tax: input.tax,
      billing_date: input.billing_date,
      tolerance: input.tolerance,
      active: input.active,
      created_at: input.created_at,
      updated_at: input.updated_at,
      created_by: input.created_by,
      updated_by: input.updated_by, // DEFAULT null
    }

    // Now, its time to insert data to table production_unit_config
    let data = await db.run_insert("production_unit_config", insertValue)

    return data
  },
  validation: {
    corporate_id: "integer|required",
    production_unit_id: "integer|required",
    unit_price: "decimal",
    installation_fee: "decimal",
    tax: "boolean",
    billing_date: "date",
    tolerance: "integer",
    active: "boolean",
  },
}

module.exports = CoreService(service)
