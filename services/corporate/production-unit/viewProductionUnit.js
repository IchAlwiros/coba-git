const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * VIEW PRODUCTION UNIT
 */

const service = {
  input: function (request) {
    return request.params;
  },
  prepare: async function (input, db) {
    return input;
  },
  process: async function (input, OriginalInput, db) {
    let columnName = `B.id, B.uuid, B.corporate_id, A.name AS corporate_name, B.code, B.name, B.phone, B.email, B.address, B.registered_at, B.active, B.subscription_code`;
    let sql = `SELECT ${columnName} FROM production_unit B
                    INNER JOIN corporate A
                    ON B.corporate_id = A.id
                    WHERE B.id = ?`;
    let data = await db.row(sql, [input.id]);
    if (!data) throw new CoreException("Production Unit not found|Unit Produksi tidak ditemukan");

    return data;
  },
  validation: {
    id: "integer",
  },
};

module.exports = CoreService(service);
