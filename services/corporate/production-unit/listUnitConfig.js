const { CoreService, CoreException } = require("../../../core/CallService");
const filterBuilder = require("../../../util/filterBuilder");

/**
 * PRODUCTION UNIT
 */

const service = {
  input: function (request) {
    let input = request.query
    return input;
  },

  prepare: async function (input, db) {
    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    input.searchField = [];
    input.searchValue = [];

    // CONFIG
    let filter = [
      {
        field: "corporate_id",
        alias: "C",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "production_unit_id",
        alias: "C",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "unit_price",
        alias: "C",
        search: false,
        order: true,
        filter: true,
      },
      {
        field: "tax",
        alias: "C",
        search: false,
        order: false,
        filter: true,
      },
      {
        field: "billing_date",
        alias: "C",
        search: false,
        order: true,
        filter: false,
      },
      {
        field: "active",
        alias: "C",
        search: true,
        order: true,
        filter: true,
      },
    ];
    input = filterBuilder(input, filter);

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "C.id";
    input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let columnName = `C.id, C.corporate_id, A.name AS corporate_name, C.production_unit_id, B.name AS production_unit_name, C.unit_price, C.installation_fee, C.tax, C.billing_date, C.tolerance, C.active`;
    let sql = `SELECT ${columnName} FROM production_unit_config C 
                    INNER JOIN corporate A ON C.corporate_id = A.id 
                    INNER JOIN production_unit B ON C.production_unit_id = B.id ${input.filter} 
                    ORDER BY ${input.orderBy} LIMIT ${input.limit} OFFSET ${input.offset}`;

    // run_select untuk return array
    let data = await db.run_select(sql, input.filterValue);

    // COUNT
    var sql2 = `SELECT COUNT(C.*) AS record
                FROM production_unit_config C ${input.filter}`;
    // row untuk return object
    let record = await db.row(sql2, input.filterValue);

    return {
      data: data,
      record: record ? record.record : 0,
    };
  },
  validation: {
    limit: "integer|min:0",
    offset: "integer|min:0",
    orderBy: "string",
  },
};

module.exports = CoreService(service);
