const { CoreService, CoreException } = require("../../../core/CallService")
const filterBuilder = require("../../../util/filterBuilder")
const { Global } = require("../../../util/globalFunction")

/**
 * UPDATE PRODUCTION UNTIT CONFIG
 */

const service = {
  input: function (request) {
    let input = request.body
    return input
  },

  prepare: async function (input, db) {
    // SET SESSION
    input.currentUserId = input.session.user_id

    // Check production_unit_config by ID
    let sql = `SELECT * FROM production_unit_config C
               WHERE C.id = ?`
    let isExist = await db.row(sql, input.id)

    if (!isExist) throw new CoreException("Production Unit Config not found | Config Unit Produksi tidak ditemukan")
    // if(isExist.production_unit_id != isExist.id_production_unit && isExist.corporate_id != isExist.id_corporate) throw new CoreException("Corporate and Production Unit do not match | Perusahaan dan Unit Produksi tidak sesuai");

    input.updated_at = Global.currentDateTime()
    input.updated_by = input.currentUserId
    return input
  },

  process: async function (input, OriginalInput, db) {
    const updateValue = {
      unit_price: input.unit_price,
      installation_fee: input.installation_fee,
      tax: input.tax,
      billing_date: input.billing_date,
      tolerance: input.tolerance,
      active: input.active,
      updated_by : input.updated_by
    }

    // Now, its time to update data in table production_unit_config
    let data = await db.run_update("production_unit_config", updateValue, { id: input.id })

    return data
  },
  validation: {
    // corporate_id: "integer|required",
    // production_unit_id: "integer|required",
    unit_price: "decimal",
    installation_fee: "decimal",
    tax: "boolean",
    billing_date: "date",
    tolerance: "integer",
    active: "boolean",
  },
}

module.exports = CoreService(service)
