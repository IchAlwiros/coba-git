const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");

/**
 * Service LIST CORPORATE
 */

const service = {
  input: function (request) {
    let input = request.query;
    return input;
  },

  prepare: async function (input, db) {
    // SET SESSION
    input.currentUserId = input.session.user_id;

    // SET FILTER
    input.filter = [];
    input.filterValue = [];

    input.searchField = [];
    input.searchValue = [];

    // CONFIG
    let filter = [
      {
        field: "name",
        alias: "A",
        search: true,
        order: true,
        filter: false,
      },
      {
        field: "active",
        alias: "A",
        search: true,
        order: true,
        filter: true,
      },
      {
        field: "registered_at",
        alias: "A",
        search: false,
        order: true,
        filter: false,
      },
    ];

    input = filterBuilder(input, filter);

    // INIT VALUE
    input.orderBy = input.orderBy ? input.orderBy : "A.id";
    input.filter =
      input.filter.length > 0 ? ` WHERE ${input.filter.join(" AND ")}` : "";

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT A.id, A.uuid, A.code, A.name, A.phone, A.email, A.address, A.registered_at, A.active 
               FROM corporate A ${input.filter} 
               ORDER BY ${input.orderBy} ${input.sort} 
               LIMIT ${input.limit} 
               OFFSET ${input.offset}`;

    let data = await db.run_select(sql, input.filterValue);

    // COUNT
    var sql2 = `SELECT COUNT(A.*) AS record
                FROM corporate A ${input.filter}
                `;
    let record = await db.row(sql2, input.filterValue);

    return {
      data: data,
      record: record ? record.record : 0,
    };
  },
  validation: {
    date: "array",
    limit: "integer|min:0",
    offset: "integer|min:0",
    orderBy: "string",
    sort: "string|accepted:ASC,DESC",
  },
};

module.exports = CoreService(service);
