const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");

/**
 * VIEW CORPORATE
 */

const service = {
  input: function (request) {
    return request.params;
  },

  prepare: async function (input, db) {
    return input;
  },

  process: async function (input, OriginalInput, db) {
    let sql = `SELECT A.id, A.uuid, A.code, A.name, A.phone, A.email, A.address, A.registered_at, A.active 
               FROM corporate A
               WHERE A.id = ?`;
    let data = await db.row(sql, [input.id]);
    if (!data) throw new CoreException("Corporate not found|Perusahaan tidak ditemukan");

    return data
  },
  validation: {
    id: "integer",
  },
};

module.exports = CoreService(service);
