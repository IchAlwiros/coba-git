const { CoreService, CoreException } = require("../../core/CallService");
const bcrypt = require("bcryptjs");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const { Extend } = require("../../util/extendFunction");

/**
 * LOGIN
 */

const service = {
  input: function (request) {
    return request.body;
  },

  prepare: async function (input, db) {
    let { email, username, password } = input;
    let { getUser } = Extend;

    const users = await getUser(undefined, email);

    if (!users) {
      throw new CoreException(`user not found | pengguna tidak ditemukan`);
    }
    const valid = bcrypt.compareSync(password, users.password);
    if (!valid) {
      throw new CoreException(`invalid credential | email atau password salah`);
    }

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { email, username } = input;
    let { getUser, generateRandomToken } = Extend;
    // FIND USER BY EMAIL OR USERNAME
    const users = await getUser(undefined, email);

    // Generate JWT TOKEN
    let tokenGenerate = generateRandomToken();
    const token = jwt.sign(
      {
        user_id: users.id,
        username: users.username,
        api_token: tokenGenerate,
      },
      process.env.APP_KEY
    );

    if (users.role_id == 3 && users.roles == "SUPER-ADMIN") {
      if (token) {
        // INSERT TOKEN
        var sql = `INSERT INTO api_token (user_id, api_token) VALUES (?, ?)`;
        const insertToken = await db
          .raw(sql, [parseInt(users.id), tokenGenerate])
          .then((result) => {
            // Menampilkan hasil query
            return {
              message: "success insert token | berhasil insert token",
            };
          })
          .catch((error) => {
            // Menampilkan pesan error jika terjadi kesalahan
            throw new CoreException("Failed insert | gagal insert");
          });
        // VALIDASI INPUT TOKEN SUCCESS
        if (insertToken) {
          const dataLogin = {
            profile: {
              username: users.username,
              email: users.email,
              photo: users.photo,
              phone: users.phone,
              address: users.address,
              role: users.roles,
              role_code: users.code,
              role_id: users.id,
              role_name: users.roles,
            },
            token: token,
            permission: users.permission,
          };
          return dataLogin;
        }
      } else throw new CoreException("Failed login | gagal login");
    } else {
      // VALIDATION TOKEN FOUND
      if (token) {
        // INSERT TO TABLE api_token
        var sql = `SELECT a.api_token, a.user_id FROM api_token a where user_id = ?`;
        const exitsToken = await db
          .raw(sql, [parseInt(users.id)])
          .then((result) => {
            return result.rows[0];
          })
          .catch((err) => {
            // console.log(err.message);
            throw new CoreException("token not found | token tidak ditemukan");
          });
        // VALIDATION api_tokens OWNED BY USER FOR UPDATE OR INSERT
        if (exitsToken) {
          // UPDATE TOKEN
          var sql = `UPDATE api_token
                     SET api_token = ?
                     WHERE user_id = ?;`;
          const updateToken = await db
            .raw(sql, [tokenGenerate, parseInt(exitsToken.user_id)])
            .then((result) => {
              // Menampilkan hasil query
              return {
                message: "success update token | berhasil update token",
              };
            })
            .catch((error) => {
              // Menampilkan pesan error jika terjadi kesalahan
              throw new CoreException("Failed update | gagal update");
            });
          // VALIDASI UPDATE TOKEN SUCCESS
          if (updateToken) {
            const dataLogin = {
              profile: {
                username: users.username,
                email: users.email,
                photo: users.photo,
                phone: users.phone,
                address: users.address,
                role: users.roles,
                role_code: users.code,
                role_id: users.id,
                role_name: users.roles,
              },
              token: token,
              permission: users.permission,
            };
            return dataLogin;
          }
        } else {
          // INSERT TOKEN
          var sql = `INSERT INTO api_token (user_id, api_token) VALUES (?, ?)`;
          const insertToken = await db
            .raw(sql, [parseInt(users.id), tokenGenerate])
            .then((result) => {
              // Menampilkan hasil query
              return {
                message: "success insert token | berhasil insert token",
              };
            })
            .catch((error) => {
              // Menampilkan pesan error jika terjadi kesalahan
              throw new CoreException("Failed insert | gagal insert");
            });
          // VALIDASI INPUT TOKEN SUCCESS
          if (insertToken) {
            const dataLogin = {
              profile: {
                username: users.username,
                email: users.email,
                photo: users.photo,
                phone: users.phone,
                address: users.address,
                role: users.roles,
                role_code: users.code,
                role_id: users.id,
                role_name: users.roles,
              },
              token: token,
              permission: users.permission,
            };
            return dataLogin;
          }
        }
      } else {
        throw new CoreException("Failed login | gagal login");
      }
    }
  },

  validation: {
    password: "required",
    email: "required",
  },
};

module.exports = CoreService(service);
