const { CoreService, CoreException } = require('../../core/CallService')
const bcrypt = require('bcryptjs')
var sha1 = require('sha1')
var jwt = require('jsonwebtoken')
const { Global } = require('../../util/globalFunction')

const service = {
    transaction: true,
    input: function (request) {
        return request.body
    },

    prepare: async function (input, db) {
        input.currentDateTime = Global.currentDateTime()
        return input
    },
    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, CASE WHEN A.name IS NULL THEN A.name ELSE A.name END AS name,A.phone,A.email, 
                    A.role_id,B.code AS role_code, B.name AS role_name,
                    CASE WHEN A.photo !='' AND A.photo IS NOT NULL THEN CONCAT('${process.env.config.BASE_URL}/public/users/thumbnail_',A.photo) ELSE '' END AS photo_profile,
                    A.password, CASE WHEN A.active ='1' THEN TRUE ELSE FALSE END AS active
                    FROM users A
                    INNER JOIN roles B ON B.id = A.role_id
                    WHERE  A.username = ? OR A.email = ?`
        let profile = await db.row(sql, [input.username, input.username])
        if (!profile) throw new CoreException("Incorect username or password | username atau password salah")
        if (!bcrypt.compareSync(input.password, profile.password)) throw new CoreException("Incorect username or password | username atau password salah")
        if (!profile.active) throw new CoreException(`User inactive|pengguna tidak aktif`, {}, 421)

        // GENERATE API TOKEN 
        var api_token = sha1(`${profile.id}-${Math.random(100000, 999999)}`)
        var token = jwt.sign({
            uuid: profile.uuid,
            user_id: profile.id,
            api_token: api_token
        }, process.env.config.APP_KEY)

        let apiTokenData = {
            user_id: profile.id,
            api_token: api_token,
            last_active: input.currentDateTime,
            created_at: input.currentDateTime
        }
        let checkToken = null

        // 1 USER HANYA BISA LOGIN DI 1 APLIKASI
        if (profile.role_id !== -1) checkToken = await db.run_update(`api_token`, apiTokenData, { user_id: profile.id })
        if (!checkToken) {
            await db('api_token').insert(apiTokenData)
                .catch((err) => {
                    console.log(`Error Insert Api token `, err)
                    throw new CoreException("Login failed, try again later | Gagal masuk, silahkan coba beberapa saat lagi")
                })
        }
        let permissions = []

        var sql2 = `SELECT CONCAT(D.action,'-',E.code) AS task FROM roles A
                    INNER JOIN role_details B ON B.role_id = A.id
                    INNER JOIN modules C ON C.id = B.module_id
                    INNER JOIN module_permissions D ON D.module_id = C.id
                    INNER JOIN tasks E ON E.id = D.task_id
                    WHERE A.id = ? AND B.allowed ='1' AND B.active ='1'`
        let access = await db.run_select(sql2, [profile.role_id])

        for (let item of access) {
            permissions.push(item.task)
        }

        delete profile.password
        return {
            message: "Login successfully",
            profile: profile,
            token: token,
            permissions: permissions,
        }
    },
    validation: {
        username: 'required',
        password: 'required'
    }
}


module.exports = CoreService(service)