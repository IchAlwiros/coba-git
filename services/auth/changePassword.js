const { CoreService, CoreException } = require("../../core/CallService");
const bcrypt = require("bcryptjs");
require("dotenv").config();
const { Extend } = require("../../util/extendFunction");

/**
 * CHANGE PASSWORD USERS
 */

const service = {
  input: function (request) {
    let input = request.body;
    input.session = request.session;
    return input;
  },

  prepare: async function (input, db) {
    let { user_id, email } = input.session;
    let { oldpassword, newpassword, confirmpassword } = input;
    let validation = {};
    const user = await Extend.getUser(user_id, email);

    const cekCurrentPassword = bcrypt.compareSync(oldpassword, user.password);
    if (!cekCurrentPassword) {
      validation = {
        ...validation,
        oldpassword: `old password is wrong | kata sandi lama salah `,
      };
    }
    // Validasei newpassowrd match
    if (newpassword != confirmpassword) {
      validation = {
        ...validation,
        newpassword: `new passwords doesn't match | sandi baru tidak sama`,
      };
    }
    if (Object.keys(validation).length > 0) {
      throw new CoreException("BAD REQUEST", validation, 400);
    }
  },

  process: async function (input, OriginalInput, db) {
    let { user_id, email } = input.session;
    let { confirmpassword } = input;
    const user = await Extend.getUser(user_id, email);
    if (user) {
      const hashedPassword = await bcrypt.hash(confirmpassword, 10);
      // CHANGE PASSWORD
      var sql = `UPDATE "users"
        SET password = ?
        WHERE email = ?;`;
      const updatePassword = await db
        .raw(sql, [hashedPassword, email])
        .then((result) => {
          // Menampilkan hasil query
          return result.rowCount;
        })
        .catch((error) => {
          return error.message; // Menampilkan pesan error jika terjadi kesalahan
        });
      // VALIDASI SUCCESS UPDATE PASSWORD
      if (updatePassword) {
        return {
          message: "Your password has been changed successfully",
          email,
        };
      } else
        throw new CoreException(
          "Change Password Failed | mengubah sandi salah"
        );
    }
  },

  validation: {
    oldpassword: "required",
    newpassword: "required|minLength:8",
    confirmpassword: "required",
  },
};

module.exports = CoreService(service);
