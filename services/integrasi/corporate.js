const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");
const { Global } = require("../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const e = require("express");

/**
 * Corporate Integration
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.body = request.body;
    return input;
  },

  prepare: async function (input, db) {},

  process: async function (input, OriginalInput, db) {
    const insertCorporate = async (item) => {
      item.forEach(async (el) => {
        let data = {
          uuid: el.id,
          name: el.name,
          phone: el.phone,
          address: el.address,
          email: el.email,
          registered_at: el.registered_at,
          updated_at: el.last_update,
        };

        await db("corporate")
          .insert(data)
          .then((result) => {
            return "Data inserted successfully";
          })
          .catch((error) => {
            console.error("Error inserting data:", error);
          });

        // await db.raw("select setval('id', max(id)) from corporate");
      });
    };

    let baseUrl = "https://api.teknologireadymix.com/rbs/dev/";
    let token = "14093f30c292a7686b34ced731cf7295";
    await axios
      .get(`${baseUrl}/support/data/corporate`, {
        headers: { Authorization: token },
      })
      .then((e) => {
        let data = e.data.data.data;
        return insertCorporate(data);
      })
      .catch((err) => {
        throw new CoreException("failed pull data | gagal pull data");
      });
  },

  validation: {},
};

module.exports = CoreService(service);
