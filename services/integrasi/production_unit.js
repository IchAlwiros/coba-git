const { default: axios } = require("axios");
const { CoreService, CoreException } = require("../../core/CallService");
const filterBuilder = require("../../util/filterBuilder");
const { Global } = require("../../util/globalFunction");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const e = require("express");

/**
 * Production Unit Integration
 */

const service = {
  input: function (request) {
    let input = request.query;
    input.body = request.body;
    return input;
  },

  prepare: async function (input, db) {
    let { corporate_id } = input;

    return input;
  },

  process: async function (input, OriginalInput, db) {
    let { corporate_id } = input;

    const insertProductUnitConfig = async (item) => {
      let data = {
        corporate_id: item.corporate_id,
        production_unit_id: item.id,
        billing_date: item.registered_at,
      };

      await db
        .run_insert("production_unit_config", data)
        .then((resuslt) => {
          console.log(resuslt);
        })
        .catch((err) => {
          throw new CoreException(
            "invoice key number already exists | nomor invoice tersedia"
          );
        });
    };

    const insertProductionUnit = async (item_production, id) => {
      item_production.forEach(async (el) => {
        let data = {
          uuid: el.id,
          corporate_id: id,
          name: el.name,
          phone: el.pic_phone,
          address: el.address,
          registered_at: el.registered_at,
          updated_at: el.last_update,
        };
        // insertProductUnitConfig(id);

        await db
          .run_insert("production_unit", data)
          .then((resuslt) => {
            console.log(resuslt);
            // INSERT INVOCE PAYMENT
            insertProductUnitConfig(resuslt);
          })
          .catch((err) => {
            // console.error("Error inserting data:", err);
            throw new CoreException(
              "error inserting data | gagal memasukan data"
            );
          });
        // console.log(data);

        // await db.raw("select setval('id', max(id)) from corporate");
      });
    };

    let baseUrl = "https://api.teknologireadymix.com/rbs/dev/";
    let token = "14093f30c292a7686b34ced731cf7295";

    await axios
      .get(`${baseUrl}/support/data/production-unit/${corporate_id}`, {
        headers: { Authorization: token },
      })
      .then(async (e) => {
        let production_unit = e.data.data.production_unit;
        let corporate = e.data.data.corporate;
        let sql = `select c.id ,c.code,c."name" from corporate c where c.uuid = ?`;
        let selectIdCorporate = await db.row(sql, [corporate.id]);

        // console.log(production_unit);
        // console.log(selectIdCorporate.id);
        return insertProductionUnit(production_unit, selectIdCorporate.id);
      })
      .catch((err) => {
        throw new CoreException("failed pull data | gagal pull data");
      });
  },

  validation: {},
};

module.exports = CoreService(service);
