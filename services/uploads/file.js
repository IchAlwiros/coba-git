const { CoreService, CoreException } = require('../../core/CallService')
const fs = require('fs')
const moment = require('moment')

const service = {
    transaction: true,
    task: null,
    input: function (request) {
        let header = request.headers
        let input = {
            file: request.file,
            header: header
        }
        return input
    },

    prepare: async function (input, db) {
        let limitUpload = 5 * 1024 * 1024
        if (parseInt(input.file.size) > limitUpload) {
            throw new CoreException(`File to large. Max file size 5 MB | Dokumen yang diunggah terlalu besar. Pastikan ukuran maksimal dokumen 5 MB`)
        }
        return input
    },

    process: async function (input, OriginalInput, db) {
        input.ext = input.file.originalname.split(".")
        const extensi = input.ext[input.ext.length - 1]

        input.fileName = moment().utcOffset(7).format('YYYMMMDDDHHmmss') + '.' + extensi
        let dir = "tmp/"
        if (!fs.existsSync(dir)) fs.mkdirSync(dir)
        fs.writeFileSync(`${dir}${input.fileName}`, input.file.buffer)
        let temporaryFile = `${dir}${input.fileName}`

        if (!fs.existsSync(temporaryFile)) {
            throw new CoreException(`Upload failed | Dokumen gagal diunggah`)
        }
        return {
            file: input.fileName,
            preview: `${process.env.config.BASE_URL}/${temporaryFile}`
        }
    },

    validation: {
        file: "required|mime:jpg,jpeg,png,pdf,doc,xls,xlsx,bmp,svg"
    }
}


module.exports = CoreService(service)