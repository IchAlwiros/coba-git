const { CoreService, CoreException } = require('../../../../core/CallService')
const filterBuilder = require('../../../../util/filterBuilder')

/**
 * Service * SERVICE NAME*
 */

const service = {
    input: function (request) {
        let input = request.query
        return input
    },

    prepare: async function (input, db) {
        input.currentUserId = input.session.user_id
        input.currentCorporateId = input.session.corporate_id

        input.filter = [`B.corporate_id = ?`]
        input.filterValue = [input.currentCorporateId]

        input.searchField = []
        input.searchValue = []

        // CONFIG
        let filter = [
            { field: 'production_unit_id', alias: 'B', search: false, order: false, filter: true },
            { field: 'customer_id', alias: 'A', search: false, order: false, filter: true },
            { field: 'active', alias: 'A', search: true, order: true, filter: false },
            { field: 'created_at', alias: 'A', search: false, order: true, filter: false },
        ]
        input = filterBuilder(input, filter)

        // INIT VALUE
        input.orderBy = input.orderBy ? input.orderBy : 'A.id'
        input.filter = input.filter.length > 0 ? ` WHERE ${input.filter.join(' AND ')}` : ''
        return input
    },

    process: async function (input, OriginalInput, db) {
        var sql = ` SELECT A.id, A.customer_id, A.document_category_id,C.name AS document_category_name, B.name AS customer_name, A.attachment,
                    CASE WHEN A.attachment IS NOT NULL AND A.attachment !='' THEN CONCAT('${process.env.config.BASE_URL}/public/customer-document/',A.attachment) ELSE NULL END AS attachment_preview,
                    A.description, to_char(A.created_at,'YYYY-MM-DD hh24:MI:ss') AS created_at
                    FROM customer_document A
                    INNER JOIN customers B ON B.id = A.customer_id
                    LEFT JOIN document_category C ON C.id = A.document_category_id  
                    ${input.filter} ORDER BY ${input.orderBy} ${input.sort}  LIMIT ${input.limit} OFFSET ${input.offset}`
        let data = await db.run_select(sql, input.filterValue)

        // COUNT
        var sql2 = `SELECT COUNT(A.*) AS record 
                    FROM customer_document A
                    INNER JOIN customers B ON B.id = A.customer_id
                    LEFT JOIN document_category C ON C.id = A.document_category_id ${input.filter} `
        let record = await db.row(sql2, input.filterValue)

        return {
            data: data,
            record: record ? record.record : 0
        }
    },
    validation: {
        limit: "integer|min:0",
        offset: "integer|min:0",
        production_unit_id: "integer",
        document_category_id: "integer",
        customer_id: "integer",
    }
}

module.exports = CoreService(service)