const moment = require("moment");
const fs = require("fs");
const sharp = require("sharp");
const { database, CoreException } = require("../core/CallService");
const axios = require("axios");
const { v4: uuidv4 } = require("uuid");

const Global = {
  setDate: function (date) {
    return moment(date).format("YYYY-MM-DD HH:mm:ss");
  },
  currentDateTime: function (utcOffset = 7) {
    moment().locale("id");
    return moment().utcOffset(utcOffset).format("YYYY-MM-DD HH:mm:ss");
  },
  currentDate: function (utcOffset = 7) {
    moment().locale("id");
    return moment().utcOffset(utcOffset).format("YYYY-MM-DD");
  },
  currentTime: function (utcOffset = 7) {
    moment().locale("id");
    return moment().utcOffset(utcOffset).format("HH:mm:ss");
  },
  currentFirstMonth: function (utcOffset = 7) {
    moment().locale("id");
    return moment().utcOffset(utcOffset).format("YYYY-MM-01");
  },
  currentMonth: function (utcOffset = 7) {
    moment().locale("id");
    return moment().utcOffset(utcOffset).format("YYYY-MM");
  },
  parseDesimal: function (data) {
    let value = parseFloat(data);
    if (isNaN(value) || value == null || data == undefined) return 0;
    else return parseFloat(parseFloat(data).toFixed(2));
  },
  makeOtp: function (length = 5) {
    var result = "";
    var characters = "01234567890";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },
  makeUniq: function (length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },
  makeExpired: function (val) {
    moment().locale("id");
    return moment()
      .utcOffset(7)
      .add(val, "minute")
      .format("YYYY-MM-DD HH:mm:ss");
  },
  addMonth: function (val) {
    moment().locale("id");
    return moment().utcOffset(7).add(val, "M").format("YYYY-MM-DD HH:mm:ss");
  },
  separator(value) {
    if (!value) return 0;
    var val = value.toString().replace(/\,/g, ".");
    var newVal = parseFloat(val).toFixed(0);
    newVal = newVal.toString().replace(/\,/g, ".");
    newVal = newVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return newVal;
  },
  fileExist: function (path) {
    if (!fs.existsSync(path)) return false;
    else return true;
  },
  imageSharp: async function (path, dest) {
    await sharp(path)
      .resize(250, 250)
      .jpeg({ quality: 100 })
      .rotate(0)
      .toFile(dest);
  },
  moveFile: function (curPath, destPath, fileName) {
    if (fs.existsSync(curPath)) {
      if (!fs.existsSync(destPath)) fs.mkdirSync(destPath);
      return fs.renameSync(curPath, `${destPath}/${fileName}`);
    } else return false;
  },
  deleteFile: function (filePath) {
    if (fs.existsSync(filePath)) return fs.unlinkSync(filePath);
    else return false;
  },

  getNewIdsAndDeletedIds: function (currentIds, newIds) {
    let generate_deleted_ids = [];
    let generate_new_ids = [];

    // console.log(newIds);
    newIds.forEach((newId) => {
      if (currentIds.indexOf(newId) === -1) {
        generate_new_ids = [...generate_new_ids, newId];
      }
    });

    currentIds.forEach((currentId) => {
      if (newIds.indexOf(currentId) === -1) {
        generate_deleted_ids = [...generate_deleted_ids, currentId];
      }
    });

    return [generate_deleted_ids, generate_new_ids];
  },
};

module.exports = { Global };
