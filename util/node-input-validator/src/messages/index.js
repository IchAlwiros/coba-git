
/**
 * node-input-validator
 * https://github.com/artisangang/node-input-validator
 */

const messages = require('./en/messages');
const messagesId = require('./id/messages');

module.exports = { en: messages, id: messagesId };

module.exports.defaultLang = 'en';
