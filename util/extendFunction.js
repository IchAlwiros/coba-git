const moment = require("moment");
const fs = require("fs");
const sharp = require("sharp");
const { database, CoreException } = require("../core/CallService");
const axios = require("axios");
const { v4: uuidv4 } = require("uuid");

const Extend = {
  generatePeriod: (isoDate = "2023-06-29T17:00:00.000Z") => {
    const momentDate = moment(isoDate).format("YYYY-MM-DD");
    const year = momentDate.slice(0, 4); // Tahun periode
    const month = momentDate.slice(5, 7); // Bulan periode (0 untuk Januari, 1 untuk Februari, dst.)

    // Menghitung tanggal awal periode
    const startDate = moment({ year, month })
      .startOf("month")
      .format("YYYY-MM-DD");

    // Menghitung tanggal akhir periode
    const endDate = moment({ year, month }).endOf("month").format("YYYY-MM-DD");
    return { startDate, endDate };
  },

  getUser: async function (userId = 0, email = "") {
    var sql = `SELECT A.id, A."name", A.gender , A.phone , A.email , A.username , A."password" , A.address, A.photo, r.id role_id, r."name" roles, r.code, r.active
                FROM users A 
                JOIN roles r on A.role_id = r.id  
                WHERE A.id = ? OR A.email = ?`;

    let error, permission;

    let users = await database
      .row(sql, [parseInt(userId), email])
      .then((result) => {
        // console.log(result.rows[0]); // Menampilkan hasil query

        return result;
      })
      .catch((err) => {
        error = err.message;
        // Menampilkan pesan error jika terjadi kesalahan
      });

    let permision_module = `
    SELECT mp.module_id, mp.task_id , mp."action", rd.role_id
    FROM module_permissions mp
    INNER JOIN modules m on m.id = mp.module_id
    INNER JOIN role_details rd on m.id = rd.module_id
    WHERE rd.role_id = ?`;

    const data = await database
      .raw(permision_module, [users.role_id])
      .then((el) => el.rows)
      .catch((e) => console.log(e));

    let responseManipulation = async (item) => {
      let task = [];
      let permmission = [];
      // PERULANGAN UNTUK MENDAPATKAN DATA TASK DAN ACTION
      for (let i = 0; i < data.length; i++) {
        let selectTask = `select t."name" from tasks t where t.id = ?`;
        let fromTask = await database.row(selectTask, [data[i].task_id]);
        task.push(fromTask);
        permmission.push({ action: data[i].action });
      }
      // PERULANGAN UNTUK MENGGABUNGKAN DATA TASK DAN PERMISSION
      let result = new Set();
      for (const p of permmission) {
        for (const t of task) {
          result.add(`${t.name}-${p.action}`);
        }
      }

      return [...result];
    };

    // //   DATA MANIPULASI UNTUK MENGEMBALIKAN RESPONSE
    let response = await responseManipulation(data);
    permission = [...response];

    if (error) {
      return error;
    } else {
      return { ...users, permission };
    }
  },

  generateRandomToken: function () {
    const randomPart = Math.random().toString(36).substring(2);
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
    const date = ("0" + currentDate.getDate()).slice(-2);
    const token = randomPart + "-" + `${year}/${month}/${date}`;
    return token;
  },

  nestedData: function (data) {
    const nestedData = [];

    data.forEach((item) => {
      const user = nestedData.find((user) => user.username === item.user);

      if (!user) {
        nestedData.push({
          username: item.user,
          roles: item.name,
          role_id: item.role_id,
          role_code: item.code,
          active: item.active,
          description: item.description,
          detail_role: [
            {
              allowed: item.allowed,
              module_group: item.module_group,
              module_name: item.module_name,
              module_code: item.module_code,
            },
          ],
        });
      } else {
        user.detail_role.push({
          allowed: item.allowed,
          module_group: item.module_group,
          module_name: item.module_name,
          module_code: item.module_code,
        });
      }
    });

    return nestedData;
  },

  nestedModule: function (data) {
    const nestedData = [];

    data.forEach((item) => {
      const moduleGrup = nestedData.find(
        (moduleGrup) => moduleGrup.module_group === item.module_group
      );

      if (!moduleGrup) {
        nestedData.push({
          module_group: item.module_group,
          grup_code: item.grup_code,
          module: [
            {
              modules: item.modules,
              module_code: item.module_code,
            },
          ],
        });
      } else {
        moduleGrup.module.push({
          modules: item.modules,
          module_code: item.module_code,
        });
      }
    });

    return nestedData;
  },

  generateNestedTask: function (data) {
    const nestedData = [];

    data.forEach((item) => {
      const taskModule = nestedData.find(
        (taskModule) => taskModule.module_name === item.module_name
      );

      if (!taskModule) {
        nestedData.push({
          id: item.id,
          module_code: item.module_code,
          module_name: item.module_name,
          task: [
            {
              name: item.task,
              code: item.task_code,
              action: item.action,
              task_id: item.task_id,
              module_id: item.module_id,
            },
          ],
        });
      } else {
        taskModule.task.push({
          name: item.task,
          code: item.task_code,
          action: item.action,
          task_id: item.task_id,
          module_id: item.module_id,
        });
      }
    });

    return nestedData;
  },

  generateNested: function (data) {
    const nestedData = [];

    data.forEach((item) => {
      const taskModule = nestedData.find(
        (taskModule) => taskModule.module_name === item.module_name
      );

      if (!taskModule) {
        nestedData.push({
          id: item.id,
          role_id: item.role_id,
          module_code: item.module_code,
          module_name: item.module_name,
          permission: [
            {
              module_id: item.module_id,
              code: item.task_code,
              action: item.action,
              task: item.task,
            },
          ],
        });
      } else {
        taskModule.permission.push({
          module_id: item.module_id,
          code: item.task_code,
          action: item.action,
          task: item.task,
        });
      }
    });

    return nestedData;
  },

  autoGenerateInvoice: async (
    db,
    corporate_id,
    uuid_plant,
    user_id,
    date,
    generateInvoiceNumber,
    generatePeriod
  ) => {
    let currentDate = moment().locale("id").utcOffset(7).format("YYYY-MM-DD");

    // FUNTION TO HENDLE FIND THE PRODUCTION UNIT WHERE CORPORATE AND ACTIVE 1
    const selectPUC = async (corporateId) => {
      let sql = `select puc.corporate_id ,puc.production_unit_id ,puc.billing_date ,puc.unit_price, puc.tolerance, puc.active, puc.installation_fee 
                  from production_unit_config puc 
                  where puc.corporate_id = ? AND puc.active = '1'`;
      const unit_config = await db
        .raw(sql, [corporateId])
        .then((result) => {
          // Menampilkan hasil query
          return result.rows;
        })
        .catch((err) => {
          // Menampilkan pesan error jika terjadi kesalahan
          console.log(err);
          throw new CoreException("something wrong | terjadi kesalahan");
        });
      return unit_config;
    };

    // FIND THE CORPORATE
    let sql = `select c.id ,c.code,c."name" 
                from corporate c where c.uuid = ?`;
    const corporate_unit_cfg = await db
      .row(sql, [corporate_id])
      .then(async (corporate) => {
        // Menampilkan hasil query
        let dataPuc = await selectPUC(corporate.id);
        return dataPuc;
      })
      .catch((err) => {
        // Menampilkan pesan error jika terjadi kesalahan
        throw new CoreException("something wrong | terjadi kesalahan");
      });

    // TO GET DATA REALTIME FROM RBS
    let baseUrl = "https://api.teknologireadymix.com/rbs/dev/";
    // let token = "14093f30c292a7686b34ced731cf7295";
    let dataPlant = await axios
      .get(
        `${baseUrl}/support/summary/production-by-plant/${uuid_plant}?date[]=${date[0]}&date[]=${date[1]}`,
        {
          headers: {
            Authorization: process.env.config.INTEGRATION.AUTHORIZATION,
          },
        }
      )
      .then((e) => {
        return e.data.data;
      })
      .catch((err) => {
        throw new CoreException("something wrong | terjadi kesalahan");
      });

    // FUNTION TO HENDLE INSERT INVOICE PAYMENT AFTER INVOICE CREATED
    const insertInvoicePayment = async (item) => {
      let paymentInvoice = {
        uuid: uuidv4(),
        invoice_id: item.id,
        payment_channel_id: 3,
        receipt_number: item.invoice_number,
        amount: item.amount,
        paid_at: item.end_periode,
        description: "Auto Generate Invoice",
        status_code: "PENDING",
        active: 1,
        created_at: currentDate,
        created_by: user_id,
      };
      await db
        .run_insert("invoice_payment", paymentInvoice)
        .then((resuslt) => {
          console.log(resuslt);
        })
        .catch((err) => {
          throw new CoreException(
            "invoice key number already exists | nomor invoice telah ada"
          );
        });
    };

    // FUNTION TO HENDLE DATA INVOICE
    const insertInvoice = (item) => {
      let dataInvoice = {};
      let generateInvoice = generateInvoiceNumber(currentDate);
      let periode = generatePeriod(item.billing_date);
      let unitPrice = item.unit_price;
      let quantity = dataPlant.total_success;
      let ammount = quantity * unitPrice;
      let taxAmount = (11 / 100) * ammount;
      let total_amount = taxAmount + ammount;
      return (dataInvoice = {
        ...dataInvoice,
        uuid: uuidv4(),
        corporate_id: item.corporate_id ? item.corporate_id : null,
        production_unit_id: item.production_unit_id
          ? item.production_unit_id
          : null,
        invoice_number: generateInvoice,
        unit_price: unitPrice,
        quantity: quantity,
        amount: ammount,
        tax: 1,
        tax_amount: taxAmount,
        total_amount: total_amount,
        total_paid: total_amount + taxAmount,
        periode: currentDate,
        start_periode: periode.startDate,
        end_periode: periode.endDate,
        overdue: periode.endDate,
        status_code: "PENDING",
        created_at: currentDate,
        created_by: user_id,
      });
    };

    // JIKA CORPORATE UNIT CONFIG DITEMUKAN MAKA PENJADWALAN PEMBUATAN INVOICE OTOMATIS AKAN DILAKUKAN SETIAP BULAN
    corporate_unit_cfg.forEach((item) => {
      let startMonth = moment(item.billing_date).format("M");
      let startDate = moment(item.billing_date).format("DD");
      //   CRON RUNNING
      const cron = require("node-cron");

      // PENJADWALAN PERBULAN
      let perPeriodMount = `0 0 ${startDate} ${startMonth + 1} *`;
      const task = cron.schedule(
        // "*/10 * * * * *",
        perPeriodMount, //Aktifkan Untuk Schedule perbulan
        async () => {
          // INSERT INVOICE
          let dataInvoice = insertInvoice(item);
          //   console.log("cron berjalan");
          if (Object.keys(dataInvoice).length === 0) {
            throw new CoreException(
              "unit active config null | unit active config kosong"
            );
          } else {
            // console.log(dataInvoice);

            await db
              .run_insert("invoice", dataInvoice)
              .then((resuslt) => {
                console.log(resuslt);
                // INSERT INVOCE PAYMENT
                insertInvoicePayment(resuslt);
              })
              .catch((err) => {
                throw new CoreException(
                  "invoice key number already exists | nomor invoice telah ada"
                );
              });
          }
        },
        {
          scheduled: false, // Menjadwalkan task secara manual
        }
      );
      // Menjalankan cron job setelah 10 detik
      // setTimeout(() => {
      task.start();
      // }, 10000);
    });
    // 0 0 1 * *
    // | | | | |
    // | | | | +---- Hari dalam seminggu (0-7, di sini 0 dan 7 merepresentasikan Minggu)
    // | | | +------ Bulan (1-12)
    // | | +-------- Tanggal (1-31)
    // | +---------- Jam (0-23)
    // +------------ Menit (0-59)
  },

  generateTemplateNumberInvoice: (input) => {
    const parts = input.split("/");

    let prefix = [
      { key: "prefix", value: "" },
      { key: "number", value: "" },
      { key: "year", value: "" },
      { key: "month", value: "" },
    ];

    parts.forEach((part) => {
      if (/^\d{3}$/.test(part)) {
        prefix.find((item) => item.key === "number").value = part;
      } else if (/^\d{4}$/.test(part)) {
        prefix.find((item) => item.key === "year").value = part;
      } else if (/^\d{1,2}$/.test(part)) {
        prefix.find((item) => item.key === "month").value = part;
      } else {
        prefix.find((item) => item.key === "prefix").value =
          part === "INV" ? part : "INV";
      }
    });

    const romanNumerals = {
      1: "I",
      2: "II",
      3: "III",
      4: "IV",
      5: "V",
      6: "VI",
      7: "VII",
      8: "VIII",
      9: "IX",
      10: "X",
      11: "XI",
      12: "XII",
    };

    convertRoman =
      romanNumerals[
        parseInt(prefix.find((item) => item.key === "month").value)
      ];

    const result = [
      prefix.find((item) => item.key === "number").value || "001",
      prefix.find((item) => item.key === "prefix").value || "INV",
      "TRI",
      convertRoman || "00",
      prefix.find((item) => item.key === "year").value ||
        new Date().getFullYear().toString(),
    ].join("/");

    return result;
  },

  fetchAPI: async (URL) => {
    try {
      const options = {
        method: "GET",
        headers: {
          Authorization: process.env.config.INTEGRATION.AUTHORIZATION,
        },
      };
      const response = await axios.get(URL, options);
      return response.data;
    } catch (error) {
      if (error.response.status == "401" || error.response.status == "403") {
        throw new CoreException(
          "Authorization Code is invalid | Kode Autorisasi salah (invalid)"
        );
      }
      if (error.response.status == "422") {
        throw new CoreException(
          "Corporate and Production Unit not match| Perusahaan dan Unit Produksi tidak sesuai"
        );
      }

      throw new CoreException(error.message);
    }
  },

  filterDinamis: (arrFilter, arrValue) => {
    let filter = [];
    let filterValue = [];

    filter = [...filter, ...arrFilter];
    filterValue = [...filterValue, ...arrValue];

    filter = filter.length > 0 ? ` WHERE ${filter.join(" AND ")}` : "";

    return {
      filter,
      filterValue,
    };
  },

  generateMonthlyDates: (year) => {
    const startDate = new Date(year, 0, 1); // Mulai dari tanggal 1 Januari
    const endDate = new Date(year, 11, 31); // Sampai tanggal 31 Desember

    const dates = [];

    let currentDate = new Date(startDate);

    function formatMonth(month) {
      return month < 10 ? "0" + month : month;
    }

    function getLastDayOfMonth(year, month) {
      return new Date(year, month + 1, 0).getDate();
    }

    while (currentDate <= endDate) {
      const startOfMonth =
        currentDate.getFullYear() +
        "-" +
        formatMonth(currentDate.getMonth() + 1) +
        "-01";
      const endOfMonth =
        currentDate.getFullYear() +
        "-" +
        formatMonth(currentDate.getMonth() + 1) +
        "-" +
        getLastDayOfMonth(currentDate.getFullYear(), currentDate.getMonth());

      const month = currentDate.getMonth() + 1;

      dates.push({
        month: month,
        start: startOfMonth,
        end: endOfMonth,
      });

      currentDate.setMonth(currentDate.getMonth() + 1);
    }

    return dates;
  },

  setDate: function (date) {
    return moment(date).format("YYYY-MM-DD");
  },

  calculateMedian: (numbers) => {
    // Sort the numbers from smallest to largest
    numbers.sort(function (a, b) {
      return a - b;
    });

    var median;
    // If the number of digits is odd, the median is the number in the middle
    if (numbers.length % 2 === 1) {
      median = numbers[Math.floor(numbers.length / 2)];
    } else {
      // If the number of digits is even, the median is the average of the two middle numbers
      var middleIndex = numbers.length / 2;
      median = (numbers[middleIndex - 1] + numbers[middleIndex]) / 2;
    }

    return median;
  },

  dateValidation: (date) => {
    if (!date) {
      const currentDate = new Date()
      const year = currentDate.getFullYear()
      const month = currentDate.getMonth()

      let firstMonth = new Date(year, month, 1)
      let lastMonth = new Date(year, month + 1, 0)

      firstMonth = moment(firstMonth).format("YYYY-MM-DD")
      lastMonth = moment(lastMonth).format("YYYY-MM-DD")

      return [firstMonth, lastMonth]
    }

    const oneDayMiliseconds = 86400000

    let [startDate, endDate] = date
    startDate = new Date(startDate)
    endDate = new Date(endDate)

    let differentDate = (endDate - startDate) / oneDayMiliseconds

    let totalDay = Extend.getTotalDay(startDate.getMonth(), startDate.getFullYear())

    if (differentDate > totalDay) throw new CoreException(`Maximum period of ${totalDay} days`)

    return date
  },
};

module.exports = { Extend };
