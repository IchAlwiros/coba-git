exports.up = async function (knex) {
  await knex.schema.table("production_unit_config", (table) => {
    table
      .foreign("production_unit_id")
      .references("id")
      .inTable("production_unit")
      .onDelete("CASCADE");
  });
};

exports.down = async function (knex) {
  await knex.schema.table("production_unit_config", (table) => {
    table.dropColumn("production_unit_id");
  });
};
