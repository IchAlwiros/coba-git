/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("users", function (table) {
    table.bigIncrements("id").primary();
    table.string("name").index();
    table.string("phone").index();
    table.string("email").index();
    table.enum("gender", ["L", "P"]).index();
    table.text("address").index();
    table.string("username").index();
    table.string("password").index();
    table.enum("google_auth", ["0", "1"]).defaultTo("0").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.string("photo").index();
    table.integer("role_id").index();
    table.integer("religion_id").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("users");
};
