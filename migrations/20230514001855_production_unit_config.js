/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("production_unit_config", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.bigint("corporate_id").index()
    // FK 
    table.bigint("production_unit_id").index()

    // .references("id")
    // .inTable("production_unit")
    table.decimal("unit_price", 20, 2).index()
    table.decimal("installation_fee", 20, 2).index()
    table.enum("tax", ["0", "1"]).defaultTo("1").index();
    table.date("billing_date").index()
    table.integer("tolerance").index()
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true)
    table.integer("created_by").nullable()
    table.integer("updated_by").nullable()
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("production_unit_config");
};
