/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("role_details", (table) => {
    table.bigIncrements("id").primary();
    table.integer("role_id").index();
    table.integer("module_id").index();
    table.enum("allowed", ["0", "1"]).defaultTo("0").index();
    table.enum("active", ["0", "1"]).defaultTo("0").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("role_details");
};
