/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("module_groups", (table) => {
    table.bigIncrements("id").primary();
    table.string("code").index();
    table.string("name").index();
    table.text("description").index();
    table.integer('display_number').index()
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("module_groups");
};
