/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("production_unit", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.uuid("uuid").unique().index();
    // FK
    table.bigint("corporate_id").references("id").inTable("corporate").index();
    table.string("code").unique().index();
    table.string("name").index();
    table.string("phone").index();
    table.string("email").index();
    table.text("address").index();
    table.timestamp("registered_at").index();
    table.string("subscription_code").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true);
    table.integer("created_by").nullable();
    table.integer("updated_by").nullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("production_unit");
};
