/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("company_profile", function (table) {
    table.bigIncrements("id").primary();
    table.integer("invoice_template_config_id").index();
    table.text("profile_image").index();
    table.string("name").index();
    table.text("tagline").index();
    table.string("signature_name").index();
    table.string("signature_description").index();
    table.enum("npwp", ["0", "1"]).defaultTo("0").index();
    table.string("nomor_npwp").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("company_profile");
};
