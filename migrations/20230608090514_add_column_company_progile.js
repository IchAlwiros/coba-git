exports.up = function (knex) {
  return knex.schema.table("company_profile", function (table) {
    table.uuid("uuid").unique().index();
  });
};

exports.down = function (knex) {
  return knex.schema.table("company_profile", function (table) {
    table.dropColumn("uuid");
  });
};
