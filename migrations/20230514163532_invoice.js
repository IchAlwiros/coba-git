/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("invoice", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.uuid("uuid").unique().index()
    table.bigint("corporate_id").index()
    table.bigint("production_unit_id").index()
    // FK
    table.bigint("invoice_category_id")
      .index()
    // .references("id")
    // .inTable("invoice_category")
    table.string("invoice_number").unique().index()
    table.decimal("unit_price", 20, 2).index()
    table.decimal("quantity", 20, 2).index()
    table.decimal("amount", 20, 2).index()
    table.enum("tax", ["0", "1"]).defaultTo("1").index();
    table.decimal("tax_amount", 20, 2).index()
    table.decimal("total_amount", 20, 2).index()
    table.decimal("total_paid", 20, 2).index()
    table.date("periode").index()
    table.date("start_periode").index()
    table.date("end_periode").index()
    table.date("overdue").index()
    table.string("status_code").index()
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true)
    table.integer("created_by").nullable()
    table.integer("updated_by").nullable()
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("invoice");
};
