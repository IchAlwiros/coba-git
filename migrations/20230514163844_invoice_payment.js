/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("invoice_payment", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.uuid("uuid").unique().index();
    table.bigint("invoice_id").index()
    // .references("id").inTable("invoice")
    table.bigint("payment_channel_id").index();
    table.string("receipt_number").index();
    table.decimal("amount", 20, 2).index();
    table.timestamp("paid_at").index();
    table.string("status_code").index();
    table.string("attachment");
    table.string("description");
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true);
    table.integer("created_by").nullable();
    table.integer("updated_by").nullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("invoice_payment");
};
