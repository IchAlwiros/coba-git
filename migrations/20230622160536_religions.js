/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("religion", (table) => {
    table.bigIncrements("id");
    table.string("name", 255).index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now()).index();
    table.timestamp("updated_at").defaultTo(knex.fn.now()).index();
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("religion");
};
