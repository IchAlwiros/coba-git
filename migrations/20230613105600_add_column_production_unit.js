/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  try {
    await knex.schema.alterTable("corporate", function (table) {
      table.string("logo_preview").after("address").nullable()
    })
    await knex.schema.alterTable("production_unit", function (table) {
      table.string("pic_name").after("address").nullable().index()
      table.string("pic_phone").after("pic_name").nullable().index()
      table.string("pic_position").after("pic_phone").nullable().index()
    })
  } catch (error) {
    console.error("Error Add Column : ", error)
  }
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
    try {
        await knex.schema.alterTable("corporate", function (table) {
          table.dropColumn("logo_preview")
        })
        await knex.schema.alterTable("production_unit", function (table) {
          table.dropColumn("pic_name")
          table.dropColumn("pic_phone")
          table.dropColumn("pic_position")
        })
    } catch (error) {
    console.error("Error Drop Column: ", error)
        
    }
}
