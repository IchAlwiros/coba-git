/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("payment_channel_category", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.string("code").unique().index()
    table.string("name").unique().index()
    table.string("description");
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true)
    table.integer("created_by").nullable()
    table.integer("updated_by").nullable()
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("payment_channel_category");
};
