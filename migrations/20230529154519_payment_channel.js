exports.up = function (knex) {
  return knex.schema.createTable("payment_channel", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable();
    table.uuid("uuid").unique().index()
    table.bigint("payment_channel_category_id")
    table.string("name").index()
    table.string("bank_code").index()
    table.string("bank_name").index()
    table.string("account_name").index()
    table.string("account_number").index()
    table.decimal("admin_fee", 20, 2).index().defaultTo(0)
    table.text("icon");
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true)
    table.integer("created_by").nullable()
    table.integer("updated_by").nullable()
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("payment_channel");
};
