/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable("invoice_payment_detail", (table) => {
      table
        .bigIncrements("id", {
          primaryKey: true,
        })
        .index()
        .notNullable();
      table.uuid("uuid").unique().index()
      // FK 1
      table.bigint("payment_id")
        .references("id")
        .inTable("invoice_payment")
        .index()
      // FK 2
      table.bigint("payment_channel_id")
        .references("id")
        .inTable("payment_channel")
        .index()
      table.string("ref_code").unique().index()
      table.string("payment_number").unique().index()
      table.decimal("amount", 20, 2).index()
      table.decimal("admin_fee", 20, 2).index()
      table.decimal("total_amount", 20, 2).index()
      table.timestamp("expired_at").index()
      table.timestamp("paid_at").index()
      table.string("status_code").index()
      table.string("description");
      table.enum("active", ["0", "1"]).defaultTo("1").index();
      table.timestamps(true, true)
      table.integer("created_by").nullable()
      table.integer("updated_by").nullable()
    });
  };
  
  /**
   * @param { import("knex").Knex } knex
   * @returns { Promise<void> }
   */
  exports.down = function (knex) {
    return knex.schema.dropTable("invoice_payment_detail");
  };
  