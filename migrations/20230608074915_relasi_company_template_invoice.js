/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.table("company_profile", (table) => {
    table
      .foreign("invoice_template_config_id")
      .references("id")
      .inTable("invoice_template_config")
      .onDelete("CASCADE");
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.table("company_profile", (table) => {
    table.dropColumn("invoice_template_config_id");
  });
};
