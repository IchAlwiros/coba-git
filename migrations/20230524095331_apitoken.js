/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("api_token", (table) => {
    table.bigIncrements("id").primary();
    table.integer("user_id").index();
    table.text("api_token").index();
    table.string("sso_token").index();
    table.string("socket").index();
    table.string("device_info").index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("last_active").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("api_token");
};
