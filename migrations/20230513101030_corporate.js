/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("corporate", (table) => {
    table
      .bigIncrements("id", {
        primaryKey: true,
      })
      .index()
      .notNullable()
    table.uuid("uuid").unique().index()
    table.string("code").unique().index()
    table.string("name").unique().index()
    table.string("phone").unique().index();
    table.string("email").unique().index();
    table.text("address").index();
    table.timestamp("registered_at").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.timestamps(true, true)
    table.integer("created_by").nullable()
    table.integer("updated_by").nullable()
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("corporate");
};
