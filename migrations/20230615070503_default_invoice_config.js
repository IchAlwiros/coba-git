/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("default_invoice_config", function (table) {
    table.bigIncrements("id").primary();
    table.integer("invoice_category_id").index();
    table.integer("payment_channel_id").index();
    table.string("format_number").index();
    table.enum("tax", ["0", "1"]).defaultTo("1").index();
    table.decimal("price", 20, 2).index();
    table.integer("tolerance").defaultTo("30").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("default_invoice_config");
};
