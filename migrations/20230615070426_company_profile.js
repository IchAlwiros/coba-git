/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("company_profile", function (table) {
    table.bigIncrements("id").primary();
    table.text("logo").index();
    table.string("name").index();
    table.text("address").index();
    table.text("tagline").index();
    table.string("email").index();
    table.string("suip_number").index();
    table.string("tdp_number").index();
    table.string("npwp_number").index();
    table.string("pic_name").index();
    table.string("pic_position").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("company_profile");
};
