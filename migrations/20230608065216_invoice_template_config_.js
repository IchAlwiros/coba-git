/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("invoice_template_config", function (table) {
    table.bigIncrements("id").primary();
    table.integer("invoice_category_id").index();
    table.integer("payment_channel_id").index();
    table.string("number_invoice").index();
    table.enum("installation_fee", ["0", "1"]).defaultTo("1").index();
    table.decimal("price", 20, 2).index();
    table.integer("tax").defaultTo("11").index();
    table.date("billing_date").index();
    table.integer("tolerance").defaultTo("30").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("invoice_template_config");
};
