/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("module_permissions", (table) => {
    table.bigIncrements("id").primary();
    table.integer("module_id").index();
    table.integer("task_id").index();
    table.string("action").index();
    table.string("attribute").index();
    table.string("possession").index();
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("module_permissions");
};
