/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("payment_gateway_config", function (table) {
    table.bigIncrements("id").primary();
    table.string("ip_address").index();
    table.text("secret_key").index();
    table.string("client_key").index();
    table.enum("active", ["0", "1"]).defaultTo("1").index();
    table.bigInteger("created_by").nullable().index();
    table.bigInteger("updated_by").nullable().index();
    table.timestamps(true, true);
  });
};
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("payment_gateway_config");
};
