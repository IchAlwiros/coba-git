exports.up = async function (knex) {
  await knex.schema.table("invoice", (table) => {
    table
      .foreign("corporate_id")
      .references("id")
      .inTable("corporate")
      .onDelete("CASCADE");
  });
};

exports.down = async function (knex) {
  await knex.schema.table("invoice", (table) => {
    table.dropColumn("corporate_id");
  });
};
