const config = {
  VERSION: "1.0.0",
  APP_NAME: "RBS SUPPORT SYSTEM",
  APP_KEY: "rbssupport2023",
  DEFAULT_LANGUAGE: "EN", // ID,
  BASE_URL: "https://api.teknologireadymix.com/rbs/support",
  WEB_URL: "https://app.teknologireadymix.com/rbs/support",

  // INTEGRATION
  INTEGRATION: {
    BASE_URL: "https://api.teknologireadymix.com/rbs/dev",
    AUTHORIZATION: "14093f30c292a7686b34ced731cf7295",
    BASE_LOCAL: "http://localhost:7000",
  },
}
module.exports = config
