require('dotenv').config()
const { CoreResponse, database } = require('../core/CallService')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const requestIp = require('request-ip')

module.exports = function middleware(service) {
    return async (req, res, next) => {
        // INIT ENV
        req.language = req.headers.lang ? req.headers.lang.toUpperCase() : process.env.config.LANGUAGE
        const clientIp = requestIp.getClientIp(req)
        req.ipAddress = clientIp
        req.serviceType = service.type

        // FIND SERVICE
        if (!service) return CoreResponse.fail(res, 'Page not found', {}, 404)
        let session = {
            user_id: null,
            api_token: null,
            socket_id: null,
            fcm_token: null,
            token_id: null,
            role_code: 'GUEST',
            role_name: 'GUEST',
            role_id: -2,
        }

        // NO AUTH
        if (!service.auth) {
            req.session = session
            return next()
        }

        // HEADERS
        let headers = req.headers.authorization
        // DECODE AUTH TOKEN
        let decodedToken = jwt.verify(headers, process.env.config.APP_KEY, (err, decoded) => {
            if (err) return null
            return decoded
        })
        // END DECODE AUTH TOKEN
        let db = database

        if (headers) {
            if (service.auth && !decodedToken) return CoreResponse.fail(res, 'Unauthorized', {}, 401)
            var sql = ` SELECT A.id AS token_id, A.user_id,B.name AS user_name, 
                        B.role_id, C.code AS role_code, C.name AS role_name,
                        CASE WHEN B.active ='1' THEN TRUE ELSE FALSE END AS active
                        FROM api_token A
                        INNER JOIN users B ON B.id = A.user_id
                        INNER JOIN roles C ON C.id = B.role_id 
                        WHERE A.api_token = ? LIMIT 1`
            await db.raw(sql, [decodedToken.api_token])
                .then((resp) => { session = resp.rows[0] })
                .catch((err) => {
                    console.log(err)
                    session = false
                })
            if (!session) return CoreResponse.fail(res, 'Unauthorized', {}, 401)
            if (!session.active) return CoreResponse.fail(res, 'Account non active', {}, 401)
        }

        // SESSION
        session.datetime = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss')
        req.session = session
        // END SESSION

        // BYPASS SUPER ADMIN
        if (session.role_id === -1) return next()

        // INIT PERMISSSIONS
        let actionObj = {
            POST: 'create',
            GET: 'read',
            PATCH: 'update',
            PUT: 'update',
            DELETE: 'delete',
            UPLOAD: 'create',
            VIEW: 'view',
        }
        let method = req.method
        let action = actionObj[method]
        let serviceTask = service.task

        if (['VIEW'].includes(service.type)) action = 'view'
        else if (service.type == 'UPLOAD') action = 'create'
        else if (service.type == 'LOOKUP') action = 'lookup'

        var sql2 = `SELECT A.id FROM roles A
                    INNER JOIN role_details B ON B.role_id = A.id
                    INNER JOIN modules C ON C.id = B.module_id
                    INNER JOIN module_permissions D ON D.module_id = C.id
                    INNER JOIN tasks E ON E.id = D.task_id
                    WHERE B.allowed ='1' AND B.active ='1' AND  A.id = ? AND E.code = ? AND D.action = ?`
        let permission = await db.raw(sql2, [session.role_id, serviceTask, action])
            .then((resp) => { return resp.rows[0] })
            .catch((err) => { return })
        if (!permission) return CoreResponse.fail(res, 'Permission Denied', {}, 403)
        return next()
    }
}